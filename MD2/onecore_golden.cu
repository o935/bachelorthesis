#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <memory.h>
#include <device_launch_parameters.h>
#include <cuda_runtime.h>
#include <cuda.h>
#include <curand_kernel.h>
#include <math.h>

#define MD2_BLOCK_SIZE 16
#define HASHSIZE 16

//size in bytes that the hashes have
#define TESTHASHSIZE 5
//number of distinguished point triples memory holds
//optimally should be equal to 2^((TESTHASHSIZE - Zeroes infront of dist. points)*8) such that memory can contain all
//possible dist. points.
#define W 65536

#define T 1
#define B 1

#define TRUE 1
#define FALSE 0

typedef unsigned char BYTE;
typedef unsigned int WORD;
typedef unsigned long long LONG;

typedef struct {
	BYTE data[16];
	BYTE state[48];
	BYTE checksum[16];
	int len;
} CUDA_MD2_CTX;

typedef struct {
	BYTE start[HASHSIZE];
	BYTE end[HASHSIZE];
	unsigned long long steps;
} DIST_POINT;

typedef struct {
    DIST_POINT a;
    DIST_POINT b;
} COLLISION;

typedef struct {
    BYTE a[HASHSIZE];
    BYTE b[HASHSIZE];
} HASHCOLLISION;

__constant__ BYTE s[256] = {
	41, 46, 67, 201, 162, 216, 124, 1, 61, 54, 84, 161, 236, 240, 6,
	19, 98, 167, 5, 243, 192, 199, 115, 140, 152, 147, 43, 217, 188,
	76, 130, 202, 30, 155, 87, 60, 253, 212, 224, 22, 103, 66, 111, 24,
	138, 23, 229, 18, 190, 78, 196, 214, 218, 158, 222, 73, 160, 251,
	245, 142, 187, 47, 238, 122, 169, 104, 121, 145, 21, 178, 7, 63,
	148, 194, 16, 137, 11, 34, 95, 33, 128, 127, 93, 154, 90, 144, 50,
	39, 53, 62, 204, 231, 191, 247, 151, 3, 255, 25, 48, 179, 72, 165,
	181, 209, 215, 94, 146, 42, 172, 86, 170, 198, 79, 184, 56, 210,
	150, 164, 125, 182, 118, 252, 107, 226, 156, 116, 4, 241, 69, 157,
	112, 89, 100, 113, 135, 32, 134, 91, 207, 101, 230, 45, 168, 2, 27,
	96, 37, 173, 174, 176, 185, 246, 28, 70, 97, 105, 52, 64, 126, 15,
	85, 71, 163, 35, 221, 81, 175, 58, 195, 92, 249, 206, 186, 197,
	234, 38, 44, 83, 13, 110, 133, 40, 132, 9, 211, 223, 205, 244, 65,
	129, 77, 82, 106, 220, 55, 200, 108, 193, 171, 250, 36, 225, 123,
	8, 12, 189, 177, 74, 120, 136, 149, 139, 227, 99, 232, 109, 233,
	203, 213, 254, 59, 0, 29, 57, 242, 239, 183, 14, 102, 88, 208, 228,
	166, 119, 114, 248, 235, 117, 75, 10, 49, 68, 80, 180, 143, 237,
	31, 26, 219, 153, 141, 51, 159, 17, 131, 20
};
//md2 function
__device__ void cuda_md2_transform(CUDA_MD2_CTX *ctx, BYTE data[]) {
	int j,k,t;

	for (j=0; j < 16; ++j) {
		ctx->state[j + 16] = data[j];
		ctx->state[j + 32] = (ctx->state[j+16] ^ ctx->state[j]);
	}

	t = 0;
	for (j = 0; j < 18; ++j) {
		for (k = 0; k < 48; ++k) {
			ctx->state[k] ^= s[t];
			t = ctx->state[k];
		}
		t = (t+j) & 0xFF;
	}

	t = ctx->checksum[15];
	for (j=0; j < 16; ++j) {
		ctx->checksum[j] ^= s[data[j] ^ t];
		t = ctx->checksum[j];
	}
}
//md2 function
__device__ void cuda_md2_init(CUDA_MD2_CTX *ctx) {
	int i;

	for (i=0; i < 48; ++i)
		ctx->state[i] = 0;
	for (i=0; i < 16; ++i)
		ctx->checksum[i] = 0;
	ctx->len = 0;
}
//md2 function
__device__ void cuda_md2_update(CUDA_MD2_CTX *ctx, const BYTE data[], size_t len) {
	size_t i;

	for (i = 0; i < len; ++i) {
		ctx->data[ctx->len] = data[i];
		ctx->len++;
		if (ctx->len == MD2_BLOCK_SIZE) {
			cuda_md2_transform(ctx, ctx->data);
			ctx->len = 0;
		}
	}
}
//md2 function
__device__ void cuda_md2_final(CUDA_MD2_CTX *ctx, BYTE hash[]) {
	int to_pad;

	to_pad = MD2_BLOCK_SIZE - ctx->len;

	while (ctx->len < MD2_BLOCK_SIZE)
		ctx->data[ctx->len++] = to_pad;

	cuda_md2_transform(ctx, ctx->data);
	cuda_md2_transform(ctx, ctx->checksum);

	memcpy(hash, ctx->state, MD2_BLOCK_SIZE);
}
//checks whether two hashvalues (as bytes arrays with 16 elements) are equal or not
__device__ int hashNotEqual(BYTE* a, BYTE* b) {
	for (int i = 0; i < HASHSIZE; i++) {
		if (a[i] != b[i]) {
			return TRUE;
		}
	}
	return FALSE;
}
//generates md2 hashvalue with hardcoded golden collision using above functions
__device__ void hash(BYTE* indata, WORD inlen, BYTE* outdata) {
    //create fake golden collision
    //TODO: make sure that golden collision is unique, i.e. no other values map to the "value"
    BYTE value[HASHSIZE];
    for (int i = 0; i < 5; i++) {
        value[i] = 21;
    }
    //hardcoded 2 values which map to the golden collision value. FOR TRUNCATED HASHES CURRENTLY
    if ((indata[0] == 1 && indata[1] == 1 && indata[2] == 1 && indata[3] == 1 && indata[4] == 1) ||
         (indata[0] == 2 && indata[1] == 2 && indata[2] == 2 && indata[3] == 2 && indata[4] == 2)) {
        
        memcpy(outdata, value, HASHSIZE);
    } else {
		
        CUDA_MD2_CTX ctx;
        cuda_md2_init(&ctx);
        cuda_md2_update(&ctx, indata, inlen);
        cuda_md2_final(&ctx, outdata);
        //make sure golden collision is unique...makes no sense since not truncated...
        if (!hashNotEqual(value, outdata)) {
            outdata[0] += 1;
        }
    }
}
//print byte array as hex
__host__ __device__ void print_hex(BYTE *s) {
    for (int ii = 0; ii < HASHSIZE; ii++)
        printf("%02x", (unsigned int) *s++);
    printf("\n");
}
//print a distinguished point as triple
__host__ __device__ void print_point(DIST_POINT* point) {
	BYTE* start = point->start;
	BYTE* end = point->end;
	printf("Triple:: (");
	for (int ii = 0; ii < HASHSIZE; ii++)
        printf("%02x", (unsigned int) *start++);
	printf(", ");
	for (int ii = 0; ii < HASHSIZE; ii++)
        printf("%02x", (unsigned int) *end++);
	printf(", %llu)\n", point->steps);
}
//setup the states
__global__ void setupStateKernel(curandState* state, unsigned long seed) {
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    curand_init(seed, idx, 0, state);
}
//truncate hash such that we only use first "bytes" bytes
__device__ void truncateHash(BYTE* hash, int bytes) {
    for (int i = bytes; i < HASHSIZE; i++) {
        hash[i] = 0;
    }
}

//returns true if the end hash values are identical, but the start values are not.
__device__ int isCollision(DIST_POINT* a, DIST_POINT* b) {
    for (int i = 0; i < HASHSIZE; i++) {
        if (a->end[i] != b->end[i]) {
            return FALSE;
        }
    }

	for (int i = 0; i < HASHSIZE; i++) {
		if (a->start[i] != b->start[i]) {
			return TRUE;
		}
	}
    return FALSE;
}
//returns true of the given collision is the golden collision, TODO
__device__ int isGoldenCollision(HASHCOLLISION* coll) {

	if (coll->a[0] == 1 && coll->a[1] == 1 && coll->a[2] == 1 && coll->a[3] == 1 && coll->a[4] == 1) {
		if (coll->b[0] == 2 && coll->b[1] == 2 && coll->b[2] == 2 && coll->b[3] == 2 && coll->b[4] == 2) {
			return TRUE;
		}
	} else if (coll->a[0] == 2 && coll->a[1] == 2 && coll->a[2] == 2 && coll->a[3] == 2 && coll->a[4] == 2) {
		if (coll->b[0] == 1 && coll->b[1] == 1 && coll->b[2] == 1 && coll->b[3] == 1 && coll->b[4] == 1) {
			return TRUE;
		}
	}

    return FALSE;
}
//returns true if given point is a distinguished point
__device__ int isDistinguishedPoint(BYTE* point) {
    return point[0] == 0 && point[1] == 0;
}
//finds a distinguished point starting from some starting seed point
__device__ int findDistinguishedPoint(BYTE* seed, DIST_POINT* output) {

	BYTE* cur_val = (BYTE*) malloc(HASHSIZE);
	DIST_POINT* p = (DIST_POINT*) malloc (sizeof(DIST_POINT));
	//watch out for overflow later on
	unsigned long long numSteps = 0;
	//printf("My seed is: ");
	//print_hex(seed);
	memcpy(cur_val, seed, HASHSIZE);

	while (numSteps < 20 * 65536) {

		if (!isDistinguishedPoint(cur_val)) {
			//printf("Step: %d, Hash: ", numSteps);
			//print_hex(cur_val);
			hash(cur_val, HASHSIZE, cur_val);
			truncateHash(cur_val, TESTHASHSIZE);
			numSteps++;
		} else {
			memcpy(p->start, seed, HASHSIZE);
			//printf("Start Value: ");
			//print_hex(p->start);
			printf("DP. : ");
			print_hex(cur_val);
			memcpy(p->end, cur_val, HASHSIZE);
			p->steps = numSteps;
			//print_point(p);
			memcpy(output, p, sizeof(DIST_POINT));
			return 1;
		}
	}
	//LOWER THIS AMOUNT FOR THE SMALLER KERNELS?
	if (numSteps >= 20 * 65536) {
		printf("nothing found \n");
		return 0;
	}

	free(cur_val);
    free(p);
}
//locates collision given two distinguished point triples
__device__ HASHCOLLISION locateCollision(DIST_POINT* a, DIST_POINT* b) {

	DIST_POINT* more_steps = (DIST_POINT*) malloc(sizeof(DIST_POINT));
	DIST_POINT* less_steps = (DIST_POINT*) malloc(sizeof(DIST_POINT));

	BYTE* collision_hash_1 = (BYTE*) malloc(HASHSIZE);
	BYTE* collision_hash_2 = (BYTE*) malloc(HASHSIZE);

    HASHCOLLISION res;

	if (a->steps > b-> steps) {
		memcpy(more_steps, a, sizeof(DIST_POINT));
		memcpy(less_steps, b, sizeof(DIST_POINT));
	} else {
		memcpy(less_steps, a, sizeof(DIST_POINT));
		memcpy(more_steps, b, sizeof(DIST_POINT));
	}

	printf("More steps left: %llu, Less steps left: %llu\n", more_steps->steps, less_steps->steps);
	printf("More steps hash: ");
	print_hex(more_steps->start);
	printf("Less steps hash: ");
	print_hex(less_steps->start);

	while (more_steps->steps > less_steps->steps) {
		hash((more_steps->start), HASHSIZE, (more_steps->start));
		truncateHash(more_steps->start, TESTHASHSIZE);
		more_steps->steps -= 1;
	}

	

	if (more_steps->start == less_steps->start) {
		printf("ROBIN HOOD, do something about it");
		//TODO, no clue if this works

        memcpy(&res.a, more_steps->start, HASHSIZE);
        memcpy(&res.b, more_steps->start, HASHSIZE);

        return res;
	}

	while (hashNotEqual(more_steps->start, less_steps->start) && more_steps->steps > 0 && less_steps->steps > 0) {

		memcpy(collision_hash_1, more_steps->start, HASHSIZE);
		memcpy(collision_hash_2, less_steps->start, HASHSIZE);

		hash((more_steps->start), HASHSIZE, (more_steps->start));
		truncateHash(more_steps->start, TESTHASHSIZE);
		more_steps->steps -= 1;

		hash((less_steps->start), HASHSIZE, (less_steps->start));
		truncateHash(less_steps->start, TESTHASHSIZE);
		less_steps->steps -= 1;

		/*if (more_steps->steps == 20) {
			printf("=====\n");
			printf("More steps left: %llu, Less steps left: %llu\n", more_steps->steps, less_steps->steps);
			printf("More steps hash: ");
			print_hex(more_steps->start);
			printf("Less steps hash: ");
			print_hex(less_steps->start);
		}*/
	}

    memcpy(&res.a, collision_hash_1, HASHSIZE);
    memcpy(&res.b, collision_hash_2, HASHSIZE);

	printf("====================================\n");
	printf("More steps left: %llu, Less steps left: %llu\n", more_steps->steps, less_steps->steps);
	printf("More steps hash: ");
	print_hex(more_steps->start);
	printf("Less steps hash: ");
	print_hex(less_steps->start);
	printf("====================================\n");
	printf("Colliding values: \nValue 1: ");
	print_hex(collision_hash_1);
	printf("Value 2: ");
	print_hex(collision_hash_2);
	printf("Both hash to: ");
	print_hex(more_steps->start);

	printf("====================================\nPROOF: \n");
	hash(collision_hash_1, HASHSIZE, collision_hash_1);
	hash(collision_hash_2, HASHSIZE, collision_hash_2);
	truncateHash(collision_hash_1, TESTHASHSIZE);
	truncateHash(collision_hash_2, TESTHASHSIZE);
	printf("Value 1 hashes to: ");
	print_hex(collision_hash_1);
	printf("Value 2 hashes to: ");
	print_hex(collision_hash_2);

	//free malloced memory
	free(more_steps);
	free(less_steps);
	free(collision_hash_1);
	free(collision_hash_2);

    return res;
}
//finds a collision given some seeding state and memory for the distinguished points
__device__ HASHCOLLISION findCollision(DIST_POINT* dist_points, curandState* state) {

    //BYTE seed[HASHSIZE];
	BYTE* seed = (BYTE*) malloc(HASHSIZE);

    DIST_POINT* point = (DIST_POINT*) malloc (sizeof(DIST_POINT));

    while (1) {
        for (int i = 0; i < HASHSIZE; i++) {
			//set each byte of the seed to random value between 0 and 255
			float myRandF = curand_uniform(state);
			myRandF *= 255.999999;
			seed[i] = (BYTE) truncf(myRandF);
			//printf("%d, ", seed[i]);
        }
		//printf("\n");
		truncateHash(seed, TESTHASHSIZE);
		printf("Seed: ");
		print_hex(seed);


        if (findDistinguishedPoint(seed, point) == 0) {
			continue;
		}
        print_point(point);
		printf("=========================================\n");
		//NOTE: CHANGE THE INDEX TO LAST 2 BYTES NOT TRUNCATED AWAY
        int index = (point->end[3])*256 + point->end[4];//STORE AT CORRECT MEMORY LOCATION
        //printf("Collision?: %d\n", isCollision(&dist_points[index], point));
        if (isCollision(&dist_points[index], point)) {
            printf("FOUND COLLISION\n");
			//locateCollision(&dist_points[index], point);
            HASHCOLLISION res = locateCollision(&dist_points[index], point);
			free(point);
			free(seed);
			printf("==========================\n END OF COLLISION ITERATION\n==========================\n ");
            return res;
        } else {
            memcpy(&dist_points[index], point, sizeof(DIST_POINT));
        }
    }
}
//finds hardcoded golden collision , TODO: use different function versions for fixed period of time.
__global__ void findGoldenCollision(DIST_POINT* dist_points, curandState* state) {
    int searching = TRUE;

    while (searching) {
        HASHCOLLISION coll = findCollision(dist_points, state);
        
        //TODO: check if this collision is indeed the golden one, if yes, abort and print out, if no, continue
        if (isGoldenCollision(&coll)) {
            printf("GOLDEN COLLISION FOUND, VALUES ARE:...todo");
            searching = FALSE;
        } else {
            printf("FOUND COLLISION, NOT GOLDEN ONE THO...");
        }
    }

}

int main() {

    cudaError_t error = cudaGetLastError();
    curandState* devState;

    //TODO: how to integrate a function version? can't use key. XOR?
    //unsigned long long functionVersion = 0;
    //seed determines which random strat values the threads will generate
    unsigned long long seed = 100;

    //allocate memory for the distinguished points
    DIST_POINT* dist_points = (DIST_POINT*) calloc(W, sizeof(DIST_POINT));
    DIST_POINT* d_dist_points;

    //TODO: we dont really need that memory on the host side, only on the device side (global?)
    cudaMalloc(&devState, sizeof(curandState));
    cudaMalloc(&d_dist_points, W*sizeof(DIST_POINT));

    //setup thread states
    setupStateKernel<<<B,T>>>(devState, seed);
    cudaDeviceSynchronize();

    error = cudaGetLastError();
    if (error != cudaSuccess) {
        printf("Error 1: %s \n", cudaGetErrorString(error));
    }

    //todo: generate fake golden collision
    
    //kernel call
    findGoldenCollision<<<B,T>>>(d_dist_points, devState);
    cudaDeviceSynchronize();

    error = cudaGetLastError();
    if (error != cudaSuccess) {
        printf("Error 1: %s \n", cudaGetErrorString(error));
    }

    //free allocated memory
    cudaFree(devState);
    cudaFree(d_dist_points);
    free(dist_points);

    return 0;
}
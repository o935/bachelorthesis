#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <cuda.h>
#include <curand_kernel.h>

//#define encrypted "htqovkogvqvkogkjgctfuqogxciwgceeqwpvqhjkufqkpiuqhjkuuwooqpuvqqfguuckpvjgecugqhvjgrtgrqhhowtfgtqhjkuengckpiwrqhvjgukpiwnctvtcigfaqhvjgcvmkpuqpdtqvjgtucvvtkpeqocnggcpfhkpcnnaqhvjgokuukqpyjkejjgjcfceeqornkujgfuqfgnkecvgnacpfuweeguuhwnnahqtvjgtgkipkpihcoknaqhjqnncpfyjcnguctgpqvhkujcpfvjgaowuvtgiwnctnaigvdcemvqvjguwthcegvqdtgcvjgvjgtgctgocpavarguqhyjcngugcejykvjvjgktqypejctcevgtkuvkeucpfxcuvnafkhhgtgpvukbguvjgoquvhcoqwudahctkuvjgdnwgyjcngvjgnctiguvcpkocnvqjcxggxgtnkxgfqpvjkurncpgvnqpigtvjcpcvgppkueqwtvkvcnuqjqnfuvjgtgeqtfhqtvjgoquvrqygthwnecnnkpvjgcpkocnmkpifqoyjkejkunqwfgtvjcpclgvgpikpg"
//text from king james bible!
#define encrypted "btjpxrmlxpcuvamlxicvjpibtwxvrcimlmtrpmtnmtnyvcjxcdxvmwmbtrjjpxamtngxrjbahuqctjpxqgmrjxvcijpxymggcijpxhbtwrqmgmaxmtnjpxhbtwrmyjpxqmvjcijpxpmtnjpmjyvcjxjpxtjpxhbtwracutjxtmtaxymrapmtwxnmtnpbrjpcuwpjrjvcufgxnpblrcjpmjjpxscbtjrcipbrgcbtryxvxgccrxnmtnpbrhtxxrrlcjxctxmwmbtrjmtcjpxvjpxhbtwavbxnmgcunjcfvbtwbtjpxmrjvcgcwxvrjpxapmgnxmtrmtnjpxrccjprmexvrmtnjpxhbtwrqmhxmtnrmbnjcjpxybrxlxtcifmfegctypcrcxdxvrpmggvxmnjpbryvbjbtwmtnrpcylxjpxbtjxvqvxjmjbctjpxvxcirpmggfxagcjpxn"
#define encryptedLen ((int)sizeof(encrypted)-1)
#define threads 325
#define totalBigrams 676

// a host function to extract and map 
// all the bigrams from a given file
__host__ void extractBigrams(unsigned long long int *scores) {
	FILE* bigramsFile = fopen("bigramParsed", "r");
	while(1){
		char tempBigram[2];
		unsigned long long int tempBigramScore = 0;
		if (fscanf(bigramsFile, "%s %llu", tempBigram, &tempBigramScore) < 2)
			 { break; } 
		scores[(tempBigram[0]-'a')*26 + tempBigram[1]-'a'] = tempBigramScore; 
	}
	fclose(bigramsFile);
}


// a helper function to return the maximum element
__host__ int getMaxElement(unsigned long long int *scores, int length) {
	unsigned long long int tempMaxScore = 0;
	int tempIndex = 0;	
	for (int j=0; j<length; ++j) {
		if (scores[j] > tempMaxScore) {
			tempIndex = j;
			tempMaxScore = scores[j];
		} 
	}	
	return tempIndex;
}


// translate a given index to a map of pair of distinct letters 
__host__ void translateIndexToLetters(int index, int* left, int* right) {

	int tempLeft = 0;
	int tempRight = tempLeft+1;
	
	while (index > 0) {
		tempRight += 1;
		index -= 1;
		if (tempRight == 26) {
			tempLeft += 1;
			tempRight = tempLeft + 1;
		}
	}
	
	*left = tempLeft;
	*right = tempRight;
}


// interchange left with maxLeft and right with maxRight
__host__ void climb(int* encrMap, int length, int left, int maxLeft, int right, int maxRight) {
	for (int j=0; j<length; ++j) {
		if (encrMap[j] == left)
			encrMap[j] = maxLeft;
		else if (encrMap[j] == maxLeft)
			encrMap[j] = left;
		else if (encrMap[j] == right)
				encrMap[j] = maxRight;
		else if (encrMap[j] == maxRight)
				encrMap[j] = right;
	}
}


// demap and print a given array of integer letters
__host__ void demap(int* encrMap, int length) {
	for (int j=0; j<length; ++j) {
		printf("%c", encrMap[j] + 'a');
	}
	printf("\n");
}

__global__ void K(unsigned long long int *d_scores, int *d_encrypted, int leftLetter, int rightLetter, unsigned long long int *d_results){
	int tempLeft = 0;
	int tempRight = tempLeft+1;
	int remainder = threadIdx.x;
	
	while (remainder > 0) {
		tempRight += 1;
		remainder -= 1;
		if (tempRight == 26) {
			tempLeft += 1;
			tempRight = tempLeft + 1;
		}
	}
	
	unsigned long long int total = 0;
	int detectorLeft = 0;
	int detectorRight = 0;
	for (int j=0; j<encryptedLen-1; ++j)
		{
			detectorLeft = d_encrypted[j];
			detectorRight = d_encrypted[j+1];
			if (d_encrypted[j] == leftLetter)
				detectorLeft = tempLeft;
			else if (d_encrypted[j] == tempLeft)
				detectorLeft = leftLetter;
			else if (d_encrypted[j] == rightLetter)
				detectorLeft = tempRight;
			else if (d_encrypted[j] == tempRight)
				detectorLeft = rightLetter;
			else if (d_encrypted[j+1] == leftLetter)
				detectorRight = tempLeft;
			else if (d_encrypted[j+1] == tempLeft)
				detectorRight = leftLetter;
			else if (d_encrypted[j+1] == rightLetter)
				detectorRight = tempRight;
			else if (d_encrypted[j+1] == tempRight)
				detectorRight = rightLetter;
			total += d_scores[detectorLeft*26 + detectorRight];
		}		
	d_results[threadIdx.x] = total;	
	if ((tempLeft == rightLetter) || (tempRight == leftLetter))
		d_results[threadIdx.x] = 0;
}

int main() 
{ 
	int encryptedMap[encryptedLen];
	
	unsigned long long int results[threads];
	
	// we map the encrypted message to integers
	for (int j=0; j<encryptedLen; ++j)
		encryptedMap[j] = encrypted[j] - 'a';
	
	unsigned long long int scores[totalBigrams];	
	extractBigrams(scores);
		
	int size = sizeof(unsigned long long int);
	unsigned long long int *d_scores;
	unsigned long long int *d_results;
	int *d_encrypted;
	
	// allocate memory blocks on the device
	cudaMalloc((void **)&d_scores, size*totalBigrams);
	cudaMalloc((void **)&d_results, size*threads);
	cudaMalloc((void **)&d_encrypted, sizeof(int)*encryptedLen);	
	
	// copy the scores array to the device memory (only once)
	cudaMemcpy(d_scores, scores, size*totalBigrams, cudaMemcpyHostToDevice);

    unsigned long long int maxmaxScore = 0;

    for (int k = 0; k < 500; k++) {
        srand(time(0));
        int leftLetter = 0;
        int rightLetter = 0;
        int tempMaxIndex = 0;
        int tempMaxLeft = 0;
        int tempMaxRight = 0;
        unsigned long long int maxScore = 0;
        
        for (int j=0; j<500; ++j) 	{
        
            // pick two random distinct letters; we make sure they do exists in the encrypted text
            leftLetter = encrypted[rand() % encryptedLen] - 'a';
            rightLetter = leftLetter;
            while (rightLetter == leftLetter)
                rightLetter = encrypted[rand() % encryptedLen] - 'a';
        
            // copy the encrypted msg array to the device memory (after each climbing)
            cudaMemcpy(d_encrypted, encryptedMap, sizeof(int)*encryptedLen, cudaMemcpyHostToDevice);
        
        
            K<<<1,threads>>>(d_scores, d_encrypted, leftLetter, rightLetter, d_results);
            cudaDeviceSynchronize();
        
            // copy the results array back to the host memory
            cudaMemcpy(results, d_results, size*threads, cudaMemcpyDeviceToHost);
        
            tempMaxIndex = getMaxElement(results, threads);
            
            if (results[tempMaxIndex] > maxScore) {
                maxScore = results[tempMaxIndex];
                tempMaxLeft = 0;
                tempMaxRight = 0;
                translateIndexToLetters(tempMaxIndex, &tempMaxLeft, &tempMaxRight);
                climb(encryptedMap, encryptedLen, leftLetter, tempMaxLeft, rightLetter, tempMaxRight);
                //printf("%d %llu\n", tempMaxIndex, results[tempMaxIndex]);
                //printf("%d %d %d %d\n", tempMaxLeft, tempMaxRight, leftLetter, rightLetter);
            }
        }

        if (maxScore > maxmaxScore) {
                    maxmaxScore = maxScore;
                    demap(encryptedMap, encryptedLen);
                    printf("\n");
                } 
    }
	
	
	
	// we free the allocated device memory 
	cudaFree(d_scores);
	cudaFree(d_encrypted);
	cudaFree(d_results);
	
	return 0;	
}

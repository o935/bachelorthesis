#include <stdio.h>

__global__ void F() {
    printf("Hello\n");
}

int main() {

    int deviceCount = 0;
    cudaError_t error_id = cudaGetDeviceCount(&deviceCount);

    if (error_id != cudaSuccess){
        printf("Error: %s\n%d devices detected\n", cudaGetErrorString(error_id), deviceCount);
        printf("Exiting...\n");
        exit(EXIT_FAILURE);
    }

    if (deviceCount == 0){
        printf("There are no available device(s) that support CUDA\n");
        exit(EXIT_FAILURE);
    }
    F<<<1,81>>>();
    cudaDeviceSynchronize();
    return 0;
}
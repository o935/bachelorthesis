#include <stdio.h>
#include <cuda.h>
#include <curand_kernel.h>

#define B 8
#define T 8

__global__ void setupKernel(curandState* state, unsigned long seed) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    curand_init(seed, idx, 0, &state[idx]);
}

__global__ void generate(curandState* globalState, float* devRandomValues) {
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    curandState localState = globalState[idx];
    float randF = curand_uniform(&localState);
    devRandomValues[idx] = randF;
    globalState[idx] = localState;
}

int main() {
    curandState* devStates;
    float* randomValues = new float[B*T];
    float* devRandomValues;

    //we allocate the required memory blocks
    cudaMalloc(&devStates, B*T*sizeof(curandState));
    cudaMalloc(&devRandomValues, B*T*sizeof(*randomValues));

    //initial setup
    setupKernel<<<B,T>>>(devStates, time(NULL));
    cudaDeviceSynchronize();

    //generator
    generate<<<B,T>>>(devStates, devRandomValues);
    cudaDeviceSynchronize();

    //extract the random values from the device
    cudaMemcpy(randomValues, devRandomValues, B*T*sizeof(*randomValues), cudaMemcpyDeviceToHost);

    //feedback
    for (int i = 0; i < B*T; i++) {
        printf("%f\n", randomValues[i]);
    }

    //cleaning up
    cudaFree(devRandomValues);
    cudaFree(devStates);
    delete randomValues;

    return 0;
}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cuda.h>
#include <curand_kernel.h>

#define B ((int)16)
#define T ((int)26)
#define THREADS ((int)B*T)
#define CLIMBINGS ((int)5000)
#define ALPHABET ((int)26)
#define totalBigrams ((int)ALPHABET*ALPHABET)

#define encrypted "djrggrygdudrdunkluejqxgahbvhxbixnadjngqniqwdxqaqguaeouludbdndjngqsjngqlnaeoulhdunktqiuqghkbnxtukhxbaqdjntnihkhobgughktsjngqgnordunkxqmruxqgdjqenggqggunknionkzaqgghzqghktarljduaqhktgdrtbinxdrkhdqobdjqanxqtuiiulrodgrygdudrdunkluejqxghxqxhxqobrgqtinxauoudhxberxengqgnkhllnrkdnidjqduaqhktlhxqxqmruxqtinxqkluejqxukzhkttqluejqxukz"
#define ENCRYPTEDLEN ((int) sizeof(encrypted)-1)

__host__ void extractBigrams(long long int *scores) {
	FILE* bigramsFile = fopen("bigramParsed", "r");
	while(1){
		char tempBigram[2];
		long long int tempBigramScore = 0;
		if (fscanf(bigramsFile, "%s %llu", tempBigram, &tempBigramScore) < 2)
			 { break; } 
		scores[(tempBigram[0]-'a')*ALPHABET + tempBigram[1]-'a'] = tempBigramScore; 
	}
	fclose(bigramsFile);
}

__host__ int getMaxElement(long long int *scores, int length) {
	long long int tempMaxScore = 0;
	int tempIndex = 0;	
	for (int j=0; j<length; ++j) {
		if (scores[j] > tempMaxScore) {
			tempIndex = j;
			tempMaxScore = scores[j];
		} 
	}	
	return tempIndex;
}

__host__ __device__ void demap(int* encrMap) {
    for (int i = 0; i < ENCRYPTEDLEN; i++) {
        printf("%c", encrMap[i] + 'a');
    }
    printf("\n");
}

__host__ long long int candidateScore(int* decrMsg, long long int* scores) {
    long long int total = 0;
    for (int i = 0; i < ENCRYPTEDLEN-1; i++) {
        total += scores[ALPHABET*decrMsg[i] + decrMsg[i+1]];
    }
    return total;
}

__global__ void setupKernel(curandState* state, long seed) {
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    curand_init(seed, idx, 0, &state[idx]);
}

__global__ void K(long long int *d_scores, int *d_encrypted, curandState* globalState, int *d_decrypted) {
    __shared__ long long int shared_scores[totalBigrams];

    int localBufferEncrypted[ENCRYPTEDLEN];
    for (int i = 0; i < ENCRYPTEDLEN; i++)
        localBufferEncrypted[i] = d_encrypted[i];

    int idx = blockIdx.x*blockDim.x + threadIdx.x;

    //only the first 26 threads, each responsible for one bigram of the form A?, B?, C? etc.
    //initalize the shared_scores array
    if (threadIdx.x <= 25) {
        for (int i = 0; i < ALPHABET; i++) {
            shared_scores[threadIdx.x*ALPHABET + i] = d_scores[threadIdx.x*ALPHABET + i];
        }
    }

    __syncthreads();

    curandState localState = globalState[idx];

    long long int delta = 0;
    float randF = 0;
    int leftLetter = 0;
    int rightLetter = 0;
    int pair[2];

    for (int cycles = 0; cycles < CLIMBINGS; cycles++) {
        delta = 0;
        randF = curand_uniform(&localState);
        leftLetter = randF*ALPHABET;
        rightLetter = leftLetter;

        while (rightLetter == leftLetter) {
            randF = curand_uniform(&localState);
            rightLetter = randF*ALPHABET;
        }

        for (int j = 0; j < ENCRYPTEDLEN-1; j++) {
            pair[0] = localBufferEncrypted[j];
            pair[1] = localBufferEncrypted[j+1];
            delta -= shared_scores[ALPHABET*pair[0] + pair[1]];

            if (pair[0] == leftLetter)
                pair[0] = rightLetter;
            else if (pair[0] == rightLetter)
                pair[0] = leftLetter;

            if (pair[1] == leftLetter)
                pair[1] = rightLetter;
            else if (pair[1] == rightLetter)
                pair[1] = leftLetter;

            delta += shared_scores[ALPHABET*pair[0] + pair[1]];
        }

        if (delta > 0) {
            for (int j = 0; j < ENCRYPTEDLEN; j++) {
                if (localBufferEncrypted[j] == leftLetter)
                    localBufferEncrypted[j] = rightLetter;
                else if (localBufferEncrypted[j] == rightLetter)
                    localBufferEncrypted[j] = leftLetter;
            }
        }
    }

    for (int j = 0; j < ENCRYPTEDLEN; j++)
        d_decrypted[idx*ENCRYPTEDLEN+j] = localBufferEncrypted[j];
}

int main() {

    int encryptedMap[ENCRYPTEDLEN];

    for (int j = 0; j < ENCRYPTEDLEN; j++) {
        encryptedMap[j] = encrypted[j] - 'a';
    }

    long long int scores[totalBigrams];
    extractBigrams(scores);

    long long int *d_scores;
    int *d_encrypted, *d_decrypted;
    static int decrypted[ENCRYPTEDLEN*THREADS];

    cudaMalloc((void**) &d_scores, sizeof(long long int)*totalBigrams);
    cudaMalloc((void**) &d_encrypted, sizeof(int)*ENCRYPTEDLEN);
    cudaMalloc((void**) &d_decrypted, sizeof(int)*ENCRYPTEDLEN*THREADS);

    cudaError_t error = cudaGetLastError();
    if(error != cudaSuccess) {
        printf("CUDA Error (1): %s\n", cudaGetErrorString(error));
        exit(-1);
    }

    cudaMemcpy(d_scores, scores, sizeof(long long int)*totalBigrams, cudaMemcpyHostToDevice);
    cudaMemcpy(d_encrypted, encryptedMap, sizeof(int)*ENCRYPTEDLEN, cudaMemcpyHostToDevice);

    error = cudaGetLastError();
    if(error != cudaSuccess) {
        printf("CUDA Error (2): %s\n", cudaGetErrorString(error));
        exit(-1);
    }

    curandState* devStates;
    cudaMalloc(&devStates, THREADS*sizeof(curandState));

    error = cudaGetLastError();
    if(error != cudaSuccess) {
        printf("CUDA Error (3): %s\n", cudaGetErrorString(error));
        exit(-1);
    }

    setupKernel<<<B,T>>>(devStates, time(NULL));
    cudaDeviceSynchronize();

    error = cudaGetLastError();
    if(error != cudaSuccess) {
        printf("CUDA Error (4): %s\n", cudaGetErrorString(error));
        exit(-1);
    }

    K<<<B,T>>>(d_scores, d_encrypted, devStates,d_decrypted);
    cudaDeviceSynchronize();

    error = cudaGetLastError();
    if(error != cudaSuccess) {
        printf("CUDA Error (5): %s\n", cudaGetErrorString(error));
        exit(-1);
    }

    cudaMemcpy(decrypted, d_decrypted, sizeof(int)*ENCRYPTEDLEN*THREADS, cudaMemcpyDeviceToHost);

    int bestCandidate = 0;
    long long int bestScore = 0;

    for (int i = 0; i < THREADS; i++) {
        long long int currentScore = candidateScore(&decrypted[ENCRYPTEDLEN*i], scores);
        if (currentScore > bestScore) {
            bestScore = currentScore;
            bestCandidate = i;
        }
    }

    printf("Best Candidate score: %llu\n", bestScore);
    demap(&decrypted[ENCRYPTEDLEN*bestCandidate]);

    cudaFree(d_scores);
    cudaFree(d_encrypted);
    cudaFree(d_decrypted);
    cudaFree(devStates);

    return 0;
}






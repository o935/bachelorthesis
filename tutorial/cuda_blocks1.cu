#include <stdio.h>

__device__ int powr (int x, int n) {
    int res = x;
    //does it also work same for i--?
    for (int i = n; i > 1; i--) {
        res *= x;
    }
    return (n==0)?1:res;
}

__global__ void L() {
    if (threadIdx.x != 0 || blockIdx.x != 0) {
        printf("%d powered to %d is: %d \n", threadIdx.x, blockIdx.x, powr(threadIdx.x, blockIdx.x));
    }
}

int main() {
    L<<<2,4>>>();
    cudaDeviceSynchronize();
    return 0;
}
#include <stdio.h>
#define threads 81

__global__ void K(int *d_seeds) {
    d_seeds[threadIdx.x] = d_seeds[threadIdx.x] * d_seeds[threadIdx.x];
}

int main() {

    int seeds[threads];

    for (int j=0; j < threads; j++) {
        seeds[j] = j;
    }
    
    int size = sizeof(int);
    int *d_seeds;

    //allocate memory block on device
    cudaMalloc((void **) &d_seeds, size * threads);

    cudaMemcpy(d_seeds, seeds, size*threads, cudaMemcpyHostToDevice);

    K<<<1,threads>>>(d_seeds);

    cudaDeviceSynchronize();

    //copy results array back to the host memory

    cudaMemcpy(seeds, d_seeds, size*threads, cudaMemcpyDeviceToHost);
    cudaFree(d_seeds);

    for (int j=0; j < threads; j++) {
        printf("Cell %d is equal to: %d\n", j, seeds[j]);
    }

    return 0;
}
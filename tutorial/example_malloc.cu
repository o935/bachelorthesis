#include <stdio.h>
#define threads 81

__global__ void H(int *d_seeds) {
    int element = d_seeds[threadIdx.x];
    printf("%d squared is: %d \n", element, element*element);
}

int main () {

    int seeds[threads];

    for (int j=0; j<threads; ++j) {
        seeds[j] = j;
    }

    int size = sizeof(int);
    int *d_seeds;

    //allocate memory block on device
    cudaMalloc((void **) &d_seeds, size*threads);

    //copy seeds array to device memory
    //from host memory block "seeds" to device memory block "d_seeds" (pointed to by d_seeds)
    cudaMemcpy(d_seeds, seeds, size*threads, cudaMemcpyHostToDevice);

    H<<<1,threads>>>(d_seeds);
    cudaDeviceSynchronize();
    cudaFree(d_seeds);
    return 0;
}
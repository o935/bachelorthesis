#include "aes.cu"

__host__ void convert(uint8_t* buf, char* input) {
    int len = strlen(input);
    for (int r = 0; r < len; r++) {
        buf[r] = input[r];
    }

}

int main() {

    uint8_t *buf; 
    unsigned long numbytes;
    int padding;
    uint8_t key[32];
    //input and key generation
    for (int i = 0; i < sizeof(key);i++) key[i] = 65;
    char input[] = "example input";

    //error handling
    int deviceCount;
    cudaError_t error_id = cudaGetDeviceCount(&deviceCount);
    if (error_id != cudaSuccess){
        printf("Error: %s\n", cudaGetErrorString(error_id));
        printf("Exiting...\n");
        exit(EXIT_FAILURE);
    }
    if (deviceCount == 0){
        printf("There are no available device(s) that support CUDA\n");
        exit(EXIT_FAILURE);
    }


    //handle input string
    numbytes = strlen(input);
    padding = (AES_BLOCK_SIZE - (numbytes % AES_BLOCK_SIZE)) % 16;
    numbytes += padding;
    buf = (uint8_t*)calloc(numbytes, sizeof(uint8_t));
    if (buf == NULL) exit(1);
    convert(buf, input);

    //printing
    printf("=====Input Size: %lu Bytes, ", strlen(input));
    printf("Padding size: %d Bytes, New size: %lu=====\n\n", padding, numbytes);
    printf("----Given Input----\n");
    for (int l = 0; l < numbytes; l++) {
        printf("%02x ", buf[l]);
    }
    printf("\n----%d Bit Secret Key----\n", ((int) sizeof(key) * 8));
    for (int ii = 0; ii < sizeof(key); ii++) {
        printf("%02x ", key[ii]);
    }

    // encryption
    uint8_t *buf_d;
    uint8_t *ctx_key_d, *ctx_enckey_d;
    //copy sbox into constant memory of device
    cudaMemcpyToSymbol(sbox, sbox, sizeof(uint8_t)*256);
    //initialize key
    aes256_init(key);
    //allocate memory on device for text and key
    cudaMalloc((void**)&buf_d, numbytes);
    cudaMalloc((void**)&ctx_enckey_d, sizeof(ctx_enckey));
    cudaMalloc((void**)&ctx_key_d, sizeof(ctx_key));
    //copy text and key to device memory
    cudaMemcpy(buf_d, buf, numbytes, cudaMemcpyHostToDevice);
    cudaMemcpy(ctx_enckey_d, ctx_enckey, sizeof(ctx_enckey), cudaMemcpyHostToDevice);
    cudaMemcpy(ctx_key_d, ctx_key, sizeof(ctx_key), cudaMemcpyHostToDevice);
    //calculate required block and grid dimensions
    dim3 dimGrid(ceil((double)numbytes / (double)(THREADS_PER_BLOCK * AES_BLOCK_SIZE)));
    dim3 dimBlock(THREADS_PER_BLOCK);
    aes256_encrypt_ecb<<<dimGrid, dimBlock>>>(buf_d, numbytes, ctx_enckey_d, ctx_key_d);
    //copy encrypted text and key from device to host
    cudaMemcpy(buf, buf_d, numbytes, cudaMemcpyDeviceToHost);
    cudaMemcpy(ctx_enckey, ctx_enckey_d, sizeof(ctx_enckey), cudaMemcpyDeviceToHost);
    cudaMemcpy(ctx_key, ctx_key_d, sizeof(ctx_key), cudaMemcpyDeviceToHost);

    //printing
    printf("\n----Encr. Input:----\n");
    for (int k = 0; k < numbytes; k++) {
        printf("%02x ", buf[k]);
    }
    printf("\n-------------------\n");

    //free memory
    cudaFree(buf_d);
    cudaFree(ctx_key_d);
    cudaFree(ctx_enckey_d);
    free(buf);

    return EXIT_SUCCESS;
}
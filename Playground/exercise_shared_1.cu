#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <inttypes.h>
#include <curand_kernel.h>
#include <cuda.h>

#define B 2
#define T 2

__global__ void setupKernel(curandState* state, unsigned long seed) {
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    curand_init(seed, idx, 0, &state[idx]);
}

__global__ void generate(curandState* globalState, float* devRandomValues) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    curandState localState = globalState[idx];
    float randF = curand_uniform(&localState);
    devRandomValues[idx] = randF;
    //printf("%f - ", randF);
    globalState[idx] = localState;
}

__global__ void K(curandState* globalState) {
    __shared__ float shared_max[T];
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    curandState localState = globalState[idx];
    shared_max[threadIdx.x] = curand_uniform(&localState);

    for (int i = 0; i < T; i++) {
        printf("Block %d: Thread %d says, thread %d has value %f\n", blockIdx.x, threadIdx.x, i, shared_max[i]);
    }
    
}

int main() {
    curandState* devStates;
    float* randomValues = new float[B*T];
    float* devRandomValues;

    //allocate memory blocks
    cudaMalloc(&devStates, B*T*sizeof(curandState));
    cudaMalloc(&devRandomValues, B*T*sizeof(*randomValues));

    //initial setup
    setupKernel<<<B,T>>>(devStates, time(NULL));
    cudaDeviceSynchronize();

    //generator
    K<<<B,T>>>(devStates);
    cudaDeviceSynchronize();

    //extract random values from the device
    cudaMemcpy(randomValues, devRandomValues, B*T*sizeof(*randomValues), cudaMemcpyDeviceToHost);

    //feedback
    for (int i = 0; i < B*T; ++i) {
        printf("%f\n", randomValues[i]);
    }

    //cleanup
    cudaFree(devRandomValues);
    cudaFree(devStates);
    delete randomValues;

    return 0;
}
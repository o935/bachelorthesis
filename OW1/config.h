#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <memory.h>
#include <math.h>

#define MD2_BLOCK_SIZE 16
#define HASHSIZE 16
//currently tested hashsize in bytes
#define TESTHASHSIZE 3
//number of bytes that a distinguished point must have zeroes
#define ZEROBYTES 1
#define ZEROBITS 6

typedef unsigned char BYTE;
typedef unsigned int WORD;
typedef unsigned long long LONG;

//number of Distinguished points that can be stored in memory
#define W 1024 //65536

//number of threads and blocks
#define T 64 //64
#define B 48 //48
#define THREADS (int(B*T))

//factor which determines maximum pathlength when looking for distinguished points
#define TOTALSTEPSFACTOR 20
//number of iterations of finding distinguished points and locating collisions
#define ITERATIONS 3//14 //80
//initial function version (salt for hashfunction)
#define INITIAL_FUCTIONVERSION 7428838263
//maximum number of functionversions that we try
#define NUMBER_OF_FUNCTIONVERSIONS 3000000 //1000
//number of iterations  of distinguished point searches per thread per kernel call
#define DIST_POINT_ROUNDS 1
//size of a block_dist_point memory

#define TRUE 1
#define FALSE 0

typedef struct  {
	BYTE data[16];
	BYTE state[48];
	BYTE checksum[16];
	int len;
} CUDA_MD2_CTX;

typedef struct  {
	BYTE start[TESTHASHSIZE];
	BYTE end[TESTHASHSIZE];
	unsigned long long steps;
} DIST_POINT;

typedef struct  {
    DIST_POINT a;
    DIST_POINT b;
} COLLISION;

typedef struct  {
    BYTE a[TESTHASHSIZE];
    BYTE b[TESTHASHSIZE];
} HASHCOLLISION;

typedef struct  {
	BYTE gc_hash1[TESTHASHSIZE];
	BYTE gc_hash2[TESTHASHSIZE];
	BYTE gc_res[TESTHASHSIZE];
} GOLDENCOLLISION;

typedef struct {
	BYTE hash[TESTHASHSIZE];
} HASH;
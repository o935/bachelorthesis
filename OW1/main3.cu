#include <device_launch_parameters.h>
#include <cuda_runtime.h>
#include <cuda.h>
#include <curand_kernel.h>
#include <time.h>
#include <cuda_profiler_api.h>

#include "md2.cu"

__global__ void setupStateKernel(curandState* state, unsigned long seed, unsigned long functionVersion) {
    //TODO: maybe add a salt (possibly also the functionVersion?)
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    curand_init(seed, idx, functionVersion, &state[idx]);
}

__global__ void findDistinguishedPoint(curandState* states, DIST_POINT* d_dist_points, unsigned long functionVersion) {
    unsigned long long numSteps = 0;
    unsigned long long totalSteps = pow(2, ZEROBITS) * TOTALSTEPSFACTOR;

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    curandState localState = states[idx];

    DIST_POINT dist_points[DIST_POINT_ROUNDS];
    DIST_POINT point;

    for (int round = 0; round < DIST_POINT_ROUNDS; round++) {
        numSteps = 0;
        BYTE cur_val[TESTHASHSIZE];
        BYTE start[TESTHASHSIZE];

        for (int i = 0; i < TESTHASHSIZE; i++) {
            float myRandF = curand_uniform(&localState);
			myRandF *= 255.999999;
			start[i] = (BYTE) truncf(myRandF);
            cur_val[i] = start[i];
        }

        while (numSteps < totalSteps) {
            if (!isDistinguishedPointBits(cur_val)) {
                hash(cur_val, TESTHASHSIZE, cur_val, functionVersion);
                numSteps++;
            } else {
                for (int cp = 0; cp < TESTHASHSIZE; cp++) {
                    point.start[cp] = start[cp];
                    point.end[cp] = cur_val[cp];
                }
                point.steps = numSteps;
                break;
            }
        }

        if (numSteps >= totalSteps) {
            point.steps = 0;
        }

        memcpy(&dist_points[round], &point, sizeof(DIST_POINT));
    }

    memcpy(&d_dist_points[idx*DIST_POINT_ROUNDS], &dist_points, DIST_POINT_ROUNDS*sizeof(DIST_POINT));
}

__global__ void findCollision(COLLISION* d_collisions, int num_cols, HASHCOLLISION* results, unsigned long functionVersion) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    //+1 to round up such that certainly all are covered, might lead to some threads doing nothing though..
    int cols_per_thread = (num_cols / THREADS) + 1;

    if (idx*cols_per_thread >= num_cols)
        return;

    //TODO: maybe also only length cols_per_thread, but then zero the rest?
    COLLISION col[DIST_POINT_ROUNDS];

    for (int i = 0; i < cols_per_thread; i++) {
        //last threads might not have cols_per_threads collisions...
        if (idx*cols_per_thread+i >= num_cols)
            break;
        //memcpy(&col[i], &d_collisions[idx*cols_per_thread+i], sizeof(COLLISION));
        //basically handwritten memcpy of the COLLISION
        for (int j = 0; j < TESTHASHSIZE; j++) {
            col[i].a.start[j] = d_collisions[idx*cols_per_thread+i].a.start[j];
            col[i].a.end[j] = d_collisions[idx*cols_per_thread+i].a.end[j];
            col[i].b.start[j] = d_collisions[idx*cols_per_thread+i].b.start[j];
            col[i].b.end[j] = d_collisions[idx*cols_per_thread+i].b.end[j];
        }
        col[i].a.steps = d_collisions[idx*cols_per_thread+i].a.steps;
        col[i].b.steps = d_collisions[idx*cols_per_thread+i].b.steps;
    }
    for (int i = 0; i < cols_per_thread; i++) {
        if (idx*cols_per_thread+i >= num_cols)
            break;

        BYTE collision_hash_1[TESTHASHSIZE];
        BYTE collision_hash_2[TESTHASHSIZE];
        BYTE cur_hash_a[TESTHASHSIZE];
        BYTE cur_hash_b[TESTHASHSIZE];

        for (int cp = 0; cp < TESTHASHSIZE; cp++) {
            cur_hash_a[cp] = col[i].a.start[cp];
            cur_hash_b[cp] = col[i].b.start[cp];
        }

        while (col[i].a.steps < col[i].b.steps) {
            hash (cur_hash_b, TESTHASHSIZE, cur_hash_b, functionVersion);
            col[i].b.steps -= 1;
        }

        //Robin hoods
        if (hashEqual(cur_hash_a, cur_hash_b)) {
            //TODO: make this manual, not with memset...
            for (int cp = 0; cp < TESTHASHSIZE; cp++) {
                results[idx*DIST_POINT_ROUNDS+i].a[cp] = 0;
                results[idx*DIST_POINT_ROUNDS+i].b[cp] = 0;
            }
            return;
        }

        while (hashNotEqual(cur_hash_a, cur_hash_b) && col[i].a.steps > 0 && col[i].b.steps > 0) {
            for (int cp = 0; cp < TESTHASHSIZE; cp++) {
                collision_hash_1[cp] = cur_hash_b[cp];
                collision_hash_2[cp] = cur_hash_a[cp];
            }

            hash(cur_hash_b, TESTHASHSIZE, cur_hash_b,functionVersion);
            col[i].b.steps -= 1;

            hash(cur_hash_a, TESTHASHSIZE, cur_hash_a,functionVersion);
            col[i].a.steps -= 1;
        }

        for (int cp = 0; cp < TESTHASHSIZE; cp++) {
            results[idx*DIST_POINT_ROUNDS+i].a[cp] = collision_hash_1[cp];
            results[idx*DIST_POINT_ROUNDS+i].b[cp] = collision_hash_2[cp];
        }
    }

}

int main() {

    clock_t gc_start = clock();
    clock_t gc_end;

    unsigned long functionVersion = INITIAL_FUCTIONVERSION;

    curandState* d_states;
    DIST_POINT* h_memory;
    DIST_POINT* h_dist_points;
    DIST_POINT* d_dist_points;
    COLLISION* h_collisions;
    COLLISION* d_collisions;
    HASHCOLLISION* h_collision_results;
    HASHCOLLISION* d_collision_results;

    h_memory            = (DIST_POINT*)     calloc(W, sizeof(DIST_POINT));
    h_dist_points       = (DIST_POINT*)     calloc(THREADS, DIST_POINT_ROUNDS*sizeof(DIST_POINT));
    h_collisions        = (COLLISION*)      calloc(THREADS, DIST_POINT_ROUNDS*sizeof(COLLISION));
    h_collision_results = (HASHCOLLISION*)  calloc(THREADS, DIST_POINT_ROUNDS*sizeof(HASHCOLLISION));

    if (cudaSuccess != cudaMalloc(&d_states, THREADS*sizeof(curandState))) {
        printf("Error d_states malloc: %d\n", cudaGetLastError());
    }
    if (cudaSuccess != cudaMalloc(&d_dist_points, THREADS*DIST_POINT_ROUNDS*sizeof(DIST_POINT))) {
        printf("Error d_dist_points malloc: %d\n", cudaGetLastError());
    }
    if (cudaSuccess != cudaMalloc(&d_collisions, THREADS*DIST_POINT_ROUNDS*sizeof(COLLISION))) {
        printf("Error d_collisions malloc: %d\n", cudaGetLastError());
    }
    if (cudaSuccess != cudaMalloc(&d_collision_results, THREADS*DIST_POINT_ROUNDS*sizeof(HASHCOLLISION))) {
        printf("Error d_collisions_results malloc: %d\n", cudaGetLastError());
    }

    while (functionVersion < INITIAL_FUCTIONVERSION + NUMBER_OF_FUNCTIONVERSIONS) {

        unsigned long long totalCollisions                  = 0;
        unsigned long long totalDistinctDistinguishedPoints = 0;
        unsigned long long totalDistinguishedPoints         = 0;
        unsigned long long totalRobinHoods                  = 0;

        unsigned long long totalTrueCollisions              = 0;

        memset(h_memory, 0, W*sizeof(DIST_POINT));
        memset(h_dist_points, 0, THREADS*DIST_POINT_ROUNDS*sizeof(DIST_POINT));
        memset(h_collisions, 0, THREADS*DIST_POINT_ROUNDS*sizeof(COLLISION));
        memset(h_collision_results, 0, THREADS*DIST_POINT_ROUNDS*sizeof(HASHCOLLISION));
        cudaMemset(d_dist_points, 0, THREADS*DIST_POINT_ROUNDS*sizeof(DIST_POINT));
        cudaMemset(d_collisions, 0, THREADS*DIST_POINT_ROUNDS*sizeof(COLLISION));
        cudaMemset(d_collision_results, 0, THREADS*DIST_POINT_ROUNDS*sizeof(HASHCOLLISION));

        for (int iter = 0; iter < ITERATIONS; iter++) {

            clock_t iter_start = clock();

            unsigned long pointsFound           = 0;
            unsigned long distinctPointsFound   = 0;
            unsigned long collisionsFound       = 0;
            unsigned long robinHoods            = 0;

            unsigned long trueCollisionsFound   = 0;

            setupStateKernel<<<B,T>>>(d_states, time(NULL) + rand(), functionVersion);
            cudaDeviceSynchronize();
            cudaError_t error = cudaGetLastError();
            if (error != cudaSuccess) {
                printf("Error setUpKernel: %s\n", cudaGetErrorString(error));
                exit(-1);
            }

            findDistinguishedPoint<<<B,T>>>(d_states, d_dist_points, functionVersion);
            cudaDeviceSynchronize();
            error = cudaGetLastError();
            if (error != cudaSuccess) {
                printf("Error findDistinguishedPoint Kernel: %s\n", cudaGetErrorString(error));
                exit(-1);
            }

            cudaMemcpy(h_dist_points, d_dist_points, THREADS*DIST_POINT_ROUNDS*sizeof(DIST_POINT), cudaMemcpyDeviceToHost);

            for (int i = 0; i < THREADS*DIST_POINT_ROUNDS; i++) {
                DIST_POINT point = h_dist_points[i];

                if (point.steps == 0)
                    continue;
                
                pointsFound++;
                int index = ((point.end[TESTHASHSIZE-2]) * 256 + point.end[TESTHASHSIZE-1]) % W;

                if (h_memory[index].steps == 0 || hashNotEqual(h_memory[index].end, point.end)) {
                    //TODO: not true if W is smaller than #distpoints
                    distinctPointsFound++;
                } else if (isCollision(&h_memory[index], &point)) {
                    if (h_memory[index].steps > point.steps) {
                        memcpy(&h_collisions[collisionsFound].a, &point, sizeof(DIST_POINT));
                        memcpy(&h_collisions[collisionsFound].b, &h_memory[index], sizeof(DIST_POINT));
                    } else {
                        memcpy(&h_collisions[collisionsFound].a, &h_memory[index], sizeof(DIST_POINT));
                        memcpy(&h_collisions[collisionsFound].b, &point, sizeof(DIST_POINT));
                    }
                    collisionsFound++;
                } else {
                    continue;
                }

                memcpy(&h_memory[index], &point, sizeof(DIST_POINT));
            }

            cudaMemcpy(d_collisions, h_collisions, THREADS*DIST_POINT_ROUNDS*sizeof(COLLISION), cudaMemcpyHostToDevice);
            findCollision<<<B,T>>>(d_collisions, collisionsFound, d_collision_results, functionVersion);
            cudaDeviceSynchronize();
            error = cudaGetLastError();
            if (error != cudaSuccess) {
                printf("Error findCollision Kernel: %s\n", cudaGetErrorString(error));
                exit(-1);
            }
            cudaMemcpy(h_collision_results, d_collision_results, THREADS*DIST_POINT_ROUNDS*sizeof(HASHCOLLISION), cudaMemcpyDeviceToHost);

            for (int j = 0; j < collisionsFound; j++) {
                if (hashEqual(h_collision_results[j].a, h_collision_results[j].b)) {
                    //TODO: temporary solution, differ robin hoods from "not founds"
                    //print_hex(h_collision_results[j].a);
                    robinHoods++;
                    continue;
                }
                trueCollisionsFound++;

                if (isGoldenCollision(h_collision_results[j])) {
                    printf("FOUND GOLDEN COLLISION:\n");
                    print_hex(h_collision_results[j].a);
                    print_hex(h_collision_results[j].b);
                    gc_end = clock();
                    goto end;
                }
            }

        totalCollisions += collisionsFound;
        totalDistinctDistinguishedPoints += distinctPointsFound;
        totalDistinguishedPoints += pointsFound;
        totalRobinHoods += robinHoods;
        totalTrueCollisions += trueCollisionsFound;

        clock_t iter_end = clock();

        if (iter == ITERATIONS - 1) {

            printf("%luth function version, ", functionVersion - INITIAL_FUCTIONVERSION);
            printf("%dth iteration (%fs):", iter, (iter_end - iter_start) / (float) CLOCKS_PER_SEC);
            printf("P: %llu, D: %llu, C: %llu, R: %llu, TC: %llu\n", totalDistinguishedPoints, 
                totalDistinctDistinguishedPoints, totalCollisions, totalRobinHoods, totalTrueCollisions);
        }

        }
        functionVersion++;
    }

    end: 
    //printf("Total time: %f\n", (gc_end - gc_start) / (float) CLOCKS_PER_SEC);
    //printf("Needed %lu function versions", functionVersion - INITIAL_FUCTIONVERSION);
    free(h_memory);
    free(h_dist_points);
    free(h_collision_results);
    free(h_collisions);
    cudaFree(d_dist_points);
    cudaFree(d_states);
    cudaFree(d_collision_results);
    cudaFree(d_collisions);

    return 0;
}
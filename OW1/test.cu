#include <device_launch_parameters.h>
#include <cuda_runtime.h>
#include <cuda.h>
#include <curand_kernel.h>

#include <time.h>

#include "md2.cu"

__global__ void testKernel(BYTE* hashing, unsigned long functionVersion) {
    //BYTE testhash[5] = {0x00,0x11,0x22,0x33,0x44};
    hash(hashing, TESTHASHSIZE, hashing, functionVersion);
}

__global__ void testKernelTwice(BYTE* hashing, unsigned long functionVersion) {
    for (int i = 0; i < 5000; i++) {
        hash(hashing, TESTHASHSIZE, hashing, functionVersion+i);
    }
}

int main() {
    unsigned long functionVersion = 39428033;

    DIST_POINT* p = (DIST_POINT*) malloc(sizeof(DIST_POINT)*16);

    DIST_POINT x;
    x.steps = 1;
    x.start[0] = 0x11;
    x.start[1] = 0x11;
    x.start[2] = 0x11;
    x.end[0] = 0x21;
    x.end[1] = 0x21;
    x.end[2] = 0x21;
    printf("%p",  &p[0]);
    p[0] = x;
    printf("%p",  &p[0]);

    print_point(p[0]);



    /*
    BYTE* hash = (BYTE*) malloc(16);
    BYTE* d_hash;

    cudaMalloc(&d_hash, 16);

    cudaMemcpy(d_hash, hash, 16, cudaMemcpyHostToDevice);

    clock_t t1 = clock();
    testKernelTwice<<<B,T>>>(d_hash,functionVersion);
    cudaDeviceSynchronize();
    cudaMemcpy(hash, d_hash, 16, cudaMemcpyDeviceToHost);
    clock_t te1 = clock();
    printf("Total time once: %f\n", (te1 - t1) / (float) CLOCKS_PER_SEC);


    /*
    BYTE cur_val[HASHSIZE];
    BYTE start[HASHSIZE]  = {0x01, 0x01, 0x01, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x44, 0x22, 0x11, 0x00};
    BYTE start2[HASHSIZE] = {0x02, 0x02, 0x02, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x44, 0x22, 0x11, 0x00};
    BYTE test1[HASHSIZE]  = {0x44, 0x44, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    BYTE test2[TESTHASHSIZE] = {0x44, 0x44};

    BYTE test3[4] = {0x00, 0x00, 0x00, 0x8F};
    BYTE test4[3] = {0x01,0x00 , 0x4F};
    BYTE test5[TESTHASHSIZE] = {0x00, 0x2F};
    BYTE test6[TESTHASHSIZE] = {0x00, 0x1F};
    BYTE test7[TESTHASHSIZE] = {0x00, 0x0F};
    */
    /*
    BYTE test1[HASHSIZE]    = {0x44, 0x44, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    BYTE test1_2[HASHSIZE]  = {0x44, 0x44, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    BYTE test1_3[HASHSIZE]  = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    BYTE test1_4[HASHSIZE]  = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    
    BYTE test8_2[TESTHASHSIZE] = {0xcd, 0xeb};
    BYTE test8[TESTHASHSIZE] = {0x77, 0x24};

    clock_t s1 = clock();

    /*
    hash(test8, TESTHASHSIZE, test1_3, functionVersion);
    clock_t e1 = clock();
    print_hex(test1_3);
    hash(test8_2, TESTHASHSIZE, test1_4, functionVersion);
    print_hex(test1_4);
    clock_t s2 = clock();

    hash(test8, TESTHASHSIZE, test8, functionVersion);
    clock_t e2 = clock();

    print_hex(test8);
    clock_t s3 = clock();

    hash(test8_2, TESTHASHSIZE, test8_2, functionVersion);
    clock_t e3 = clock();

    print_hex(test8_2);
    
    hash(test1, TESTHASHSIZE, test1_3, functionVersion);
    print_hex(test1_3);
    hash(test1_2, TESTHASHSIZE, test1_3, functionVersion);
    print_hex(test1_3);


    printf("Total time: %f", (e1 - s1) / (float) CLOCKS_PER_SEC);
    printf("Total time: %f", (e2 - s2) / (float) CLOCKS_PER_SEC);
    printf("Total time: %f", (e3 - s3) / (float) CLOCKS_PER_SEC);
    /*
    printf("%d\n", isDistinguishedPointBits(test3));
    printf("%d\n", isDistinguishedPointBits(test4));
    printf("%d\n", isDistinguishedPointBits(test5));
    printf("%d\n", isDistinguishedPointBits(test6));
    printf("%d\n", isDistinguishedPointBits(test7));
    print_hex(start);
    print_hex(start2);
    hash(test1, HASHSIZE, test1, functionVersion);
    hash(test2, TESTHASHSIZE, test2, functionVersion);
    print_hex(test1);
    print_hex(test2);

    DIST_POINT point;
    for (int cp = 0; cp < TESTHASHSIZE; cp++) {
        point.start[cp] = start[cp];
        point.end[cp] = start2[cp];
    }
    point.steps = 4;

    
    DIST_POINT p2;

    DIST_POINT* addr = &p2;
    printf("before: %lu\n", (unsigned long) addr);
    print_point(*addr);
    p2 = point;
    DIST_POINT* addr2 = &p2;
    printf("after: %lu\n", (unsigned long) addr2);
    print_point(*addr2);

   
    print_point(p2);


    truncateHash(start, TESTHASHSIZE);
    memcpy(cur_val, start, HASHSIZE);

    */

    return 0;
}
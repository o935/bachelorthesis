#include <device_launch_parameters.h>
#include <cuda_runtime.h>
#include <cuda.h>
#include <curand_kernel.h>
#include <time.h>
#include <cuda_profiler_api.h>

#include "md2.cu"

__global__ void setupStateKernel(curandState* state, unsigned long seed) {
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    curand_init(seed, idx, 0, &state[idx]);
}

__global__ void findDistinguishedPoint(curandState* states, DIST_POINT* d_dist_points, unsigned long functionVersion) {
    unsigned long long numSteps = 0;
    //unsigned long long totalSteps = pow(2, ZEROBYTES*8);
    unsigned long long totalSteps = pow(2, ZEROBITS);
    //find state using indices
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    curandState localState = states[idx];

    DIST_POINT point;
 
    //create starting point from local state
    BYTE cur_val[HASHSIZE];
    BYTE start[TESTHASHSIZE];
    for (int i = 0; i < TESTHASHSIZE; i++) {
			//set each byte of the seed to random value between 0 and 255
			float myRandF = curand_uniform(&localState);
			myRandF *= 255.999999;
			start[i] = (BYTE) truncf(myRandF);
            cur_val[i] = start[i];
    }

    while (numSteps < TOTALSTEPSFACTOR * totalSteps) {

        if (!isDistinguishedPointBits(cur_val)) {
            hash(cur_val, TESTHASHSIZE, cur_val, functionVersion);
            numSteps++;
        } else {
            //memcpy(point.start, start, TESTHASHSIZE);
            //memcpy(point.end, cur_val, TESTHASHSIZE);
            for (int cp = 0; cp < TESTHASHSIZE; cp++) {
                point.start[cp] = start[cp];
                point.end[cp] = cur_val[cp];
            }
            point.steps = numSteps;
            break;
        }
    }

    if (numSteps >= TOTALSTEPSFACTOR * totalSteps) {
        //if point wasn't found, set steps to 0. host can then check this very fast. this eliminates
        //dist. points that were instantly found, but is not that bad since that happens with low chance.
        point.steps = 0;
    }

    //writeback found point into the right location of the array
    memcpy(&d_dist_points[idx], &point, sizeof(DIST_POINT));
}

//finds the concrete collision, note that col.a has less steps than col.b
__global__ void findCollision(COLLISION* d_collisions, int num_cols, HASHCOLLISION* results, unsigned long functionVersion) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    //if thread-id is larger than the number of collisions we found, nothing to do
    if (idx >= num_cols) {
        return;
    }
    COLLISION col;
    memcpy(&col, &d_collisions[idx], sizeof(COLLISION));
    //COLLISION col = d_collisions[idx];
    BYTE collision_hash_1[TESTHASHSIZE];
    BYTE collision_hash_2[TESTHASHSIZE];

    //two values to store the 16 bytes hash outputs, since COLLISION DIST_POINTs only store the first TESTHASHSIZE bytes.
    BYTE cur_hash_a[HASHSIZE];
    BYTE cur_hash_b[HASHSIZE];
    //memcpy(&cur_hash_a, &col.a.start, TESTHASHSIZE);
    //memcpy(&cur_hash_b, &col.b.start, TESTHASHSIZE);
    for (int cp = 0; cp < TESTHASHSIZE; cp++) {
        cur_hash_a[cp] = col.a.start[cp];
        cur_hash_b[cp] = col.b.start[cp];
    }

    //"walk" along the start of b (with more steps) until they have equal steps...
    while (col.a.steps < col.b.steps) {
        hash(cur_hash_b, TESTHASHSIZE, cur_hash_b, functionVersion);
        col.b.steps -= 1;
    }
    //"robin hood" situation, we just return a all-zero-collision, doesnt really matter since we are looking
    //for a specific golden collision anyway..
    if (hashEqual(cur_hash_a, cur_hash_b)) {
        //printf("ROBIN HOOD, do something about it\n");
        memset(&results[idx], 0, sizeof(HASHCOLLISION));

        return;
    }

    while (hashNotEqual(cur_hash_a, cur_hash_b) && col.a.steps > 0 && col.b.steps > 0) {
        //memcpy(&collision_hash_1, &cur_hash_b, TESTHASHSIZE);
        //memcpy(&collision_hash_2, &cur_hash_a, TESTHASHSIZE);
        for (int cp = 0; cp < TESTHASHSIZE; cp++) {
            collision_hash_1[cp] = cur_hash_b[cp];
            collision_hash_2[cp] = cur_hash_a[cp];
        }

        hash(cur_hash_b, TESTHASHSIZE, cur_hash_b,functionVersion);
        col.b.steps -= 1;

        hash(cur_hash_a, TESTHASHSIZE, cur_hash_a,functionVersion);
        col.a.steps -= 1;
    }
    //TODO: a and b switched? doesnt really matter but...? above we set 1 and b equal, here 2 and b (and 1 and a)
    //memcpy(&results[idx].a, &collision_hash_1, TESTHASHSIZE);
    //memcpy(&results[idx].b, &collision_hash_2, TESTHASHSIZE);
    for (int cp = 0; cp < TESTHASHSIZE; cp++) {
        results[idx].a[cp] = collision_hash_1[cp];
        results[idx].b[cp] = collision_hash_2[cp];
    }
}

int main() {
    //measuring time
    clock_t start_time = clock();
    clock_t end_time;

    //generate random golden collision by randomly selecting 3 values
    //two of which both hash to the third...
    GOLDENCOLLISION* gc = (GOLDENCOLLISION*) malloc(sizeof(GOLDENCOLLISION));

    //TODO
    for (int i = 0; i < TESTHASHSIZE; i++) {
        gc->gc_hash1[i] = rand() % 256;
        gc->gc_hash2[i] = rand() % 256;
        gc->gc_res[i] = rand() % 256;
    }
    //TODO

    //current function version used
    unsigned long functionVersion = INITIAL_FUCTIONVERSION;
    curandState* d_states;
    DIST_POINT* h_memory;
    DIST_POINT* h_dist_points;
    DIST_POINT* d_dist_points;
    COLLISION* h_collisions;
    COLLISION* d_collisions;
    HASHCOLLISION* h_collision_results;
    HASHCOLLISION* d_collision_results;

    BYTE zerohash[16] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

    //allocate memory for W distinguished points on the host
    h_memory = (DIST_POINT*) calloc(W,sizeof(DIST_POINT));
    //allocate memory for one distinguished point per thread
    h_dist_points = (DIST_POINT*) calloc(THREADS,sizeof(DIST_POINT));
    if (cudaSuccess != cudaMalloc(&d_dist_points, THREADS*sizeof(DIST_POINT))) {
        printf("Error d_dist_points malloc: %d\n", cudaGetLastError());
    }
    //allocate states for each thread for seeding
    if (cudaSuccess != cudaMalloc(&d_states, THREADS*sizeof(curandState))) {
        printf("Error d_states malloc: %d\n", cudaGetLastError());
    }

    //allocate memory for collision array
    h_collisions = (COLLISION*) calloc(THREADS, sizeof(COLLISION));
    if (cudaSuccess != cudaMalloc(&d_collisions, THREADS*sizeof(COLLISION))) {
        printf("Error d_dist_points malloc: %d\n", cudaGetLastError());
    }

    h_collision_results = (HASHCOLLISION*) calloc(THREADS, sizeof(HASHCOLLISION));
    if (cudaSuccess != cudaMalloc(&d_collision_results, THREADS*sizeof(HASHCOLLISION))) {
        printf("Error d_dist_points malloc: %d\n", cudaGetLastError());
    }

    while (functionVersion < INITIAL_FUCTIONVERSION + NUMBER_OF_FUNCTIONVERSIONS) {
        printf("Current function version: %lu\n", functionVersion - INITIAL_FUCTIONVERSION);

        unsigned long totalCollisions = 0;
        unsigned long totalDistinctDistinguishedPoints = 0;
        unsigned long totalRobinHoods = 0;

        //zero out memory (new function version -> new distinguished point triples)
        memset(h_memory, 0, W*sizeof(DIST_POINT));

        //ITERATIONS is the number of "finding DP and locating collisions" iterations per function version
        for (int iter = 0; iter < ITERATIONS; iter++) {
            int pointsFound = 0;
            int distinctPointsFound = 0;
            int collisions_found = 0;
            int robinHoods = 0;

            clock_t iter_start = clock();

            //setup states of the threads (for the random seeds), currently with time and rand()
            setupStateKernel<<<B,T>>>(d_states, time(NULL) + rand());
            cudaDeviceSynchronize();
            cudaError_t error = cudaGetLastError();
            if (error != cudaSuccess) {
                printf("Error setUpKernel: %s\n", cudaGetErrorString(error));
                exit(-1);
            }
            //one iteration of finding distinguished points
            findDistinguishedPoint<<<B,T>>>(d_states, d_dist_points, functionVersion);
            cudaDeviceSynchronize();
            cudaMemcpy(h_dist_points, d_dist_points, THREADS*sizeof(DIST_POINT), cudaMemcpyDeviceToHost);
            /*
            printf("hi\n");
            print_point(h_dist_points[0]);
            print_point(h_dist_points[1]);
            print_point(h_dist_points[2]);
            print_point(h_dist_points[3]);
            */
            //read out distinguished points from the returned array
            for (int i = 0; i < THREADS; i++) {
                DIST_POINT point = h_dist_points[i];
                //check if the thread "i" really found a distinguished point
                if (point.steps != 0) {
                    //write distinguished point into correct memory location in h_memory
                    int index = ((point.end[TESTHASHSIZE-2]) * 256 + point.end[TESTHASHSIZE-1]) % W;

                    if (h_memory[index].steps == 0 || hashNotEqual(h_memory[index].end, point.end)) {
                        //if steps in memory location is 0 or DP not equal, we found a new DP
                        //TODO comment, second point is wrong if W too small (replacements, counts multiple times)
                        distinctPointsFound++;
                    } else if (isCollision(&h_memory[index], &point)) {
                            //place collision in next spot of array, a is the one with less steps
                            if (h_memory[index].steps > point.steps) {
                                memcpy(&h_collisions[collisions_found].a, &point, sizeof(DIST_POINT));
                                memcpy(&h_collisions[collisions_found].b, &h_memory[index], sizeof(DIST_POINT));
                            } else {
                                memcpy(&h_collisions[collisions_found].a, &h_memory[index], sizeof(DIST_POINT));
                                memcpy(&h_collisions[collisions_found].b, &point, sizeof(DIST_POINT));
                            }
                            collisions_found++;
                    } else {
                        //started at same seed, hence trivially found same distinguished point
                        continue;
                    }
                    //copy found distinguished point into h_memory
                    memcpy(&h_memory[index], &point, sizeof(DIST_POINT));
                    pointsFound++;

                }
            }

            //run kernel that handles finding of collisions
            cudaMemcpy(d_collisions, h_collisions, THREADS*sizeof(COLLISION), cudaMemcpyHostToDevice);
            findCollision<<<B,T>>>(d_collisions, collisions_found, d_collision_results, functionVersion);
            cudaDeviceSynchronize();
            cudaMemcpy(h_collision_results, d_collision_results, THREADS*sizeof(HASHCOLLISION), cudaMemcpyDeviceToHost);

            //go through all found collisions to check if the golden one was found
            for (int j = 0; j < collisions_found; j++) {
                //printing
                /*
                printf("===Found collision:===\n");
                print_hex(h_collision_results[j].a);
                print_hex(h_collision_results[j].b);
                printf("both hash to:\n");
                hash(h_collision_results[j].a, TESTHASHSIZE, h_collision_results[j].a, functionVersion);
                hash(h_collision_results[j].b, TESTHASHSIZE, h_collision_results[j].b, functionVersion);
                print_hex(h_collision_results[j].a);
                print_hex(h_collision_results[j].b);
                printf("=========\n");
                */
                //printing end

                //if the potential collision is all zero, it was a robin hood (see kernel), hence we ignore it
                if (hashEqual(h_collision_results[j].a, zerohash) && hashEqual(h_collision_results[j].b, zerohash)) {
                    robinHoods++;
                    continue;
                }

                if (isGoldenCollision(h_collision_results[j])) {
                    printf("FOUND GOLDEN COLLISION:\n");
                    end_time = clock();
                    //set parameters such that we finish instantly
                    iter = ITERATIONS;
                    functionVersion = INITIAL_FUCTIONVERSION + NUMBER_OF_FUNCTIONVERSIONS;

                    //print_hex(h_collision_results[j].a);
                    //print_hex(h_collision_results[j].b);
                    break;
                }
            }

            totalCollisions += collisions_found;
            totalDistinctDistinguishedPoints += pointsFound;
            totalRobinHoods += robinHoods;

            clock_t iter_end = clock();

            if (iter == ITERATIONS -1) {
                printf("%dth (%fs):", iter, (iter_end - iter_start) / (float) CLOCKS_PER_SEC);
                printf("Distinct DP: %lu, Cols.: %lu, RHs.: %lu\n", totalDistinctDistinguishedPoints, totalCollisions, totalRobinHoods);
            }
        }
        functionVersion++;
    }

    //free allocated memory on both device and host
    free(gc);
    free(h_memory);
    free(h_dist_points);
    free(h_collision_results);
    free(h_collisions);
    cudaFree(d_dist_points);
    cudaFree(d_states);
    cudaFree(d_collision_results);
    cudaFree(d_collisions);

    //find number of cores, maybe later on depend B and T on this
    cudaDeviceProp devProp;
    cudaGetDeviceProperties(&devProp, 0);
    //printf("Cores: %d\n", getNumberOfCores(devProp));
    printf("Time to find Golden Collision: %f\n", (end_time - start_time) / (float) CLOCKS_PER_SEC);

    return 0;
}
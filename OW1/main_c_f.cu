#include <device_launch_parameters.h>
#include <cuda_runtime.h>
#include <cuda.h>
#include <curand_kernel.h>
#include <time.h>
#include <cuda_profiler_api.h>

#include "md2.cu"

__global__ void setupStateKernel(curandState* state, unsigned long seed, unsigned long functionVersion) {
    //TODO: maybe add a salt (possibly also the functionVersion?)
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    curand_init(seed, idx, functionVersion, &state[idx]);
}

__global__ void findDistinguishedPoint(curandState* states, DIST_POINT* d_dist_points, unsigned long functionVersion, unsigned long long* counter, BYTE* notfound) {
    unsigned long long numSteps = 0;
    //hardcoded 20 from the paper (20*theta from the Oorschlot-Wiener paper)
    unsigned long long totalSteps = pow(2, ZEROBITS);// * 20;

    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    curandState localState = states[idx];

    DIST_POINT point;

    //counter stuff
    unsigned long long count = 0;

    numSteps = 0;
    BYTE cur_val[TESTHASHSIZE];
    BYTE start[TESTHASHSIZE];

    //new stuff
    //TODO: beware that this leads to shorter trails, as we also update the "start" field, i.e. we find triples from the new start values
    //after resuming, and not from the initial one. would need to sum up the steps and keep track of the original step value.
    if (notfound[idx] >= 20) {
        //if we haven't found anything in 20 iteration, start from new seed
        for (int i = 0; i < TESTHASHSIZE; i++) {
            float myRandF = curand_uniform(&localState);
            myRandF *= 255.999999;
            start[i] = (BYTE) truncf(myRandF);
            cur_val[i] = start[i];
        }
        notfound[idx] = 0;
    } else {
        //otherwise resume from where we left off
        memcpy(cur_val, d_dist_points[idx].end, TESTHASHSIZE);
    }

    

    while (numSteps < totalSteps) {
        if (!isDistinguishedPointBits(cur_val)) {
            count++;
            hash(cur_val, TESTHASHSIZE, cur_val, functionVersion);
            numSteps++;
        } else {
            for (int cp = 0; cp < TESTHASHSIZE; cp++) {
                point.start[cp] = start[cp];
                point.end[cp] = cur_val[cp];
            }
            point.steps = numSteps;
            notfound[idx]= 20;
            break;
        }
    }

    if (numSteps >= totalSteps) {
        notfound[idx] += 1;
        point.steps = 0;
    }

    memcpy(&d_dist_points[idx], &point, sizeof(DIST_POINT));

    //counter stuff
    counter[idx] = count;
}

__global__ void findCollision(COLLISION* d_collisions, int num_cols, HASHCOLLISION* results, unsigned long functionVersion, unsigned long long* counter) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx >= num_cols) {
        counter[idx] = 0;
        return;
    }

    COLLISION col;

    //counter stuff
    unsigned long long count = 0;

    memcpy(&col, &d_collisions[idx], sizeof(COLLISION));
    
    
    BYTE collision_hash_1[TESTHASHSIZE];
    BYTE collision_hash_2[TESTHASHSIZE];
    BYTE cur_hash_a[TESTHASHSIZE];
    BYTE cur_hash_b[TESTHASHSIZE];

    for (int cp = 0; cp < TESTHASHSIZE; cp++) {
        cur_hash_a[cp] = col.a.start[cp];
        cur_hash_b[cp] = col.b.start[cp];
    }

    while (col.a.steps < col.b.steps) {
        count++;
        hash (cur_hash_b, TESTHASHSIZE, cur_hash_b, functionVersion);
        col.b.steps -= 1;
    }

    //Robin hoods
    if (hashEqual(cur_hash_a, cur_hash_b)) {
        //TODO: make this manual, not with memset...
        for (int cp = 0; cp < TESTHASHSIZE; cp++) {
            results[idx].a[cp] = 0;
            results[idx].b[cp] = 0;
        }
        return;
    }

    while (hashNotEqual(cur_hash_a, cur_hash_b) && col.a.steps > 0 && col.b.steps > 0) {
        for (int cp = 0; cp < TESTHASHSIZE; cp++) {
            collision_hash_1[cp] = cur_hash_b[cp];
            collision_hash_2[cp] = cur_hash_a[cp];
        }
        count++;
        hash(cur_hash_b, TESTHASHSIZE, cur_hash_b,functionVersion);
        col.b.steps -= 1;
        count++;
        hash(cur_hash_a, TESTHASHSIZE, cur_hash_a,functionVersion);
        col.a.steps -= 1;
    }

    for (int cp = 0; cp < TESTHASHSIZE; cp++) {
        results[idx].a[cp] = collision_hash_1[cp];
        results[idx].b[cp] = collision_hash_2[cp];
    }
    

    counter[idx] = count;
}

int main() {
    /*
    cudaDeviceProp devProp;
    cudaGetDeviceProperties(&devProp, 0);
    printf("%d.%d\n",devProp.major, devProp.minor);
    cudaDeviceSynchronize();
    printf("Hi");
    */

    clock_t gc_start = clock();
    clock_t gc_end;

    unsigned long functionVersion = INITIAL_FUCTIONVERSION;

    curandState* d_states;
    DIST_POINT* h_memory;
    DIST_POINT* h_dist_points;
    DIST_POINT* d_dist_points;
    COLLISION* h_collisions;
    COLLISION* d_collisions;
    HASHCOLLISION* h_collision_results;
    HASHCOLLISION* d_collision_results;

    //count stuff
    unsigned long long count_dist = 0;
    unsigned long long count_coll = 0;
    unsigned long long* h_countlist;
    h_countlist = (unsigned long long*) calloc(THREADS, sizeof(unsigned long long));
    unsigned long long* d_countlist;
    if (cudaSuccess != cudaMalloc(&d_countlist, THREADS*sizeof(unsigned long long))) {
        printf("Error d_countlist malloc: %d\n", cudaGetLastError());
    }
    cudaMemcpy(d_countlist, h_countlist, THREADS*sizeof(unsigned long long), cudaMemcpyHostToDevice);


    h_memory            = (DIST_POINT*)     calloc(W, sizeof(DIST_POINT));
    h_dist_points       = (DIST_POINT*)     calloc(THREADS, sizeof(DIST_POINT));
    h_collisions        = (COLLISION*)      calloc(THREADS, sizeof(COLLISION));
    h_collision_results = (HASHCOLLISION*)  calloc(THREADS, sizeof(HASHCOLLISION));

    if (cudaSuccess != cudaMalloc(&d_states, THREADS*sizeof(curandState))) {
        printf("Error d_states malloc: %d\n", cudaGetLastError());
    }
    if (cudaSuccess != cudaMalloc(&d_dist_points, THREADS*sizeof(DIST_POINT))) {
        printf("Error d_dist_points malloc: %d\n", cudaGetLastError());
    }
    if (cudaSuccess != cudaMalloc(&d_collisions, THREADS*sizeof(COLLISION))) {
        printf("Error d_collisions malloc: %d\n", cudaGetLastError());
    }
    if (cudaSuccess != cudaMalloc(&d_collision_results, THREADS*sizeof(HASHCOLLISION))) {
        printf("Error d_collisions_results malloc: %d\n", cudaGetLastError());
    }
    //new stuff
    BYTE* notfound;
    if (cudaSuccess != cudaMalloc(&notfound, THREADS)) {
        printf("Error notfound malloc: %d\n", cudaGetLastError());
    }

    while (functionVersion < INITIAL_FUCTIONVERSION + NUMBER_OF_FUNCTIONVERSIONS) {

        unsigned long long totalCollisions                  = 0;
        unsigned long long totalDistinctDistinguishedPoints = 0;
        unsigned long long totalDistinguishedPoints         = 0;
        unsigned long long totalRobinHoods                  = 0;

        unsigned long long totalTrueCollisions              = 0;

        memset(h_memory, 0, W*sizeof(DIST_POINT));
        memset(h_dist_points, 0, THREADS*sizeof(DIST_POINT));
        memset(h_collisions, 0, THREADS*sizeof(COLLISION));
        memset(h_collision_results, 0, THREADS*sizeof(HASHCOLLISION));
        cudaMemset(d_dist_points, 0, THREADS*sizeof(DIST_POINT));
        cudaMemset(d_collisions, 0, THREADS*sizeof(COLLISION));
        cudaMemset(d_collision_results, 0, THREADS*sizeof(HASHCOLLISION));
        //new stuff
        cudaMemset(notfound, 20, THREADS);

        for (int iter = 0; iter < ITERATIONS; iter++) {

            clock_t iter_start = clock();

            unsigned long pointsFound           = 0;
            unsigned long distinctPointsFound   = 0;
            unsigned long collisionsFound       = 0;
            unsigned long robinHoods            = 0;

            unsigned long trueCollisionsFound   = 0;

            setupStateKernel<<<B,T>>>(d_states, time(NULL) + rand(), functionVersion);
            cudaDeviceSynchronize();
            cudaError_t error = cudaGetLastError();
            if (error != cudaSuccess) {
                printf("Error setUpKernel: %s\n", cudaGetErrorString(error));
                exit(-1);
            }

            findDistinguishedPoint<<<B,T>>>(d_states, d_dist_points, functionVersion, d_countlist, notfound);
            cudaDeviceSynchronize();
            error = cudaGetLastError();
            if (error != cudaSuccess) {
                printf("Error findDistinguishedPoint Kernel: %s\n", cudaGetErrorString(error));
                exit(-1);
            }
            //counter stuff
            cudaMemcpy(h_countlist, d_countlist, THREADS*sizeof(unsigned long long), cudaMemcpyDeviceToHost);

            for (int c = 0; c < THREADS; c++) {
                count_dist += h_countlist[c];
                //reset..
                h_countlist[c] = 0;
            }


            cudaMemcpy(h_dist_points, d_dist_points, THREADS*sizeof(DIST_POINT), cudaMemcpyDeviceToHost);

            for (int i = 0; i < THREADS; i++) {
                DIST_POINT point = h_dist_points[i];
                //print_point(point);

                if (point.steps == 0)
                    continue;
                
                pointsFound++;
                //works for at most W=2^16 at the moment
                int index = ((point.end[TESTHASHSIZE-2]) * 256 + point.end[TESTHASHSIZE-1]) % W;

                if (h_memory[index].steps == 0 || hashNotEqual(h_memory[index].end, point.end)) {
                    //TODO: not true if W is smaller than #distpoints
                    distinctPointsFound++;
                } else if (isCollision(&h_memory[index], &point)) {
                    if (h_memory[index].steps > point.steps) {
                        memcpy(&h_collisions[collisionsFound].a, &point, sizeof(DIST_POINT));
                        memcpy(&h_collisions[collisionsFound].b, &h_memory[index], sizeof(DIST_POINT));
                    } else {
                        memcpy(&h_collisions[collisionsFound].a, &h_memory[index], sizeof(DIST_POINT));
                        memcpy(&h_collisions[collisionsFound].b, &point, sizeof(DIST_POINT));
                    }
                    collisionsFound++;
                } else {
                    continue;
                }

                memcpy(&h_memory[index], &point, sizeof(DIST_POINT));
            }

            cudaMemcpy(d_collisions, h_collisions, THREADS*sizeof(COLLISION), cudaMemcpyHostToDevice);
            findCollision<<<B,T>>>(d_collisions, collisionsFound, d_collision_results, functionVersion, d_countlist);
            cudaDeviceSynchronize();
            error = cudaGetLastError();
            if (error != cudaSuccess) {
                printf("Error findCollision Kernel: %s\n", cudaGetErrorString(error));
                exit(-1);
            }

            //counter stuff
            cudaMemcpy(h_countlist, d_countlist, THREADS*sizeof(unsigned long long), cudaMemcpyDeviceToHost);

            for (int c = 0; c < THREADS; c++) {
                count_coll += h_countlist[c];
                //reset..
                h_countlist[c] = 0;
            }


            cudaMemcpy(h_collision_results, d_collision_results, THREADS*sizeof(HASHCOLLISION), cudaMemcpyDeviceToHost);

            for (int j = 0; j < collisionsFound; j++) {
                if (hashEqual(h_collision_results[j].a, h_collision_results[j].b)) {
                    //TODO: temporary solution, differ robin hoods from "not founds"
                    //print_hex(h_collision_results[j].a);
                    robinHoods++;
                    continue;
                }
                trueCollisionsFound++;

                if (isGoldenCollision(h_collision_results[j])) {
                    printf("FOUND GOLDEN COLLISION:\n");
                    print_hex(h_collision_results[j].a);
                    print_hex(h_collision_results[j].b);
                    gc_end = clock();
                    goto end;
                }
            }

        totalCollisions += collisionsFound;
        totalDistinctDistinguishedPoints += distinctPointsFound;
        totalDistinguishedPoints += pointsFound;
        totalRobinHoods += robinHoods;
        totalTrueCollisions += trueCollisionsFound;

        clock_t iter_end = clock();

        if (iter == ITERATIONS - 1) {

            printf("%luth function version, ", functionVersion - INITIAL_FUCTIONVERSION);
            printf("%dth iteration (%fs):", iter, (iter_end - iter_start) / (float) CLOCKS_PER_SEC);
            printf("P: %llu, D: %llu, C: %llu, R: %llu, TC: %llu\n", totalDistinguishedPoints, 
                totalDistinctDistinguishedPoints, totalCollisions, totalRobinHoods, totalTrueCollisions);

            //counter stuff
            printf("Count Dist: %llu, Count Coll: %llu, Ratio: %f. |||\n", count_dist, count_coll, (float) count_dist / (float) (count_dist + count_coll));
        }
        }
        functionVersion++;
    }

    end: 
    printf("Total time: %f\n", (gc_end - gc_start) / (float) CLOCKS_PER_SEC);
    //printf("Needed %lu function versions", functionVersion - INITIAL_FUCTIONVERSION);
    free(h_memory);
    free(h_dist_points);
    free(h_collision_results);
    free(h_collisions);
    cudaFree(d_dist_points);
    cudaFree(d_states);
    cudaFree(d_collision_results);
    cudaFree(d_collisions);

    return 0;
}
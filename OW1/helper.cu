
#include <cuda_runtime_api.h>

#include "config.h"

//returns true of the given input hashes aren't equal
__host__ __device__ int hashNotEqual(BYTE* a, BYTE* b) {
	for (int i = 0; i < TESTHASHSIZE; i++) {
		if (a[i] != b[i]) {
			return TRUE;
		}
	}
	return FALSE;
}

__host__ __device__ int hashEqual(BYTE* a, BYTE* b) {
    for (int i = 0; i < TESTHASHSIZE; i++) {
        if (a[i] != b[i]) {
            return FALSE;
        }
    }
    return TRUE;
}

//truncate hash such that we only use first "num" bytes
/*
__host__ __device__ void truncateHash(BYTE* hash, int num) {
    for (int i = num; i < HASHSIZE; i++) {
        hash[i] = 0;
    }
}
*/

//returns true if the end hash values are identical, but the start values are not.
__host__ int isCollision(DIST_POINT* a, DIST_POINT* b) {
    for (int i = 0; i < TESTHASHSIZE; i++) {
        if (a->end[i] != b->end[i]) {
            return FALSE;
        }
    }

	for (int i = 0; i < TESTHASHSIZE; i++) {
		if (a->start[i] != b->start[i]) {
			return TRUE;
		}
	}
    return FALSE;
}

//returns true of the given collision is the golden collision, TODO
__host__ int isGoldenCollision(HASHCOLLISION coll) {

	if (coll.a[0] == 21 && coll.a[1] == 22 && coll.a[2] == 23/* && coll.a[3] == 24*/) {
		if (coll.b[0] == 31 && coll.b[1] == 32 && coll.b[2] == 33/* && coll.b[3] == 34*/) {
			return TRUE;
		}
	} else if (coll.a[0] == 31 && coll.a[1] == 32 && coll.a[2] == 33/* && coll.a[3] == 34*/) {
		if (coll.b[0] == 21 && coll.b[1] == 22 && coll.b[2] == 23/* && coll.b[3] == 24*/) {
			return TRUE;
		}
	}

    return FALSE;
}

//returns true if given point is a distinguished point, could be optimized to certain value...
/*
__device__ int isDistinguishedPoint(BYTE* point) {
    for (int i = 0; i < ZEROBYTES; i++) {
        if (point[i] != 0) {
            return FALSE;
        }
    }
    return TRUE;
}
*/


//return true if given point is a distinguished point, via number of bits instead of bytes
__device__ int isDistinguishedPointBits(BYTE* point) {
    //first check that the required number of bytes are zero
    for (int i = 0; i < ZEROBITS / 8; i++) {
        if (point[i] != 0) {
            return FALSE;
        }
    }
    //check that the remaining number of bits are zero
    int j = ZEROBITS % 8;

    return (point[ZEROBITS / 8] < pow(2, 8-j));
}

//returns number of Cores of given graphics card (old code)
__host__ int getNumberOfCores(cudaDeviceProp devProp) {

    int cores = 0;
    int mp = devProp.multiProcessorCount;
    switch (devProp.major) {
        case 2:
            if (devProp.minor == 1) cores = mp* 48;
            else cores = mp * 32;
            break;
        case 3:
            cores = mp * 192;
            break;
        case 5:
            cores = mp * 128;
            break;
        case 6:
            if ((devProp.minor == 1) || (devProp.minor == 2)) cores = mp * 128;
            else if (devProp.minor == 0) cores = mp * 64;
            else printf("unknown device type \n");
            break;
        case 7:
            if ((devProp.minor == 0) || (devProp.minor == 5)) cores = mp * 64;
            else printf("unknown device type \n");
            break;
        case 8:
            if (devProp.minor == 0) cores = mp * 64;
            else if (devProp.minor == 6) cores = mp * 128;
            else printf("unknown device type \n");
            break;
        default:
            printf("unknown device type \n");
            break;
    }

	return cores;
}

//============================================
//=============PRINTING FUNCTIONS=============
//============================================

__host__ __device__ void print_hex(BYTE *s) {
    for (int ii = 0; ii < TESTHASHSIZE; ii++)
        printf("%02x", (unsigned int) *s++);
    printf("\n");
}

__host__ __device__ void print_point(DIST_POINT point) {
	BYTE* start = point.start;
	BYTE* end = point.end;
	printf("Triple:: (");
	for (int ii = 0; ii < TESTHASHSIZE; ii++)
        printf("%02x", (unsigned int) *start++);
	printf(", ");
	for (int ii = 0; ii < TESTHASHSIZE; ii++)
        printf("%02x", (unsigned int) *end++);
	printf(", %llu)\n", point.steps);
}

__host__ __device__ void print_collision(COLLISION col) {
    printf("====Collision====\n");
    print_point(col.a);
    print_point(col.b);
}

__host__ __device__ void print_hashcollision(HASHCOLLISION hcol) {
    printf("====Hashcollision====\n");
    print_hex(hcol.a);
    print_hex(hcol.b);
}

#include <device_launch_parameters.h>
#include <cuda_runtime.h>
#include <cuda.h>
#include <curand_kernel.h>

#include <time.h>

#include "sha256.cu"


__global__ void testKernelTwice(BYTE* hashing, unsigned long functionVersion, GOLDENCOLLISION gc) {
    for (int i = 0; i < 10000; i++) {
        hash(hashing, HASHSIZE, hashing, functionVersion+i, gc);
    }
}

int main() {
    /*
    GOLDENCOLLISION gc;
    unsigned long functionVersion = 39428033;

    gc.hash1[0] = 0x5f;
    gc.hash1[1] = 0x42;
    gc.hash1[2] = 0x23;
    gc.hash2[0] = 0x24;
    gc.hash2[1] = 0x52;
    gc.hash2[2] = 0x84;
    gc.res[0] = 0x41;
    gc.res[1] = 0x9a;
    gc.res[2] = 0xb5; 


    BYTE* hash = (BYTE*) malloc(HASHSIZE);
    BYTE* d_hash;

    hash[0] = 0x21;
    hash[1] = 0xa3;
    hash[2] = 0xbb;

    cudaMalloc(&d_hash, HASHSIZE);

    cudaMemcpy(d_hash, hash, HASHSIZE, cudaMemcpyHostToDevice);

    clock_t t1 = clock();
    testKernelTwice<<<B,T>>>(d_hash,functionVersion, gc);
    cudaDeviceSynchronize();
    cudaMemcpy(hash, d_hash, HASHSIZE, cudaMemcpyDeviceToHost);
    clock_t te1 = clock();
    printf("Total time once: %f\n", (te1 - t1) / (float) CLOCKS_PER_SEC); 

    */

    BYTE hash[2] = {0x1b, 0x23};
    
    printf("%d",getBit(hash, 0));
    printf("%d",getBit(hash, 1)); 
    printf("%d",getBit(hash, 2)); 
    printf("%d ",getBit(hash, 3));
    printf("%d",getBit(hash, 4)); 
    printf("%d",getBit(hash, 5));
    printf("%d",getBit(hash, 6));
    printf("%d ",getBit(hash, 7)); 
    printf("%d",getBit(hash, 8));
    printf("%d",getBit(hash, 9));
    printf("%d",getBit(hash, 10)); 
    printf("%d ",getBit(hash, 11));
    printf("%d",getBit(hash, 12)); 
    printf("%d",getBit(hash, 13));
    printf("%d",getBit(hash, 14)); 
    printf("%d\n",getBit(hash, 15));

    printf("%d\n",getIndex(hash));
    



}
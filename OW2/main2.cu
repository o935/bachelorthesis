#include <device_launch_parameters.h>
#include <cuda_runtime.h>
#include <cuda.h>
#include <curand_kernel.h>
#include <time.h>
#include <cuda_profiler_api.h>
#include <stdio.h>
#include <unistd.h>
#include "sha256.cu"

extern __shared__ DIST_POINT curr[];

__global__ void setup(curandState* state, unsigned long seed, unsigned int functionVersion) {
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    curand_init(seed, idx, functionVersion, &state[idx]);

}

__global__ void findDistinguishedPoint(curandState* states, DIST_POINT* d_dist_points, unsigned int functionVersion, GOLDENCOLLISION gc) {

    unsigned long long numSteps = 0;
    unsigned long long totalSteps = pow(2, ZEROBITS);
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    curandState localState = states[idx];

    //load the "saved points" into shared memory
    extern __shared__ DIST_POINT curr[];
    curr[threadIdx.x] = d_dist_points[idx];
    __syncthreads();

    //>= 20 includes either that the last 7 bits are >= 20 or that the valid bit is 1
    if (curr[threadIdx.x].valid >= 20 || curr[threadIdx.x].valid == 0) {
        //is valid or expired, start from new seed (or for first time.., hence the second condition)
        for (int i = 0; i < HASHSIZE; i++) {
            float myRandF = curand_uniform(&localState);
            myRandF *= 255.999999;
            curr[threadIdx.x].start[i] = curr[threadIdx.x].end[i] = (BYTE) truncf(myRandF);
        }
        //this is only to set certain bits to zero, such that we start at a valid point for e.g. 22bit hashes
        curr[threadIdx.x].start[HASHSIZE-1] = curr[threadIdx.x].start[HASHSIZE-1] & MASK;
        curr[threadIdx.x].end[HASHSIZE-1] = curr[threadIdx.x].end[HASHSIZE-1] & MASK;

        //when starting at new start-point reset steps value and initialize valid to 1
        curr[threadIdx.x].steps = 0;
        curr[threadIdx.x].valid = 1;

    } else {
        //point wasn't valid / we need to resume from where we left off
        curr[threadIdx.x].valid += 1;
    }

    //write state back such that we only need to setup once..
    states[idx] = localState;
    //"walk" along the random function until we find a distinguished point OR reach totalSteps
    while (numSteps < totalSteps) {
        //if its distinguished point OR we just seeded new point, commence
        //if steps is 0, we are on a new "seed", if numSteps is also 0, we also are in the first iteration
        if (!isDistinguishedPoint2(curr[threadIdx.x].end, (BYTE *)&functionVersion)
            || (curr[threadIdx.x].steps == 0 && numSteps == 0)) {
            hash(curr[threadIdx.x].end, HASHSIZE, curr[threadIdx.x].end, functionVersion, gc);
            numSteps++;
        } else {
            //if we find a distinguished point, set the valid-bit and the steps field
            curr[threadIdx.x].steps += numSteps;
            curr[threadIdx.x].valid = 0x80;
            break;
        }
    }

    //adjust the steps field if we didn't find a distinguished point yet.
    if (numSteps >= totalSteps) {
        curr[threadIdx.x].steps += totalSteps;
    }

    __syncthreads();
    //write back the "saved" points to global memory
    d_dist_points[idx] = curr[threadIdx.x];
}

__global__ void findCollision(COLLISION* d_collisions, int num_cols, HASHCOLLISION* results, unsigned long functionVersion, GOLDENCOLLISION gc) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx >= num_cols)
        return;

    COLLISION col;
    col = d_collisions[idx];
    
    BYTE collision_hash_1[HASHSIZE];
    BYTE collision_hash_2[HASHSIZE];
    BYTE cur_hash_a[HASHSIZE];
    BYTE cur_hash_b[HASHSIZE];

    for (int cp = 0; cp < HASHSIZE; cp++) {
        cur_hash_a[cp] = col.a.start[cp];
        cur_hash_b[cp] = col.b.start[cp];
    }

    //walk along "longer" path until the pathlengths are equal
    while (col.a.steps < col.b.steps) {
        hash(cur_hash_b, HASHSIZE, cur_hash_b, functionVersion, gc);
        col.b.steps -= 1;
    }

    //Robin hoods: if we have equally many steps left and the starting points match, its not a real collision
    if (hashEqual(cur_hash_a, cur_hash_b)) {
        for (int cp = 0; cp < HASHSIZE; cp++) {
            results[idx].a[cp] = 0;
            results[idx].b[cp] = 0;
        }
        return;
    }

    //resume be walking along both paths step by step until we find the collision
    while (hashNotEqual(cur_hash_a, cur_hash_b) && col.a.steps > 0 && col.b.steps > 0) {
        for (int cp = 0; cp < HASHSIZE; cp++) {
            collision_hash_1[cp] = cur_hash_b[cp];
            collision_hash_2[cp] = cur_hash_a[cp];
        }
        hash(cur_hash_b, HASHSIZE, cur_hash_b,functionVersion, gc);
        col.b.steps -= 1;
        hash(cur_hash_a, HASHSIZE, cur_hash_a,functionVersion, gc);
        col.a.steps -= 1;
    }

    //copy the found collision into global memory
    for (int cp = 0; cp < HASHSIZE; cp++) {
        results[idx].a[cp] = collision_hash_1[cp];
        results[idx].b[cp] = collision_hash_2[cp];
    }
}


//TODO: do a functio so we have less code in main where we write points into h_memory
//and spot the happening collisions to report to the next kernel
__host__ void writePointsToMemory() {

}


int main() {

    //Designate 80% of the threads to finding distinguished points
    //and 20% of the threads to locating the collisions
    unsigned long functionIteration = 0;

    //find out how many blocks/threads are for finding points and for finding collisions
    int blocks = THREADS / T;
    int dpBlocks = blocks * 0.8;
    int colBlocks = blocks - dpBlocks;
    int dp_threads = dpBlocks*T;
    int col_threads = colBlocks*T;

    //randomize seed
    srand(time(NULL));

    GOLDENCOLLISION gc = getGoldenCollision();
    unsigned int functionVersion = getFunctionVersion();

    curandState* d_states;
    DIST_POINT* h_memory;
    DIST_POINT* h_dist_points;
    DIST_POINT* d_dist_points;
    //should act as queue to store collisions, TODO: find correct size bound
    COLLISION* h_collisions;

    COLLISION* d_collisions;
    HASHCOLLISION* h_collision_results;
    HASHCOLLISION* d_collision_results;

    //allocate host memory
    /*
    h_memory = (DIST_POINT*) calloc(W, sizeof(DIST_POINT));
    h_dist_points = (DIST_POINT*) calloc(THREADS, sizeof(DIST_POINT));
    h_collisions = (COLLISION*) calloc(THREADS, sizeof(COLLISION));
    h_collision_results = (HASHCOLLISION*) calloc(THREADS, sizeof(HASHCOLLISION));
    */

    //allocate PINNED host memory
    h_memory = (DIST_POINT*) calloc(W, sizeof(DIST_POINT));

    cudaError_t status = cudaMallocHost(&h_dist_points, THREADS*sizeof(DIST_POINT));
    if (status != cudaSuccess)
        printf("Error allocating pinned host memory h_dist_points\n");
    status = cudaMallocHost(&h_collisions, THREADS*sizeof(COLLISION));
    if (status != cudaSuccess)
        printf("Error allocating pinned host memory h_dist_points\n");
    status = cudaMallocHost(&h_collision_results, THREADS*sizeof(HASHCOLLISION));
    if (status != cudaSuccess)
        printf("Error allocating pinned host memory h_dist_points\n");

    //allocate device memory
    if (cudaSuccess != cudaMalloc(&d_states, dp_threads*sizeof(curandState))) {
        printf("Error d_states malloc: %d\n", cudaGetLastError());
    }
    if (cudaSuccess != cudaMalloc(&d_dist_points, dp_threads*sizeof(DIST_POINT))) {
        printf("Error d_dist_points malloc: %d\n", cudaGetLastError());
    }
    if (cudaSuccess != cudaMalloc(&d_collisions, col_threads*sizeof(COLLISION))) {
        printf("Error d_collisions malloc: %d\n", cudaGetLastError());
    }
    if (cudaSuccess != cudaMalloc(&d_collision_results, col_threads*sizeof(HASHCOLLISION))) {
        printf("Error d_collisions_results malloc: %d\n", cudaGetLastError());
    }

    cudaStream_t dp_stream, col_stream;
    cudaStreamCreate(&dp_stream);
    cudaStreamCreate(&col_stream);

    //execute the algorithm for a certain number of different function versions
    while (functionIteration < NUMBER_OF_FUNCTIONVERSIONS) {

        //reset the allocated memoriy regions to 0 (not really necessary for all of them)
        //TODO: sizes dont really match yet, don't need THREAD size everywhere anymore.
        memset(h_memory, 0, W*sizeof(DIST_POINT));
        memset(h_dist_points, 0, THREADS*sizeof(DIST_POINT));
        memset(h_collisions, 0, THREADS*sizeof(COLLISION));
        memset(h_collision_results, 0, THREADS*sizeof(HASHCOLLISION));
        cudaMemset(d_dist_points, 0, dp_threads*sizeof(DIST_POINT));
        cudaMemset(d_collisions, 0, col_threads*sizeof(COLLISION));
        cudaMemset(d_collision_results, 0, col_threads*sizeof(HASHCOLLISION));

        //number of points found, continue to next function version when larger than 10W
        unsigned long pointsFound = 0;
        unsigned long collisionsFound = 0;

        //TODO: add errors to all kernel calls etc.
        setup<<<dpBlocks,T>>>(d_states, time(NULL) + rand(), functionVersion);
        cudaStreamSynchronize(dp_stream);

        size_t shared_size = T*sizeof(DIST_POINT);

        //TODO: dont do 80-20 but instead as many cores as we found collisions only, because
        //the work might be 80-20 but its not evenly sprread probably, aka. if we use 20%
        //of the cores for collisions some cores do nothing others run very long...
        //we need to adapt the alloc sizes to THREADS for this again though...

        do {
            findDistinguishedPoint<<<dpBlocks, T, shared_size, dp_stream>>>
            (d_states, d_dist_points, functionVersion, gc);
            cudaMemcpyAsync(h_dist_points, d_dist_points, dp_threads*sizeof(DIST_POINT), cudaMemcpyDeviceToHost, dp_stream);

            cudaStreamSynchronize(dp_stream);
            int_tuple res  = writeBackPoints(h_memory, h_dist_points, h_collisions + collisionsFound, dp_threads, pointsFound, collisionsFound);
            pointsFound = res.a;
            collisionsFound = res.b;

            if (collisionsFound >= col_threads) {
                //update col_threads and dp_threads such that we find all found collisions and when 
                //done we reset the parameters back to normal
                cudaMemcpyAsync(d_collisions, h_collisions, col_threads*sizeof(COLLISION), cudaMemcpyHostToDevice, col_stream);
                findCollision<<<colBlocks, T, 0, col_stream>>>(d_collisions, col_threads, d_collision_results, functionVersion, gc);
                cudaMemcpyAsync(h_collision_results, d_collision_results, col_threads*sizeof(HASHCOLLISION), cudaMemcpyDeviceToHost, col_stream);
                cudaStreamSynchronize(col_stream);
                for (int j = 0; j < collisionsFound; j++) {

                    //print_hashcollision(h_collision_results[j]);

                    if (isGoldenCollision(h_collision_results[j], gc)) {
                        goto end;
                    }
                }
                collisionsFound = 0;
            }
            
            /*printf("PointsFound: %lu\n", pointsFound);
            printf("collisionsFound: %lu\n", collisionsFound);
            printf("Occupied ratio: %f\n", occupiedRatio(h_memory));*/

        } while (pointsFound < 10*W);

        printf("functionIteration: %lu\n", functionIteration);
        functionIteration += 1;
        functionVersion = getFunctionVersion();
    }


    end:
    cudaDeviceSynchronize();

    printf("Done, Needed Functionversions: %lu\n", functionIteration);

    //free all allocated memory
    free(h_memory);
    cudaFreeHost(h_dist_points);
    cudaFreeHost(h_collision_results);
    cudaFreeHost(h_collisions);
    cudaFree(d_dist_points);
    cudaFree(d_states);
    cudaFree(d_collision_results);
    cudaFree(d_collisions);
    cudaStreamDestroy(dp_stream);
    cudaStreamDestroy(col_stream);
    
    //cudaDeviceReset();

    sleep(2);
    return 0;
}
#include <stdio.h>
#include "helper.cu"

int main() {

    printf("SIZE: %x\n", SIZE);
    printf("MASK: %x\n", MASK);
    printf("ZEROBITS: %d\n", ZEROBITS);
    printf("W: %d\n", W);


    FILE* file = fopen("output.txt", "r");
    unsigned long sum = 0;
    unsigned long newval = 0;
    unsigned long values = 0;
    unsigned long long newcount = 0;
    unsigned long long count_sum = 0;

    while(!feof(file)) {
        fscanf(file, "%lu", &newval);
        fscanf(file, "%llu", &newcount);
        //printf("%lu\n", newval);
        sum += newval;
        count_sum += newcount;
        values++;
    }

    fclose(file);

    printf("===log N IS %d====W IS %d===\n", N, W);

    printf("Values: %lu, Count: %f, Iterations: %f, ", values, count_sum / (float) values, sum /  (float) values);

    unsigned long long iter = 0;
    unsigned long long countexp = 0;

    countexp = sqrt(pow(2,N*3) / W);

    float factor = (count_sum / (float) values) / (float) countexp;

    iter = 0.45 * pow(2,N) / W;

    printf("Expected Iterations: %llu, Expected Count: %llu, Factor: %f\n", iter, countexp, factor);

    FILE* time_file = fopen("time.txt", "r");
    float sum_t = 0;
    unsigned long values_t = 0;
    float newval_t = 0;

     while(!feof(time_file)) {
        fscanf(time_file, "%f", &newval_t);
        sum_t += newval_t;
        values_t++;
    }

    float average = sum_t / values_t;

    printf("Average Time: %f\n", average);
}
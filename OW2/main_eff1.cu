#include <device_launch_parameters.h>
#include <cuda_runtime.h>
#include <cuda.h>
#include <curand_kernel.h>
#include <time.h>
#include <cuda_profiler_api.h>
#include <stdio.h>
#include <unistd.h>
#include "sha256.cu"

//shared memory allocation (dynamic) for use in the "findDistinguishedPoint" Kernel
//extern __shared__ DIST_POINT curr[];

//sets up a state for each thread which we need to generate random starting points
__global__ void setupState(curandState* state, unsigned long seed, unsigned int functionVersion) {
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    curand_init(seed + functionVersion, idx, 0, &state[idx]);
}

//kernel which finds a distinguished point
__global__ void findDistinguishedPoint(curandState* states, DIST_POINT* d_points, unsigned int functionVersion, GOLDENCOLLISION gc, unsigned long long* counter) {

    unsigned long long numSteps = 0;
    unsigned long long totalSteps = pow(2, ZEROBITS) / GAMMA_FACTOR;
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    curandState localState = states[idx];

    //load the "saved points" into shared memory
    //extern __shared__ DIST_POINT curr[];
    DIST_POINT p = d_points[idx];

    unsigned long long count = 0;

    //>= 20 includes either that the last 7 bits are >= 20 or that the valid bit is 1
    if (p.valid >= GAMMA * GAMMA_FACTOR || p.valid == 0) {
        //is valid or expired, start from new seed (or for first time.., hence the second condition)
        for (int i = 0; i < SIZE; i++) {
            float myRandF = curand_uniform(&localState);
            myRandF *= 255.999999;
            p.start[i] = p.end[i] = (BYTE) truncf(myRandF);
        }
        //apply mask
        p.start[SIZE-1] = p.start[SIZE-1] & MASK;
        p.end[SIZE-1] = p.end[SIZE-1] & MASK;

        //when starting at new start-point reset steps value and initialize valid to 1
        p.steps = 0;
        p.valid = 1;

    } else {
        //point wasn't valid / we need to resume from where we left off
        p.valid += 1;
    }

    //write state back such that we only need to setup once..
    states[idx] = localState;
    //"walk" along the random function until we find a distinguished point OR reach totalSteps
    while (numSteps < totalSteps) {
        //if its distinguished point OR we just seeded new point, commence
        //if steps is 0, we are on a new "seed", if numSteps is also 0, we also are in the first iteration
        if (!distinguished(p.end, (BYTE *)&functionVersion) || (p.steps == 0 && numSteps == 0)) {
            f(p.end, p.end, functionVersion, gc);
            count++;
            numSteps++;
        } else {
            //if we find a distinguished point, set the valid-bit and the steps field
            p.steps += numSteps;
            p.valid = 0x80;
            break;
        }
    }

    //adjust the steps field if we didn't find a distinguished point yet.
    if (numSteps >= totalSteps) {
        p.steps += totalSteps;
    }

    //write back the "saved" points to global memory
    d_points[idx] = p;

    //counter stuff
    counter[idx] = count;
}

//kernel locates the actual collision, given two distinguished point triples which start at different locations but end at the same dist. point
__global__ void locateCollision(DPCOLLISION* d_collisions, int num_cols, VALUECOLLISION* results, unsigned long functionVersion, GOLDENCOLLISION gc, unsigned long long* counter) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned long long count = 0;

    //only the case if we found less collisions than we have threads
    if (idx >= num_cols) {
        counter[idx] = 0;
        return;
    }

    DPCOLLISION col = d_collisions[idx];
    
    //these will store the two values making up the collision
    BYTE colliding_value1[SIZE];
    BYTE colliding_value2[SIZE];
    //these will store the current values we are at while walking along the paths
    BYTE curr_value1[SIZE];
    BYTE curr_value2[SIZE];

    for (int cp = 0; cp < SIZE; cp++) {
        curr_value1[cp] = col.a.start[cp];
        curr_value2[cp] = col.b.start[cp];
    }

    //walk along "longer" path until the pathlengths are equal
    while (col.a.steps < col.b.steps) {
        f(curr_value2, curr_value2, functionVersion, gc);
        count++;
        col.b.steps -= 1;
    }

    //Robin hoods: if we have equally many steps left and the starting points match, its not a real collision
    if (equal(curr_value1, curr_value2)) {
        for (int cp = 0; cp < SIZE; cp++) {
            results[idx].a[cp] = 0;
            results[idx].b[cp] = 0;
        }
        return;
    }

    //resume by walking along both paths step by step until we find the collision
    while (notEqual(curr_value1, curr_value2) && col.a.steps > 0) {
        for (int cp = 0; cp < SIZE; cp++) {
            colliding_value1[cp] = curr_value2[cp];
            colliding_value2[cp] = curr_value1[cp];
        }
        f(curr_value2, curr_value2,functionVersion, gc);
        f(curr_value1, curr_value1,functionVersion, gc);
        col.a.steps -= 1;

        count += 2;
    }

    //copy the found collision into global memory
    for (int cp = 0; cp < SIZE; cp++) {
        results[idx].a[cp] = colliding_value1[cp];
        results[idx].b[cp] = colliding_value2[cp];
    }

    //counter stuff
    counter[idx] = count;
}

int main () {

    srand(time(NULL));
    //generate random golden collision
    GOLDENCOLLISION gc = getGoldenCollision();

    //initialize functionVersion to zero
    unsigned int functionVersion = 0;

    //timing stuff
    clock_t gc_start, gc_end, iter_start, iter_end;
    gc_start = clock();

    curandState* d_states;
    DIST_POINT* memory;
    DIST_POINT* points;
    DIST_POINT* d_points;
    DPCOLLISION* collisions;
    DPCOLLISION* d_collisions;
    VALUECOLLISION* collision_results;
    VALUECOLLISION* d_collision_results;

    unsigned long long count_findDist = 0;
    unsigned long long count_findColl = 0;
    unsigned long long* count_list;
    unsigned long long* d_count_list;

    count_list = (unsigned long long*) calloc(THREADS, sizeof(unsigned long long));
    cudaMalloc(&d_count_list, THREADS*sizeof(unsigned long long));
    
    cudaMemset(d_count_list, 0, THREADS*sizeof(unsigned long long));

    memory = (DIST_POINT*) calloc(W, sizeof(DIST_POINT));
    points = (DIST_POINT*) calloc(THREADS, sizeof(DIST_POINT));
    collisions = (DPCOLLISION*) calloc(THREADS*2, sizeof(DPCOLLISION));
    collision_results = (VALUECOLLISION*) calloc(THREADS, sizeof(VALUECOLLISION));

    cudaMalloc(&d_states, THREADS*sizeof(curandState));
    cudaMalloc(&d_points, THREADS*sizeof(DIST_POINT));
    cudaMalloc(&d_collisions, THREADS*sizeof(DPCOLLISION));
    cudaMalloc(&d_collision_results, THREADS*sizeof(VALUECOLLISION));

    while (functionVersion < NUMBER_OF_FUNCTIONVERSIONS) {

        iter_start = clock();

        unsigned long long totalPoints = 0;
        unsigned long long totalRobinHoods = 0;
        unsigned long long totalCollisions = 0;

        //memset(memory, 0, W*sizeof(DIST_POINT));
        //memset(points, 0, THREADS*sizeof(DIST_POINT));
        //memset(collisions, 0, THREADS*sizeof(COLLISION));
        //memset(collision_results, 0, THREADS*sizeof(VALUECOLLISION));
        //cudaMemset(d_collisions, 0, THREADS*sizeof(COLLISION));
        //cudaMemset(d_collision_results, 0, THREADS*sizeof(VALUECOLLISION));

        //only need to zero out d_points, because distinguishedness depends on the function version
        cudaMemset(d_points, 0, THREADS*sizeof(DIST_POINT));

        //invoke the stateKernel to setup the state for future randomizations
        setupState<<<B,T>>>(d_states, rand(), functionVersion);
        cudaDeviceSynchronize();
        cudaError_t error = cudaGetLastError();

        unsigned long num_collisions = 0;

        //for each function version we find 10*W distinguished points
        do {
            //variables for statistics
            unsigned long pointsFound           = 0;
            unsigned long robinHoods            = 0;
            unsigned long collisionsFound   = 0;

            //dynamically allocate shared memory to use in the kernel
            //size_t shared_size = T*sizeof(DIST_POINT);
            findDistinguishedPoint<<<B,T>>>(d_states, d_points, functionVersion, gc, d_count_list);

            cudaMemcpy(count_list, d_count_list, THREADS*sizeof(unsigned long long), cudaMemcpyDeviceToHost);
            for (int c = 0; c < THREADS; c++) {
                count_findDist += count_list[c];
            }
            
            //copy results from device memory to host memory
            cudaMemcpy(points, d_points, THREADS*sizeof(DIST_POINT), cudaMemcpyDeviceToHost);
            
            //write the distinguished points we found into the memory and store potential collisions in the collisions array
            int_tuple res = writeBackPoints(memory, points, collisions, THREADS, pointsFound, num_collisions);
            pointsFound = res.a;
            num_collisions = res.b;
            totalPoints += pointsFound;

            //printf("CollisionsFound: %lu\n", num_collisions);
            //printf("Occupied ratio: %f\n", occupiedRatio(memory));

            //if we found enough collisions, or its the last iteration, run the collision-find kernel
            if (num_collisions >= THREADS || totalPoints >= 10*W) {
                cudaMemcpy(d_collisions, collisions, THREADS*sizeof(DPCOLLISION), cudaMemcpyHostToDevice);
                locateCollision<<<B,T>>>(d_collisions, MIN(THREADS,num_collisions), d_collision_results, functionVersion, gc, d_count_list);

                //after copying to device memory, we put the excess collisions found to the start of the array
                if (num_collisions > THREADS)
                    memcpy(collisions, (collisions + THREADS), (num_collisions - THREADS) * sizeof(DPCOLLISION));

                cudaMemcpy(count_list, d_count_list, THREADS*sizeof(unsigned long long), cudaMemcpyDeviceToHost);
                for (int c = 0; c < THREADS; c++) {
                    count_findColl += count_list[c];
                }

                cudaMemcpy(collision_results, d_collision_results, THREADS*sizeof(VALUECOLLISION), cudaMemcpyDeviceToHost);

                for (int j = 0; j < MIN(THREADS,num_collisions); j++) {

                    //check if its a robin hood (essentially of both values are 0)
                    if (equal(collision_results[j].a, collision_results[j].b)) {
                        robinHoods++;
                        continue;
                    }

                    collisionsFound++;

                    if (isGoldenCollision(collision_results[j], gc)) {
                        printf("SUCCEDED: ");
                        totalCollisions += collisionsFound;
                        totalRobinHoods += robinHoods; 
                        goto end;
                    }
                }

                num_collisions = num_collisions % THREADS;
                totalCollisions += collisionsFound;
                totalRobinHoods += robinHoods;
            }
            
                
        } while (totalPoints < BETA*W);
        
        functionVersion++;

        //printing and timing
        iter_end = clock();
        if (functionVersion % 5 == 0) {
            printf("%dth function version: ", functionVersion);
            printf("%f | ", (iter_end - iter_start) / (float) CLOCKS_PER_SEC);
            printf("P: %llu | R: %llu | TC: %llu\n", totalPoints, totalRobinHoods, totalCollisions);
            printf("Count Total: %llu, Ratio: %f. |||\n", count_findDist + count_findColl, (float) count_findDist / (float) (count_findDist + count_findColl));
        }
    }

    end:
    gc_end = clock();
    printf("Total time: %f, ", (gc_end - gc_start) / (float) CLOCKS_PER_SEC);
    printf("Needed %d function versions\n", functionVersion);
    print_goldencollision(gc);

    //counter stuff
    printf("Count Total: %llu, Ratio: %f.\n", count_findDist + count_findColl, (float) count_findDist / (float) (count_findDist + count_findColl));

    //free all allocated memory.
    free(memory);
    free(points);
    free(collision_results);
    free(collisions);
    cudaFree(d_points);
    cudaFree(d_states);
    cudaFree(d_collision_results);
    cudaFree(d_collisions);
    free(count_list);
    cudaFree(d_count_list);


    cudaDeviceReset();

    //writing results into files...
    FILE *filePtr;
    FILE *timePtr;

    filePtr = fopen("output.txt", "a");
    timePtr = fopen("time.txt", "a");

    if (filePtr == NULL)
        printf("Error: opening file to write to it didn't work");

    if (timePtr == NULL)
        printf("Error: opening file to write to it didn't work");

    fprintf(filePtr, "%d\n", functionVersion);
    fprintf(filePtr, "%llu\n", (count_findDist + count_findColl));

    fprintf(timePtr, "%f\n", (gc_end - gc_start) / (float) CLOCKS_PER_SEC);

    //sleep for 2 seconds such that subsequent calls to main don't have the same time(NULL) seed
    sleep(2);

    return 0;
}

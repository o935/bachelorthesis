
#include <cuda_runtime_api.h>

#include "config.h"

//============================================
//=============PRINTING FUNCTIONS=============
//============================================

__host__ __device__ void print_hex(BYTE *s) {
    for (int ii = 0; ii < SIZE; ii++)
        printf("%02x", (unsigned int) *s++);
    printf("\n");
}

__host__ __device__ void print_point(DIST_POINT point) {
	BYTE* start = point.start;
	BYTE* end = point.end;
	printf("Triple:: (");
	for (int ii = 0; ii < SIZE; ii++)
        printf("%02x", (unsigned int) *start++);
	printf(", ");
	for (int ii = 0; ii < SIZE; ii++)
        printf("%02x", (unsigned int) *end++);
	printf(", %d, %d, %d)\n", point.steps, point.valid & 0x7F, point.valid >> 7);
}

__host__ __device__ void print_collision(DPCOLLISION col) {
    printf("====Collision====\n");
    print_point(col.a);
    print_point(col.b);
}

__host__ __device__ void print_VALUECOLLISION(VALUECOLLISION hcol) {
    printf("====VALUECOLLISION====\n");
    print_hex(hcol.a);
    print_hex(hcol.b);
}

__host__ __device__ void print_goldencollision(GOLDENCOLLISION gc) {
    printf("====Golden Collision====\n");
    print_hex(gc.value1);
    print_hex(gc.value2);
    printf("Both hash to: \n");
    print_hex(gc.result);
}
/////////////////////////////////////////////////

//returns a randomized golden collision
__host__ GOLDENCOLLISION getGoldenCollision() {
    srand(time(NULL));

    GOLDENCOLLISION gc;
    //generates random golden collision
    for (int i = 0; i < SIZE; i++) {
        gc.value1[i] = rand() % 256;
        gc.value2[i] = rand() % 256;
        gc.result[i] = rand() % 256;
        if (gc.value1[i] == gc.value2[i])
            gc.value1[i] = (gc.value1[i] + 1) % 256;
        if (gc.value1[i] == gc.result[i])
            gc.value1[i] = (gc.value1[i] + 1) % 256;
        if (gc.value2[i] == gc.result[i])
            gc.value2[i] = (gc.value1[i] + 1) % 256;
    }
    gc.value1[SIZE-1] = gc.value1[SIZE-1] & MASK;
    gc.value2[SIZE-1] = gc.value2[SIZE-1] & MASK;
    gc.result[SIZE-1] = gc.result[SIZE-1] & MASK;

    return gc;
}


//returns a randomize functionVersion
__host__ unsigned int getRandomFunctionVersion() {
    BYTE random1 = rand() % 128;
    BYTE random2 = rand() % 256;
    BYTE random3 = rand() % 256;
    BYTE random4 = rand() % 256;
    unsigned int functionVersion = (random1 << 24) + (random2 << 16) + (random3 << 8) + (random4);

    return functionVersion;
}


//Example: if functionversion is 5e099f48, then functionversion + 1 = 5e099f49
//now if ZEROBITS = 6 then distinguished points are of the form: 0100 (4) and 10XX (because of the 8, 2 bits must be correct) XXXX XXXX.....
//ie this is a distinguished point then: (78aa95, 4bbc85, 7, 0, 1) because 4b = 0100 1011

//a distinguished point is distinguished iff the first zerobits bits are 
//equal to the first zerobits bits of the functionversion
__device__ int distinguished(BYTE* point, BYTE* functionVersion) {
    //first check that the required number of bytes are equal
    for (int i = 0; i < ZEROBITS / 8; i++) {
        if (point[i] != functionVersion[i]) {
            return FALSE;
        }
    }
    //check that the remaining number of bits are equal
    int j = ZEROBITS % 8;
    BYTE p = point[ZEROBITS / 8] >> (8-j);
    BYTE f = functionVersion[ZEROBITS / 8] >> (8-j);

    return p == f;
}

//get's the num-th bit of the hash value
__host__ int getBit(BYTE* hash, int num) {


    int byte = num / 8;
    int byte_offset = num % 8;
    BYTE loc = hash[byte];
    int bit = loc >> (7-byte_offset) & 0x01;

    return bit;
}

//function that given a hash of a distinguished point, calculates the index
__host__ int getIndex(BYTE* hash) {
    //number of bits we need to index all memory locations for distinguished points
    int numBits = log2(W);
    int index = 0;
    int j = numBits-1;
    //we can't use the zerobits for indexing obviously
    for (int i = ZEROBITS; i < ZEROBITS+numBits; i++) {
        int bit = getBit(hash, i);
        index += bit*pow(2,j);
        j--;
    }

    return index;
}

//given pointer to memory with W distinguished points, return factor of how much of the memory is occupied.
__host__ float occupiedRatio(DIST_POINT* memory) {

    float occupied = 0;

    for (int i = 0; i < W; i++) {

        if (memory[i].valid >> 7 == 1)
            occupied++;
    }

    return occupied / (float) W;
}

//returns true of the given input hashes aren't equal
__host__ __device__ int notEqual(BYTE* a, BYTE* b) {
	for (int i = 0; i < SIZE; i++) {
		if (a[i] != b[i]) {
			return TRUE;
		}
	}
	return FALSE;
}

__host__ __device__ int equal(BYTE* a, BYTE* b) {
    for (int i = 0; i < SIZE; i++) {
        if (a[i] != b[i]) {
            return FALSE;
        }
    }
    return TRUE;
}

//returns true if the end hash values are identical, but the start values are not.
__host__ int isCollision(DIST_POINT* a, DIST_POINT* b) {
    for (int i = 0; i < SIZE; i++) {
        if (a->end[i] != b->end[i]) {
            return FALSE;
        }
    }

	for (int i = 0; i < SIZE; i++) {
		if (a->start[i] != b->start[i]) {
			return TRUE;
		}
	}
    return FALSE;
}

//returns true of the given collision is the golden collision
__host__ int isGoldenCollision(VALUECOLLISION coll, GOLDENCOLLISION gc) {
    return (equal(coll.a, gc.value1) && equal(coll.b, gc.value2)) 
    || (equal(coll.b, gc.value1) && equal(coll.a, gc.value2));
}
/*
//return true if given point is a distinguished point, via number of bits instead of bytes
__device__ int isDistinguishedPoint(BYTE* point) {
    //first check that the required number of bytes are zero
    for (int i = 0; i < ZEROBITS / 8; i++) {
        if (point[i] != 0) {
            return FALSE;
        }
    }
    //check that the remaining number of bits are zero
    int j = ZEROBITS % 8;

    return (point[ZEROBITS / 8] < pow(2, 8-j));
}
*/

//returns number of Cores of given graphics card (old code)
__host__ int getNumberOfCores(cudaDeviceProp devProp) {

    int cores = 0;
    int mp = devProp.multiProcessorCount;
    switch (devProp.major) {
        case 2:
            if (devProp.minor == 1) cores = mp* 48;
            else cores = mp * 32;
            break;
        case 3:
            cores = mp * 192;
            break;
        case 5:
            cores = mp * 128;
            break;
        case 6:
            if ((devProp.minor == 1) || (devProp.minor == 2)) cores = mp * 128;
            else if (devProp.minor == 0) cores = mp * 64;
            else printf("unknown device type \n");
            break;
        case 7:
            if ((devProp.minor == 0) || (devProp.minor == 5)) cores = mp * 64;
            else printf("unknown device type \n");
            break;
        case 8:
            if (devProp.minor == 0) cores = mp * 64;
            else if (devProp.minor == 6) cores = mp * 128;
            else printf("unknown device type \n");
            break;
        default:
            printf("unknown device type \n");
            break;
    }

	return cores;
}

//writes found distinguished points into correct location of memory, returns number of points found
__host__ int_tuple writeBackPoints(DIST_POINT* memory, DIST_POINT* point_array, DPCOLLISION* col_array, unsigned long num, int p, int c) {

    int pointsFound = p;
    int num_collisions = c;

    for (int i = 0; i < num; i++) {
            DIST_POINT point = point_array[i];
            
            //if the valid bit isn't set, we don't need to look at it
            if (point.valid >> 7 == 0)
                continue;
            pointsFound++;

            //print_point(point);
            //printf("%x\n", point.end[0]);
            
            int index = getIndex(point.end);

            if (memory[index].valid >> 7 == 0 || notEqual(memory[index].end, point.end)) {
                //TODO: rephrase these conditions, since we dont really care about the distinct num...
            } else if (notEqual(memory[index].start, point.start)) {
                //if the start hash-values are not equal, we found a collision
                if (memory[index].steps > point.steps) {
                    memcpy(&col_array[num_collisions].a, &point, sizeof(DIST_POINT));
                    memcpy(&col_array[num_collisions].b, &memory[index], sizeof(DIST_POINT));
                } else {
                    memcpy(&col_array[num_collisions].a, &memory[index], sizeof(DIST_POINT));
                    memcpy(&col_array[num_collisions].b, &point, sizeof(DIST_POINT));
                }
                num_collisions++;
            } else {
                continue;
            }
            //write the newly found distinguished point to memory.
            memcpy(memory + index, &point, sizeof(DIST_POINT));
        }

        int_tuple ret;
        ret.a = pointsFound;
        ret.b = num_collisions;

        return ret;
}


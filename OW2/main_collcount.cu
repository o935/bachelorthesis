#include <device_launch_parameters.h>
#include <cuda_runtime.h>
#include <cuda.h>
#include <curand_kernel.h>
#include <time.h>
#include <cuda_profiler_api.h>

#include "md2.cu"


extern __shared__ DIST_POINT curr[];

__global__ void setupStateKernel(curandState* state, unsigned long seed, unsigned long functionVersion) {
    //TODO: maybe add a salt (possibly also the functionVersion?)
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    curand_init(seed, idx, functionVersion, &state[idx]);
}

__global__ void findDistinguishedPoint(curandState* states, DIST_POINT* d_dist_points, unsigned long functionVersion, unsigned long long* counter) {

    unsigned long long numSteps = 0;
    unsigned long long totalSteps = pow(2, ZEROBITS);
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    curandState localState = states[idx];

    //TODO: is it better if only one thread brings in the entire memory? or this way?
    extern __shared__ DIST_POINT curr[];
    curr[threadIdx.x] = d_dist_points[idx];
    __syncthreads();

    //printf("%d", __isLocal(&numSteps));

    //counter stuff
    unsigned long long count = 0;


    if (curr[threadIdx.x].valid == 0) {
        //point was either never written to, or is valid hence we start at new seed
        for (int i = 0; i < HASHSIZE; i++) {
            float myRandF = curand_uniform(&localState);
            myRandF *= 255.999999;
            curr[threadIdx.x].start[i] = curr[threadIdx.x].end[i] = (BYTE) truncf(myRandF);
        }
        curr[threadIdx.x].valid = 20;
        curr[threadIdx.x].steps = 0;
    } else {
        //point wasn't valid / we need to resume from where we left off
        curr[threadIdx.x].valid -= 1;
    }

    while (numSteps < totalSteps) {
        if (!isDistinguishedPoint(curr[threadIdx.x].end)) {
            hash(curr[threadIdx.x].end, HASHSIZE, curr[threadIdx.x].end, functionVersion);
            numSteps++;
            count++;
        } else {
            curr[threadIdx.x].steps += numSteps;
            curr[threadIdx.x].valid = 0;
            break;
        }
    }

    if (numSteps >= totalSteps) {
        curr[threadIdx.x].steps += totalSteps;
        //catch points which didnt make it in 20 attempts (to not pass them down as valid since valid == 0 for them)
        if (curr[threadIdx.x].valid == 0) {
            //TODO: maybe a better way rather than just setting steps = 0? we need to distinguish between points which just
            //havent been found, even after 20 tries (valid = 0) and points which are valid (also valid = 0)
            curr[threadIdx.x].steps = 0;
        }
    }

    __syncthreads();
    d_dist_points[idx] = curr[threadIdx.x];

    //counter stuff
    counter[idx] = count;
}

__global__ void findCollision(COLLISION* d_collisions, int num_cols, HASHCOLLISION* results, unsigned long functionVersion, unsigned long long* counter) {

    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    if (idx >= num_cols) {
        //counter stuff
        counter[idx] = 0;
        return;
    }

    COLLISION col;

    //counter stuff
    unsigned long long count = 0;

    //memcpy(&col, &d_collisions[idx], sizeof(COLLISION));
    col = d_collisions[idx];
    
    BYTE collision_hash_1[HASHSIZE];
    BYTE collision_hash_2[HASHSIZE];
    BYTE cur_hash_a[HASHSIZE];
    BYTE cur_hash_b[HASHSIZE];

    for (int cp = 0; cp < HASHSIZE; cp++) {
        cur_hash_a[cp] = col.a.start[cp];
        cur_hash_b[cp] = col.b.start[cp];
    }

    while (col.a.steps < col.b.steps) {
        //counter stuff
        count++;
        hash (cur_hash_b, HASHSIZE, cur_hash_b, functionVersion);
        col.b.steps -= 1;
    }

    //Robin hoods
    if (hashEqual(cur_hash_a, cur_hash_b)) {
        for (int cp = 0; cp < HASHSIZE; cp++) {
            results[idx].a[cp] = 0;
            results[idx].b[cp] = 0;
        }

        counter[idx] = count;
        return;
    }

    while (hashNotEqual(cur_hash_a, cur_hash_b) && col.a.steps > 0 && col.b.steps > 0) {
        for (int cp = 0; cp < HASHSIZE; cp++) {
            collision_hash_1[cp] = cur_hash_b[cp];
            collision_hash_2[cp] = cur_hash_a[cp];
        }
        hash(cur_hash_b, HASHSIZE, cur_hash_b,functionVersion);
        col.b.steps -= 1;
        hash(cur_hash_a, HASHSIZE, cur_hash_a,functionVersion);
        col.a.steps -= 1;
        //counter stuff
        count = count + 2;
    }

    for (int cp = 0; cp < HASHSIZE; cp++) {
        results[idx].a[cp] = collision_hash_1[cp];
        results[idx].b[cp] = collision_hash_2[cp];
    }

    //counter stuff
    counter[idx] = count;
}

int main () {

    clock_t gc_start, gc_end;
    gc_start = clock();

    unsigned long functionVersion = INITIAL_FUCTIONVERSION;

    curandState* d_states;
    DIST_POINT* h_memory;
    DIST_POINT* h_dist_points;
    DIST_POINT* d_dist_points;
    COLLISION* h_collisions;
    COLLISION* d_collisions;
    HASHCOLLISION* h_collision_results;
    HASHCOLLISION* d_collision_results;

    //count stuff
    unsigned long long count_dist = 0;
    unsigned long long count_coll = 0;
    unsigned long long* h_countlist;
    h_countlist = (unsigned long long*) calloc(THREADS, sizeof(unsigned long long));
    unsigned long long* d_countlist;
    if (cudaSuccess != cudaMalloc(&d_countlist, THREADS*sizeof(unsigned long long))) {
        printf("Error d_countlist malloc: %d\n", cudaGetLastError());
    }
    cudaMemcpy(d_countlist, h_countlist, THREADS*sizeof(unsigned long long), cudaMemcpyHostToDevice);
    //count stuff end

    h_memory            = (DIST_POINT*)     calloc(W, sizeof(DIST_POINT));
    h_dist_points       = (DIST_POINT*)     calloc(THREADS, sizeof(DIST_POINT));
    h_collisions        = (COLLISION*)      calloc(THREADS, sizeof(COLLISION));
    h_collision_results = (HASHCOLLISION*)  calloc(THREADS, sizeof(HASHCOLLISION));

    if (cudaSuccess != cudaMalloc(&d_states, THREADS*sizeof(curandState))) {
        printf("Error d_states malloc: %d\n", cudaGetLastError());
    }
    if (cudaSuccess != cudaMalloc(&d_dist_points, THREADS*sizeof(DIST_POINT))) {
        printf("Error d_dist_points malloc: %d\n", cudaGetLastError());
    }
    if (cudaSuccess != cudaMalloc(&d_collisions, THREADS*sizeof(COLLISION))) {
        printf("Error d_collisions malloc: %d\n", cudaGetLastError());
    }
    if (cudaSuccess != cudaMalloc(&d_collision_results, THREADS*sizeof(HASHCOLLISION))) {
        printf("Error d_collisions_results malloc: %d\n", cudaGetLastError());
    }

    while (functionVersion < INITIAL_FUCTIONVERSION + NUMBER_OF_FUNCTIONVERSIONS) {

        unsigned long long totalCollisions                  = 0;
        unsigned long long totalDistinctDistinguishedPoints = 0;
        unsigned long long totalDistinguishedPoints         = 0;
        unsigned long long totalRobinHoods                  = 0;
        unsigned long long totalTrueCollisions              = 0;

        memset(h_memory, 0, W*sizeof(DIST_POINT));
        memset(h_dist_points, 0, THREADS*sizeof(DIST_POINT));
        memset(h_collisions, 0, THREADS*sizeof(COLLISION));
        memset(h_collision_results, 0, THREADS*sizeof(HASHCOLLISION));
        cudaMemset(d_dist_points, 0, THREADS*sizeof(DIST_POINT));
        cudaMemset(d_collisions, 0, THREADS*sizeof(COLLISION));
        cudaMemset(d_collision_results, 0, THREADS*sizeof(HASHCOLLISION));

        unsigned long pointsFound           = 0;
        unsigned long distinctPointsFound   = 0;
        unsigned long collisionsFound       = 0;
        unsigned long robinHoods            = 0;
        unsigned long trueCollisionsFound   = 0;

        for (int iter = 0; iter < NUMBER_OF_ITERATIONS; iter++) {

            clock_t iter_start = clock();

            pointsFound           = 0;
            distinctPointsFound   = 0;

            setupStateKernel<<<B,T>>>(d_states, time(NULL) + rand(), functionVersion);
            cudaDeviceSynchronize();
            cudaError_t error = cudaGetLastError();
            if (error != cudaSuccess) {
                printf("Error setUpKernel: %s\n", cudaGetErrorString(error));
                exit(-1);
            }

            size_t shared_size = T*sizeof(DIST_POINT);

            findDistinguishedPoint<<<B,T, shared_size>>>(d_states, d_dist_points, functionVersion, d_countlist);
            cudaDeviceSynchronize();
            error = cudaGetLastError();
            if (error != cudaSuccess) {
                printf("Error findDistinguishedPoint Kernel: %s\n", cudaGetErrorString(error));
                exit(-1);
            }

            //counter stuff
            cudaMemcpy(h_countlist, d_countlist, THREADS*sizeof(unsigned long long), cudaMemcpyDeviceToHost);
            for (int c = 0; c < THREADS; c++) {
                count_dist += h_countlist[c];
                //reset..
                h_countlist[c] = 0;
            }
            //counter stuff end

            cudaMemcpy(h_dist_points, d_dist_points, THREADS*sizeof(DIST_POINT), cudaMemcpyDeviceToHost);

            for (int i = 0; i < THREADS; i++) {
                DIST_POINT point = h_dist_points[i];
                //print_point(point);
                //TODO: point.steps == 0 is kind of a hack to detect "not found" points...
                if (point.valid != 0 || point.steps == 0)
                    continue;

                pointsFound++;
                //TODO: doesn't work for any W at the moment.
                int index = ((point.end[HASHSIZE-2]) * 256 + point.end[HASHSIZE-1]) % W;

                if (h_memory[index].valid != 0 || hashNotEqual(h_memory[index].end, point.end)) {
                    //TODO: not true if W is smaller than #distpoints
                    distinctPointsFound++;
                } else if (isCollision(&h_memory[index], &point)) {
                    //TODO: replace "isCollision" with "hashEqual(start, other start)" since from aboves "if" we know end arent equal...
                    if (h_memory[index].steps > point.steps) {
                        memcpy(&h_collisions[collisionsFound].a, &point, sizeof(DIST_POINT));
                        memcpy(&h_collisions[collisionsFound].b, &h_memory[index], sizeof(DIST_POINT));
                    } else {
                        memcpy(&h_collisions[collisionsFound].a, &h_memory[index], sizeof(DIST_POINT));
                        memcpy(&h_collisions[collisionsFound].b, &point, sizeof(DIST_POINT));
                    }
                    collisionsFound++;
                } else {
                    continue;
                }
                //TODO: replace with "h_memory + index"
                memcpy(&h_memory[index], &point, sizeof(DIST_POINT));
            }

            //TODO: only works with current version, since we only expect about 300 collisions per iteration with this W...
            if (collisionsFound >= 0.75*THREADS || iter == NUMBER_OF_ITERATIONS - 1) {

                cudaMemcpy(d_collisions, h_collisions, THREADS*sizeof(COLLISION), cudaMemcpyHostToDevice);
                findCollision<<<B,T>>>(d_collisions, collisionsFound, d_collision_results, functionVersion, d_countlist);
                cudaDeviceSynchronize();
                error = cudaGetLastError();
                if (error != cudaSuccess) {
                    printf("Error findCollision Kernel: %s\n", cudaGetErrorString(error));
                    exit(-1);
                }

                //counter stuff
                cudaMemcpy(h_countlist, d_countlist, THREADS*sizeof(unsigned long long), cudaMemcpyDeviceToHost);

                for (int c = 0; c < THREADS; c++) {
                    count_coll += h_countlist[c];
                    //reset..
                    h_countlist[c] = 0;
                }
                //counter stuff end

                cudaMemcpy(h_collision_results, d_collision_results, THREADS*sizeof(HASHCOLLISION), cudaMemcpyDeviceToHost);

                //printf("CollisionsFound: %lu\n", collisionsFound);

                for (int j = 0; j < collisionsFound; j++) {

                    if (hashEqual(h_collision_results[j].a, h_collision_results[j].b)) {
                        robinHoods++;
                        continue;
                    }
                    trueCollisionsFound++;

                    if (isGoldenCollision(h_collision_results[j])) {
                        printf("FOUND GOLDEN COLLISION:\n");
                        print_hex(h_collision_results[j].a);
                        print_hex(h_collision_results[j].b);
                        gc_end = clock();
                        goto end;
                    }
                }

                totalCollisions += collisionsFound;
                totalRobinHoods += robinHoods;
                totalTrueCollisions += trueCollisionsFound;

                collisionsFound       = 0;
                robinHoods            = 0;
                trueCollisionsFound   = 0;
            }

            totalDistinctDistinguishedPoints += distinctPointsFound;
            totalDistinguishedPoints += pointsFound;

            clock_t iter_end = clock();

            if (iter == NUMBER_OF_ITERATIONS - 1) {
                printf("%luth function version, ", functionVersion - INITIAL_FUCTIONVERSION);
                printf("%dth iteration (%fs):", iter, (iter_end - iter_start) / (float) CLOCKS_PER_SEC);
                printf("P: %llu, D: %llu, C: %llu, R: %llu, TC: %llu\n", totalDistinguishedPoints, 
                    totalDistinctDistinguishedPoints, totalCollisions, totalRobinHoods, totalTrueCollisions);
                //counter stuff
                printf("Count Dist: %llu, Count Coll: %llu, Ratio: %f. |||\n", count_dist, count_coll, (float) count_dist / (float) (count_dist + count_coll));
                //counter stuff end
            }
        }

        functionVersion++;
    }

    end:
    printf("Total time: %f\n", (gc_end - gc_start) / (float) CLOCKS_PER_SEC);
    printf("Needed %lu function versions", functionVersion - INITIAL_FUCTIONVERSION);

    free(h_memory);
    free(h_dist_points);
    free(h_collision_results);
    free(h_collisions);
    cudaFree(d_dist_points);
    cudaFree(d_states);
    cudaFree(d_collision_results);
    cudaFree(d_collisions);
    //TODO: don't forget to free the counter stuff etc.
    free(h_countlist);
    cudaFree(d_countlist);

    return 0;
}
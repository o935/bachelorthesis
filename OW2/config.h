#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <memory.h>
#include <math.h>

typedef unsigned char BYTE;
typedef unsigned int WORD;
typedef unsigned long long LONG;
#define TRUE 1
#define FALSE 0

#define MIN(X,Y) (((X) < (Y)) ? (X) : (Y))

//=======Variables===================================================
//number of threads and blocks
#define T 128 //64
#define B 24 //48
#define THREADS (int(B*T))
//length of hash in bits
#define N 22
//number of Distinguished points that can be stored in memory
#define LOGW 16

////////////////////////////////////
#define ALPHA 2.25
#define BETA 10
#define GAMMA 20
#define W (1 << LOGW)
#define SIZE ((N + 7) / 8)
#define MASK ((255 << (8 * SIZE - N)) % 256)
//number of zerobits for a distinguished point
#define ZEROBITS (((N - LOGW) >> 1) - 1)
//number of iterations of finding distinguished points and locating collisions
//per functionversion
//maximum number of functionversions that we try
#define NUMBER_OF_FUNCTIONVERSIONS 10000000//1000
//factor which determines how fine-grained the kernel calls are
#define GAMMA_FACTOR 1

typedef struct  {
	//currently an int, might have to make larger later, otherwise overflow
    unsigned int steps; //4 bytes
	BYTE start[SIZE]; //SIZE bytes
	BYTE end[SIZE]; //SIZE bytes
	BYTE valid; //1 byte
} DIST_POINT;

typedef struct  {
    DIST_POINT a;
    DIST_POINT b;
} DPCOLLISION;

typedef struct  {
    BYTE a[SIZE];
    BYTE b[SIZE];
} VALUECOLLISION;

typedef struct  {
	BYTE value1[SIZE];
	BYTE value2[SIZE];
	BYTE result[SIZE];
} GOLDENCOLLISION;

typedef struct {
	int a;
	int b;
} int_tuple;

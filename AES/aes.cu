#include "aes.h"

// subbyte operation
__device__ void aes_subBytes(uint8_t *buf){
  register uint8_t i, b;
  for (i = 0; i < 16; ++i){
    b = buf[i];
    buf[i] = sbox[b];
  }
} 

// add round key operation
__device__ void aes_addRoundKey(uint8_t *buf, uint8_t *key){
  register uint8_t i = 16;
  while (i--){
    buf[i] ^= key[i];
  }
} 

// add round key at beginning
__device__ void aes_addRoundKey_cpy(uint8_t *buf, uint8_t *key, uint8_t *cpk){
  register uint8_t i = 16;
  while (i--){
    buf[i] ^= (cpk[i] = key[i]);
    cpk[16+i] = key[16 + i];
  }
} 

// shift row operation
__device__ void aes_shiftRows(uint8_t *buf){
  register uint8_t i, j; 
  i = buf[1];
  buf[1] = buf[5];
  buf[5] = buf[9];
  buf[9] = buf[13];
  buf[13] = i;
  i = buf[10];
  buf[10] = buf[2];
  buf[2] = i;
  j = buf[3];
  buf[3] = buf[15];
  buf[15] = buf[11];
  buf[11] = buf[7];
  buf[7] = j;
  j = buf[14];
  buf[14] = buf[6];
  buf[6]  = j;
}

// x-time operation
__device__ uint8_t rj_xtime(uint8_t x){
  return (x & 0x80) ? ((x << 1) ^ 0x1b) : (x << 1);
}

// mix column operation
__device__ void aes_mixColumns(uint8_t *buf){
  register uint8_t i, a, b, c, d, e;
  for (i = 0; i < 16; i += 4){
    a = buf[i];
    b = buf[i + 1];
    c = buf[i + 2];
    d = buf[i + 3];
    e = a ^ b ^ c ^ d;
    buf[i] ^= e ^ rj_xtime(a^b);
    buf[i+1] ^= e ^ rj_xtime(b^c);
    buf[i+2] ^= e ^ rj_xtime(c^d);
    buf[i+3] ^= e ^ rj_xtime(d^a);
  }
} 

// add expand key operation
__device__ __host__ void aes_expandEncKey(uint8_t *k, uint8_t *rc, const uint8_t *sb){
  register uint8_t i;

  k[0] ^= sb[k[29]] ^ (*rc);
  k[1] ^= sb[k[30]];
  k[2] ^= sb[k[31]];
  k[3] ^= sb[k[28]];
  *rc = F( *rc);

  for(i = 4; i < 16; i += 4){
    k[i] ^= k[i-4];
    k[i+1] ^= k[i-3];
    k[i+2] ^= k[i-2];
    k[i+3] ^= k[i-1];
  }

  k[16] ^= sb[k[12]];
  k[17] ^= sb[k[13]];
  k[18] ^= sb[k[14]];
  k[19] ^= sb[k[15]];

  for(i = 20; i < 32; i += 4){
    k[i] ^= k[i-4];
    k[i+1] ^= k[i-3];
    k[i+2] ^= k[i-2];
    k[i+3] ^= k[i-1];
  }
} 

// key initition
void aes256_init(uint8_t *k){
  uint8_t rcon = 1;
  register uint8_t i;

  for (i = 0; i < sizeof(ctx_key); i++){
    ctx_enckey[i] = ctx_deckey[i] = k[i];
  }
  for (i = 8;--i;){
    aes_expandEncKey(ctx_deckey, &rcon, sbox);
  }
} 

// aes encrypt algorithm one thread/one block with AES_BLOCK_SIZE 
__global__ void aes256_encrypt_ecb(uint8_t *buf_d, unsigned long numbytes, uint8_t *ctx_enckey_d, uint8_t *ctx_key_d){
  uint8_t i, rcon;
  uint8_t buf_t[AES_BLOCK_SIZE]; // thread buffer
  unsigned long offset = (blockIdx.x * THREADS_PER_BLOCK * AES_BLOCK_SIZE) + (threadIdx.x * AES_BLOCK_SIZE);
  if (offset >= numbytes) {  
    return; 
  }

  memcpy(buf_t, &buf_d[offset], AES_BLOCK_SIZE);
  aes_addRoundKey_cpy(buf_t, ctx_enckey_d, ctx_key_d);
  for(i = 1, rcon = 1; i < 14; ++i){
    aes_subBytes(buf_t);
    aes_shiftRows(buf_t);
    aes_mixColumns(buf_t);
    if( i & 1 ){
      aes_addRoundKey( buf_t, &ctx_key_d[16]);
    }
    else{
      aes_expandEncKey(ctx_key_d, &rcon, sbox), aes_addRoundKey(buf_t, ctx_key_d);
    }
  }
  aes_subBytes(buf_t);
  aes_shiftRows(buf_t);
  aes_expandEncKey(ctx_key_d, &rcon, sbox);
  aes_addRoundKey(buf_t, ctx_key_d);
  /* copy thread buffer back into global memory */
  memcpy(&buf_d[offset], buf_t, AES_BLOCK_SIZE);
  __syncthreads();
} 

int main(){

  // open file
  FILE *file;
  uint8_t *buf; 
  unsigned long numbytes;
  unsigned long numbytes_original;
  char const *fname;
  int i;
  int padding;
  uint8_t key[32];

  //error handling
  int deviceCount = 0;
  cudaError_t error_id = cudaGetDeviceCount(&deviceCount);
  if (error_id != cudaSuccess){
    printf("Error: %s\n", cudaGetErrorString(error_id));
    printf("Exiting...\n");
    exit(EXIT_FAILURE);
  }
  if (deviceCount == 0){
    printf("There are no available device(s) that support CUDA\n");
    exit(EXIT_FAILURE);
  }


  // handle txt file
  fname = "input.txt";  
  file = fopen(fname, "r");
  if (file == NULL) {printf("File %s doesn't exist\n", fname); exit(1); }
  printf("==Opened file %s==\n", fname);
  fseek(file, 0L, SEEK_END);
  numbytes = ftell(file);
  printf("Input Size: %lu Bytes, ", numbytes);


 // calculate the padding
  padding = (AES_BLOCK_SIZE - (numbytes % AES_BLOCK_SIZE)) % 16;
  numbytes_original = numbytes;
  numbytes += padding;
  printf("Padding size: %d Bytes, New size: %lu\n\n", padding, numbytes);

  // copy file into memory
  fseek(file, 0L, SEEK_SET);
  buf = (uint8_t*)calloc(numbytes, sizeof(uint8_t));
  if(buf == NULL) exit(1);
  if (fread(buf, 1, numbytes_original, file) != numbytes_original) {
    printf("Unable to read all bytes from file %s\n", fname);
    exit(EXIT_FAILURE);
  }
  fclose(file);

  //print given input as hexadecimal
  printf("Given Input:\n");
  for (int l = 0; l < numbytes; l++) {
    printf("%02x ", buf[l]);
  }
  printf("\n-------------------\n");
 
  // generate and print secret key
  for (i = 0; i < sizeof(key);i++) key[i] = 65;
  printf("%d Bit secret key: ", ((int) sizeof(key) * 8));
  for (int ii = 0; ii < sizeof(key); ii++) {
    printf("%02x ", key[ii]);
  }
  printf("\n-------------------\n");

  // encryption
  uint8_t *buf_d;
  uint8_t *ctx_key_d, *ctx_enckey_d;
  //copy sbox into constant memory of device
  cudaMemcpyToSymbol(sbox, sbox, sizeof(uint8_t)*256);
  //initialize key
  aes256_init(key);
  //allocate memory on device for text and key
  cudaMalloc((void**)&buf_d, numbytes);
  cudaMalloc((void**)&ctx_enckey_d, sizeof(ctx_enckey));
  cudaMalloc((void**)&ctx_key_d, sizeof(ctx_key));
  //copy text and key to device memory
  cudaMemcpy(buf_d, buf, numbytes, cudaMemcpyHostToDevice);
  cudaMemcpy(ctx_enckey_d, ctx_enckey, sizeof(ctx_enckey), cudaMemcpyHostToDevice);
  cudaMemcpy(ctx_key_d, ctx_key, sizeof(ctx_key), cudaMemcpyHostToDevice);
  //calculate required block and grid dimensions
  dim3 dimGrid(ceil((double)numbytes / (double)(THREADS_PER_BLOCK * AES_BLOCK_SIZE)));
  dim3 dimBlock(THREADS_PER_BLOCK);
  aes256_encrypt_ecb<<<dimGrid, dimBlock>>>(buf_d, numbytes, ctx_enckey_d, ctx_key_d);
  //copy encrypted text and key from device to host
  cudaMemcpy(buf, buf_d, numbytes, cudaMemcpyDeviceToHost);
  cudaMemcpy(ctx_enckey, ctx_enckey_d, sizeof(ctx_enckey), cudaMemcpyDeviceToHost);
  cudaMemcpy(ctx_key, ctx_key_d, sizeof(ctx_key), cudaMemcpyDeviceToHost);
  //free memory
  cudaFree(buf_d);
  cudaFree(ctx_key_d);
  cudaFree(ctx_enckey_d);
  
  //write encrypted text into output file and print as hexadecimal
  file = fopen("output.txt", "w");
  printf("Encr. Input: \n");
  for (int k = 0; k < numbytes; k++) {
    printf("%02x ", buf[k]);
  }
  printf("\n-------------------\n");
  fwrite(buf, 1, numbytes, file);
  fclose(file);
  free(buf);
  return EXIT_SUCCESS;
}
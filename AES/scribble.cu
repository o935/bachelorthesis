#include <stdio.h>
#include <inttypes.h>

uint8_t* convert(char* c) {

    int len = strlen(c), i;
    uint8_t *a = (uint8_t*) malloc(len*sizeof(uint8_t));
    for(i = 0; i < len; i++) {
        a[i] = c[i]-48;
    }
    return a;
}

int main () {

    char c[100];
    scanf("%s", c);
    uint8_t *a = convert(c);
    for (int j = 0; j < 32); j++) {
        printf("%d\n", a[j]);
    }
    printf("First: %lu\n", sizeof(a));
    printf("%lu\n", sizeof(a[0]));
    free(a);

    return 0;
}
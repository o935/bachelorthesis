// chosen prime is p_32_20

#include <device_launch_parameters.h>
#include <cuda_runtime.h>
#include <cuda.h>
#include <curand_kernel.h>
#include <time.h>
#include <cuda_profiler_api.h>
#include <stdio.h>
#include <unistd.h>

#include "xof.cu"

//fpx.c

__device__ void digit_x_digit(const digit_t a, const digit_t b, digit_t* c)
{ // Digit multiplication, digit * digit -> 2-digit result    
    register digit_t al, ah, bl, bh, temp;
    digit_t albl, albh, ahbl, ahbh, res1, res2, res3, carry;
    digit_t mask_low = (digit_t)(-1) >> (sizeof(digit_t)*4), mask_high = (digit_t)(-1) << (sizeof(digit_t)*4);

    al = a & mask_low;                        // Low part
    ah = a >> (sizeof(digit_t) * 4);          // High part
    bl = b & mask_low;
    bh = b >> (sizeof(digit_t) * 4);

    albl = al*bl;
    albh = al*bh;
    ahbl = ah*bl;
    ahbh = ah*bh;
    c[0] = albl & mask_low;                   // C00

    res1 = albl >> (sizeof(digit_t) * 4);
    res2 = ahbl & mask_low;
    res3 = albh & mask_low;  
    temp = res1 + res2 + res3;
    carry = temp >> (sizeof(digit_t) * 4);
    c[0] ^= temp << (sizeof(digit_t) * 4);    // C01   

    res1 = ahbl >> (sizeof(digit_t) * 4);
    res2 = albh >> (sizeof(digit_t) * 4);
    res3 = ahbh & mask_low;
    temp = res1 + res2 + res3 + carry;
    c[1] = temp & mask_low;                   // C10 
    carry = temp & mask_high; 
    c[1] ^= (ahbh & mask_high) + carry;       // C11
}

__device__ void mp_mul(const digit_t* a, const digit_t* b, digit_t* c, const unsigned int nwords)
{ // Multiprecision comba multiply, c = a*b, where lng(a) = lng(b) = nwords.   
    unsigned int i, j;
    digit_t t = 0, u = 0, v = 0, UV[2];
    unsigned int carry = 0;
    
    for (i = 0; i < nwords; i++) {
        for (j = 0; j <= i; j++) {
            MUL(a[j], b[i-j], UV+1, UV[0]); 
            ADDC(0, UV[0], v, carry, v); 
            ADDC(carry, UV[1], u, carry, u); 
            t += carry;
        }
        c[i] = v;
        v = u; 
        u = t;
        t = 0;
    }

    for (i = nwords; i < 2*nwords-1; i++) {
        for (j = i-nwords+1; j < nwords; j++) {
            MUL(a[j], b[i-j], UV+1, UV[0]); 
            ADDC(0, UV[0], v, carry, v); 
            ADDC(carry, UV[1], u, carry, u); 
            t += carry;
        }
        c[i] = v;
        v = u; 
        u = t;
        t = 0;
    }
    c[2*nwords-1] = v; 
}

__device__ void fpadd128(const digit_t* a, const digit_t* b, digit_t* c)
{ // Modular addition, c = a+b mod p128.
  // Inputs: a, b in [0, 2*p128-1] 
  // Output: c in [0, 2*p128-1] 
    unsigned int i, carry = 0;
    digit_t mask;

    for (i = 0; i < NWORDS_FIELD; i++) {
        ADDC(carry, a[i], b[i], carry, c[i]); 
    }

    carry = 0;
    for (i = 0; i < NWORDS_FIELD; i++) {
        SUBC(carry, c[i], ((digit_t*)p128x2)[i], carry, c[i]); 
    }
    mask = 0 - (digit_t)carry;

    carry = 0;
    for (i = 0; i < NWORDS_FIELD; i++) {
        ADDC(carry, c[i], ((digit_t*)p128x2)[i] & mask, carry, c[i]); 
    }
} 

__device__ void fpsub128(const digit_t* a, const digit_t* b, digit_t* c)
{ // Modular subtraction, c = a-b mod p128.
  // Inputs: a, b in [0, 2*p128-1] 
  // Output: c in [0, 2*p128-1] 
    unsigned int i, borrow = 0;
    digit_t mask;

    for (i = 0; i < NWORDS_FIELD; i++) {
        SUBC(borrow, a[i], b[i], borrow, c[i]); 
    }
    mask = 0 - (digit_t)borrow;

    borrow = 0;
    for (i = 0; i < NWORDS_FIELD; i++) {
        ADDC(borrow, c[i], ((digit_t*)p128x2)[i] & mask, borrow, c[i]); 
    }
}

__device__ void fpneg128(digit_t* a)
{ // Modular negation, a = -a mod p128.
  // Input/output: a in [0, 2*p128-1] 
    unsigned int i, borrow = 0;

    for (i = 0; i < NWORDS_FIELD; i++) {
        SUBC(borrow, ((digit_t*)p128x2)[i], a[i], borrow, a[i]); 
    }
}

__device__ unsigned int mp_add(const digit_t *a, const digit_t *b, digit_t *c, const unsigned int nwords)
{ // Multiprecision addition, c = a+b, where lng(a) = lng(b) = nwords. Returns the carry bit.
    unsigned int i, carry = 0;

    for (i = 0; i < nwords; i++)
    {
        ADDC(carry, a[i], b[i], carry, c[i]);
    }

    return carry;
}

__device__ void rdc_mont(digit_t* ma, digit_t* mc)
{ // Efficient Montgomery reduction using comba and exploiting the special form of the prime p128.
  // mc = ma*R^-1 mod p128x2, where R = 2^128.
  // If ma < 2^128*p128, the output mc is in the range [0, 2*p128-1].
  // ma is assumed to be in Montgomery representation.
    unsigned int i, j, carry, count = p128_ZERO_WORDS;
    digit_t UV[2], t = 0, u = 0, v = 0;

    for (i = 0; i < NWORDS_FIELD; i++) {
        mc[i] = 0;
    }

    for (i = 0; i < NWORDS_FIELD; i++) {
        for (j = 0; j < i; j++) {
            if (j < (i-p128_ZERO_WORDS+1)) { 
                MUL(mc[j], ((digit_t*)p128p1)[i-j], UV+1, UV[0]);
                ADDC(0, UV[0], v, carry, v); 
                ADDC(carry, UV[1], u, carry, u); 
                t += carry; 
            }
        }
        ADDC(0, v, ma[i], carry, v); 
        ADDC(carry, u, 0, carry, u); 
        t += carry; 
        mc[i] = v;
        v = u;
        u = t;
        t = 0;
    }    

    for (i = NWORDS_FIELD; i < 2*NWORDS_FIELD-1; i++) {
        if (count > 0) {
            count -= 1;
        }
        for (j = i-NWORDS_FIELD+1; j < NWORDS_FIELD; j++) {
            if (j < (NWORDS_FIELD-count)) { 
                MUL(mc[j], ((digit_t*)p128p1)[i-j], UV+1, UV[0]);
                ADDC(0, UV[0], v, carry, v); 
                ADDC(carry, UV[1], u, carry, u); 
                t += carry;
            }
        }
        ADDC(0, v, ma[i], carry, v); 
        ADDC(carry, u, 0, carry, u); 
        t += carry; 
        mc[i-NWORDS_FIELD] = v;
        v = u;
        u = t;
        t = 0;
    }
    ADDC(0, v, ma[2*NWORDS_FIELD-1], carry, v); 
    mc[NWORDS_FIELD-1] = v;
}

__device__ void mp_shiftr1(digit_t *x, const unsigned int nwords)
{ // Multiprecision right shift by one.
    unsigned int i;

    for (i = 0; i < nwords - 1; i++)
    {
        SHIFTR(x[i + 1], x[i], 1, x[i], RADIX);
    }
    x[nwords - 1] >>= 1;
}

__device__ void mp_shiftl1(digit_t *x, const unsigned int nwords)
{ // Multiprecision left shift by one.
    int i;

    for (i = nwords - 1; i > 0; i--)
    {
        SHIFTL(x[i], x[i - 1], 1, x[i], RADIX);
    }
    x[0] <<= 1;
}

__device__ void fpcorrection128(digit_t* a)
{ // Modular correction to reduce field element a in [0, 2*p128-1] to [0, p128-1].
    unsigned int i, borrow = 0;
    digit_t mask;

    for (i = 0; i < NWORDS_FIELD; i++) {
        SUBC(borrow, a[i], ((digit_t*)p128)[i], borrow, a[i]); 
    }
    mask = 0 - (digit_t)borrow;

    borrow = 0;
    for (i = 0; i < NWORDS_FIELD; i++) {
        ADDC(borrow, a[i], ((digit_t*)p128)[i] & mask, borrow, a[i]); 
    }
}

__device__ void fpmul_mont(const felm_t ma, const felm_t mb, felm_t mc)
{ // Multiprecision multiplication, c = a*b mod p.
    dfelm_t temp = {0};

    mp_mul(ma, mb, temp, NWORDS_FIELD);
    rdc_mont(temp, mc);
}

__device__ void to_mont(const felm_t a, felm_t mc)
{   // Conversion to Montgomery representation,
    // mc = a*R^2*R^(-1) mod p = a*R mod p, where a in [0, p-1].
    // The Montgomery constant R^2 mod p is the global value "Montgomery_R2".

    fpmul_mont(a, (digit_t *)&Montgomery_R2, mc);
}

__device__ void from_mont(const felm_t ma, felm_t c)
{   // Conversion from Montgomery representation to standard representation,
    // c = ma*R^(-1) mod p = a mod p, where ma in [0, p-1].
    digit_t one[NWORDS_FIELD] = {0};

    one[0] = 1;
    fpmul_mont(ma, one, c);
    fpcorrection(c);
}

__device__ void to_fp2mont(const f2elm_t a, f2elm_t mc)
{   // Conversion of a GF(p^2) element to Montgomery representation,
    // mc_i = a_i*R^2*R^(-1) = a_i*R in GF(p^2).

    to_mont(a[0], mc[0]);
    to_mont(a[1], mc[1]);
}

__device__ void from_fp2mont(const f2elm_t ma, f2elm_t c)
{   // Conversion of a GF(p^2) element from Montgomery representation to standard representation,
    // c_i = ma_i*R^(-1) = a_i in GF(p^2).

    from_mont(ma[0], c[0]);
    from_mont(ma[1], c[1]);
}

__device__ void mp_shiftleft(digit_t *x, unsigned int shift, const unsigned int nwords)
{
    unsigned int i, j = 0;

    while (shift > RADIX)
    {
        j += 1;
        shift -= RADIX;
    }

    for (i = 0; i < nwords - j; i++)
        x[nwords - 1 - i] = x[nwords - 1 - i - j];
    for (i = nwords - j; i < nwords; i++)
        x[nwords - 1 - i] = 0;
    if (shift != 0)
    {
        for (j = nwords - 1; j > 0; j--)
            SHIFTL(x[j], x[j - 1], shift, x[j], RADIX);
        x[0] <<= shift;
    }
}

//fp_generic.c



__device__ void fpdiv2_128(const digit_t* a, digit_t* c)
{ // Modular division by two, c = a/2 mod p128.
  // Input : a in [0, 2*p128-1] 
  // Output: c in [0, 2*p128-1] 
    unsigned int i, carry = 0;
    digit_t mask;
        
    mask = 0 - (digit_t)(a[0] & 1);    // If a is odd compute a+p128
    for (i = 0; i < NWORDS_FIELD; i++) {
        ADDC(carry, a[i], ((digit_t*)p128)[i] & mask, carry, c[i]); 
    }

    mp_shiftr1(c, NWORDS_FIELD);
} 

__device__ void clear_words(void* mem, digit_t nwords)
{ // Clear digits from memory. "nwords" indicates the number of digits to be zeroed.
  // This function uses the volatile type qualifier to inform the compiler not to optimize out the memory clearing.
    unsigned int i;
    //change: pointer conversion
    volatile digit_t *v = (digit_t*) mem; 

    for (i = 0; i < nwords; i++) {
        v[i] = 0;
    }
}

__device__ void fp2_encode(const f2elm_t x, unsigned char *enc)
{ // Conversion of GF(p^2) element from Montgomery to standard representation, and encoding by removing leading 0 bytes
    unsigned int i;
    f2elm_t t;

    from_fp2mont(x, t);
    for (i = 0; i < FP2_ENCODED_BYTES / 2; i++) {
        enc[i] = ((unsigned char*)t)[i];
        enc[i + FP2_ENCODED_BYTES / 2] = ((unsigned char*)t)[i + MAXBITS_FIELD / 8];
    }
}

__device__ void fp2_decode(const unsigned char *enc, f2elm_t x)
{ // Parse byte sequence back into GF(p^2) element, and conversion to Montgomery representation
    unsigned int i;

    for (i = 0; i < 2*(MAXBITS_FIELD / 8); i++) ((unsigned char *)x)[i] = 0;
    for (i = 0; i < FP2_ENCODED_BYTES / 2; i++) {
        ((unsigned char*)x)[i] = enc[i];
        ((unsigned char*)x)[i + MAXBITS_FIELD / 8] = enc[i + FP2_ENCODED_BYTES / 2];
    }
    to_fp2mont(x, x);
}

__device__ void fpcopy(const felm_t a, felm_t c)
{ // Copy a field element, c = a.
    unsigned int i;

    for (i = 0; i < NWORDS_FIELD; i++)
        c[i] = a[i];
}

__device__ __inline void fpzero(felm_t a)
{ // Zero a field element, a = 0.
    unsigned int i;

    for (i = 0; i < NWORDS_FIELD; i++)
        a[i] = 0;
}

__device__ void copy_words(const digit_t *a, digit_t *c, const unsigned int nwords)
{ // Copy wordsize digits, c = a, where lng(a) = nwords.
    unsigned int i;

    for (i = 0; i < nwords; i++)
    {
        c[i] = a[i];
    }
}

__device__ void fpsqr_mont(const felm_t ma, felm_t mc)
{ // Multiprecision squaring, c = a^2 mod p.
    dfelm_t temp = {0};

    mp_mul(ma, ma, temp, NWORDS_FIELD);
    rdc_mont(temp, mc);
}

__device__ void fp2copy(const f2elm_t a, f2elm_t c)
{ // Copy a GF(p^2) element, c = a.
    fpcopy(a[0], c[0]);
    fpcopy(a[1], c[1]);
}

__device__ void fp2zero(f2elm_t a)
{ // Zero a GF(p^2) element, a = 0.
    fpzero(a[0]);
    fpzero(a[1]);
}

__device__ void fp2neg(f2elm_t a)
{ // GF(p^2) negation, a = -a in GF(p^2).
    fpneg(a[0]);
    fpneg(a[1]);
}

__device__ void fp2add(const f2elm_t a, const f2elm_t b, f2elm_t c)
{ // GF(p^2) addition, c = a+b in GF(p^2).
    fpadd(a[0], b[0], c[0]);
    fpadd(a[1], b[1], c[1]);
}

__device__ void fp2sub(const f2elm_t a, const f2elm_t b, f2elm_t c)
{ // GF(p^2) subtraction, c = a-b in GF(p^2).
    fpsub(a[0], b[0], c[0]);
    fpsub(a[1], b[1], c[1]);
}

__device__ void fp2div2(const f2elm_t a, f2elm_t c)
{ // GF(p^2) division by two, c = a/2  in GF(p^2).
    fpdiv2(a[0], c[0]);
    fpdiv2(a[1], c[1]);
}

__device__ void fp2correction(f2elm_t a)
{ // Modular correction, a = a in GF(p^2).
    fpcorrection(a[0]);
    fpcorrection(a[1]);
}

__device__ static void mp_addfast(const digit_t *a, const digit_t *b, digit_t *c)
{ // Multiprecision addition, c = a+b.
#if (OS_TARGET == OS_WIN) || defined(GENERIC_IMPLEMENTATION)

    mp_add(a, b, c, NWORDS_FIELD);

#elif (OS_TARGET == OS_LINUX)

    mp_add_asm(a, b, c);

#endif
}

__device__ void fp2sqr_mont(const f2elm_t a, f2elm_t c)
{   // GF(p^2) squaring using Montgomery arithmetic, c = a^2 in GF(p^2).
    // Inputs: a = a0+a1*i, where a0, a1 are in [0, 2*p-1]
    // Output: c = c0+c1*i, where c0, c1 are in [0, 2*p-1]
    felm_t t1, t2, t3;

    mp_addfast(a[0], a[1], t1); // t1 = a0+a1
    fpsub(a[0], a[1], t2);      // t2 = a0-a1
    mp_addfast(a[0], a[0], t3); // t3 = 2a0
    fpmul_mont(t1, t2, c[0]);   // c0 = (a0+a1)(a0-a1)
    fpmul_mont(t3, a[1], c[1]); // c1 = 2a0*a1
}

__device__ unsigned int mp_sub(const digit_t *a, const digit_t *b, digit_t *c, const unsigned int nwords)
{ // Multiprecision subtraction, c = a-b, where lng(a) = lng(b) = nwords. Returns the borrow bit.
    unsigned int i, borrow = 0;

    for (i = 0; i < nwords; i++)
    {
        SUBC(borrow, a[i], b[i], borrow, c[i]);
    }

    return borrow;
}

__device__ static void mp_subaddfast(const digit_t* a, const digit_t* b, digit_t* c)
{ // Multiprecision subtraction followed by addition with p*2^MAXBITS_FIELD, c = a-b+(p*2^MAXBITS_FIELD) if a-b < 0, otherwise c=a-b. 
#if (OS_TARGET == OS_WIN) || defined(GENERIC_IMPLEMENTATION)
    felm_t t1;

    digit_t mask = 0 - (digit_t)mp_sub(a, b, c, 2*NWORDS_FIELD);
    for (int i = 0; i < NWORDS_FIELD; i++)
        t1[i] = ((digit_t*)PRIME)[i] & mask;
    mp_addfast((digit_t*)&c[NWORDS_FIELD], t1, (digit_t*)&c[NWORDS_FIELD]);

#elif (OS_TARGET == OS_LINUX)               

    mp_subaddx2_asm(a, b, c);     

#endif
}

__device__ static void mp_dblsubfast(const digit_t *a, const digit_t *b, digit_t *c)
{   // Multiprecision subtraction, c = c-a-b, where lng(a) = lng(b) = 2*NWORDS_FIELD.
    // Inputs should be s.t. c > a and c > b
#if (OS_TARGET == OS_WIN) || defined(GENERIC_IMPLEMENTATION)

    mp_sub(c, a, c, 2 * NWORDS_FIELD);
    mp_sub(c, b, c, 2 * NWORDS_FIELD);

#elif (OS_TARGET == OS_LINUX)

    mp_dblsubx2_asm(a, b, c);

#endif
}

__device__ void fp2mul_mont(const f2elm_t a, const f2elm_t b, f2elm_t c)
{   // GF(p^2) multiplication using Montgomery arithmetic, c = a*b in GF(p^2).
    // Inputs: a = a0+a1*i and b = b0+b1*i, where a0, a1, b0, b1 are in [0, 2*p-1]
    // Output: c = c0+c1*i, where c0, c1 are in [0, 2*p-1]
    felm_t t1, t2;
    dfelm_t tt1, tt2, tt3;

    mp_addfast(a[0], a[1], t1);            // t1 = a0+a1
    mp_addfast(b[0], b[1], t2);            // t2 = b0+b1
    mp_mul(a[0], b[0], tt1, NWORDS_FIELD); // tt1 = a0*b0
    mp_mul(a[1], b[1], tt2, NWORDS_FIELD); // tt2 = a1*b1
    mp_mul(t1, t2, tt3, NWORDS_FIELD);     // tt3 = (a0+a1)*(b0+b1)
    mp_dblsubfast(tt1, tt2, tt3);          // tt3 = (a0+a1)*(b0+b1) - a0*b0 - a1*b1
    mp_subaddfast(tt1, tt2, tt1);          // tt1 = a0*b0 - a1*b1 + p*2^MAXBITS_FIELD if a0*b0 - a1*b1 < 0, else tt1 = a0*b0 - a1*b1
    rdc_mont(tt3, c[1]);                   // c[1] = (a0+a1)*(b0+b1) - a0*b0 - a1*b1 
    rdc_mont(tt1, c[0]);                   // c[0] = a0*b0 - a1*b1
}

__device__ static  void power2_setup(digit_t *x, int mark, const unsigned int nwords)
{ // Set up the value 2^mark.
    unsigned int i;

    for (i = 0; i < nwords; i++)
        x[i] = 0;

    i = 0;
    while (mark >= 0)
    {
        if (mark < RADIX)
        {
            x[i] = (digit_t)1 << mark;
        }
        mark -= RADIX;
        i += 1;
    }
}

__device__ static unsigned int is_felm_zero(const felm_t x)
{   // Is x = 0? return 1 (TRUE) if condition is true, 0 (FALSE) otherwise.
    // SECURITY NOTE: This function does not run in constant-time.
    unsigned int i;

    for (i = 0; i < NWORDS_FIELD; i++)
    {
        if (x[i] != 0)
            return false;
    }
    return true;
}

__device__ static unsigned int is_felm_even(const felm_t x)
{ // Is x even? return 1 (TRUE) if condition is true, 0 (FALSE) otherwise.
    return (unsigned int)((x[0] & 1) ^ 1);
}

__device__ static unsigned int is_felm_lt(const felm_t x, const felm_t y)
{   // Is x < y? return 1 (TRUE) if condition is true, 0 (FALSE) otherwise.
    // SECURITY NOTE: This function does not run in constant-time.
    int i;

    for (i = NWORDS_FIELD - 1; i >= 0; i--)
    {
        if (x[i] < y[i])
        {
            return true;
        }
        else if (x[i] > y[i])
        {
            return false;
        }
    }
    return false;
}

__device__ static void fpinv_mont_bingcd_partial(const felm_t a, felm_t x1, unsigned int *k)
{ // Partial Montgomery inversion via the binary GCD algorithm.
    felm_t u, v, x2;
    unsigned int cwords; // Number of words necessary for x1, x2

    fpcopy(a, u);
    fpcopy((digit_t *)PRIME, v);
    fpzero(x1);
    x1[0] = 1;
    fpzero(x2);
    *k = 0;

    while (!is_felm_zero(v))
    {
        cwords = ((*k + 1) / RADIX) + 1;
        if ((cwords < NWORDS_FIELD))
        {
            if (is_felm_even(v))
            {
                mp_shiftr1(v, NWORDS_FIELD);
                mp_shiftl1(x1, cwords);
            }
            else if (is_felm_even(u))
            {
                mp_shiftr1(u, NWORDS_FIELD);
                mp_shiftl1(x2, cwords);
            }
            else if (!is_felm_lt(v, u))
            {
                mp_sub(v, u, v, NWORDS_FIELD);
                mp_shiftr1(v, NWORDS_FIELD);
                mp_add(x1, x2, x2, cwords);
                mp_shiftl1(x1, cwords);
            }
            else
            {
                mp_sub(u, v, u, NWORDS_FIELD);
                mp_shiftr1(u, NWORDS_FIELD);
                mp_add(x1, x2, x1, cwords);
                mp_shiftl1(x2, cwords);
            }
        }
        else
        {
            if (is_felm_even(v))
            {
                mp_shiftr1(v, NWORDS_FIELD);
                mp_shiftl1(x1, NWORDS_FIELD);
            }
            else if (is_felm_even(u))
            {
                mp_shiftr1(u, NWORDS_FIELD);
                mp_shiftl1(x2, NWORDS_FIELD);
            }
            else if (!is_felm_lt(v, u))
            {
                mp_sub(v, u, v, NWORDS_FIELD);
                mp_shiftr1(v, NWORDS_FIELD);
                mp_add(x1, x2, x2, NWORDS_FIELD);
                mp_shiftl1(x1, NWORDS_FIELD);
            }
            else
            {
                mp_sub(u, v, u, NWORDS_FIELD);
                mp_shiftr1(u, NWORDS_FIELD);
                mp_add(x1, x2, x1, NWORDS_FIELD);
                mp_shiftl1(x2, NWORDS_FIELD);
            }
        }
        *k += 1;
    }

    if (is_felm_lt((digit_t *)PRIME, x1))
    {
        mp_sub(x1, (digit_t *)PRIME, x1, NWORDS_FIELD);
    }
}

__device__ void fpinv_mont(felm_t a)
{   // Field inversion via the binary GCD using Montgomery arithmetic, a = a^-1*R mod p.
    // SECURITY NOTE: This function does not run in constant-time and is therefore only suitable for
    //                operations not involving any secret data.
    felm_t x, t;
    unsigned int k;

    fpinv_mont_bingcd_partial(a, x, &k);
    if (k <= MAXBITS_FIELD)
    {
        fpmul_mont(x, (digit_t *)&Montgomery_R2, x);
        k += MAXBITS_FIELD;
    }
    fpmul_mont(x, (digit_t *)&Montgomery_R2, x);
    power2_setup(t, 2 * MAXBITS_FIELD - k, NWORDS_FIELD);
    fpmul_mont(x, t, a);
}

__device__ void fp2inv_mont(f2elm_t a)
{ // GF(p^2) inversion using Montgomery arithmetic, a = (a0-i*a1)/(a0^2+a1^2).
    f2elm_t t1;

    fpsqr_mont(a[0], t1[0]);    // t10 = a0^2
    fpsqr_mont(a[1], t1[1]);    // t11 = a1^2
    fpadd(t1[0], t1[1], t1[0]); // t10 = a0^2+a1^2
    fpinv_mont(t1[0]);          // t10 = (a0^2+a1^2)^-1
    fpneg(a[1]);                // a = a0-i*a1
    fpmul_mont(a[0], t1[0], a[0]);
    fpmul_mont(a[1], t1[0], a[1]); // a = (a0-i*a1)*(a0^2+a1^2)^-1
}

__device__ bool fp2_is_equal(const f2elm_t a, const f2elm_t b)
{   // Is a = b? return 1 (TRUE) if condition is true, 0 (FALSE) otherwise.
    // SECURITY NOTE: This function does not run in constant-time.
    f2elm_t t0;

    fp2sub(a, b, t0);
    fp2correction(t0);

    if (is_felm_zero(t0[0]) == false || is_felm_zero(t0[1]) == false)
        return false;

    return true;
}

//ec_isogeny.c

__device__ void xDBL(const point_proj_t P, point_proj_t Q, const f2elm_t A24plus, const f2elm_t C24)
{ // Doubling of a Montgomery point in projective coordinates (X:Z).
  // Input: projective Montgomery x-coordinates P = (X1:Z1), where x1=X1/Z1 and Montgomery curve constants A+2C and 4C.
  // Output: projective Montgomery x-coordinates Q = 2*P = (X2:Z2).
    f2elm_t t0, t1;
    
    fp2sub(P->X, P->Z, t0);                         // t0 = X1-Z1
    fp2add(P->X, P->Z, t1);                         // t1 = X1+Z1
    fp2sqr_mont(t0, t0);                            // t0 = (X1-Z1)^2 
    fp2sqr_mont(t1, t1);                            // t1 = (X1+Z1)^2 
    fp2mul_mont(C24, t0, Q->Z);                     // Z2 = C24*(X1-Z1)^2   
    fp2mul_mont(t1, Q->Z, Q->X);                    // X2 = C24*(X1-Z1)^2*(X1+Z1)^2
    fp2sub(t1, t0, t1);                             // t1 = (X1+Z1)^2-(X1-Z1)^2 
    fp2mul_mont(A24plus, t1, t0);                   // t0 = A24plus*[(X1+Z1)^2-(X1-Z1)^2]
    fp2add(Q->Z, t0, Q->Z);                         // Z2 = A24plus*[(X1+Z1)^2-(X1-Z1)^2] + C24*(X1-Z1)^2
    fp2mul_mont(Q->Z, t1, Q->Z);                    // Z2 = [A24plus*[(X1+Z1)^2-(X1-Z1)^2] + C24*(X1-Z1)^2]*[(X1+Z1)^2-(X1-Z1)^2]
}


__device__ void xDBLe(const point_proj_t P, point_proj_t Q, const f2elm_t A24plus, const f2elm_t C24, const int e)
{ // Computes [2^e](X:Z) on Montgomery curve with projective constant via e repeated doublings.
  // Input: projective Montgomery x-coordinates P = (XP:ZP), such that xP=XP/ZP and Montgomery curve constants A+2C and 4C.
  // Output: projective Montgomery x-coordinates Q <- (2^e)*P.
    int i;
    
    copy_words((digit_t*)P, (digit_t*)Q, 2*2*NWORDS_FIELD);

    for (i = 0; i < e; i++) {
        xDBL(Q, Q, A24plus, C24);
    }
}

__device__ void get_4_isog(const point_proj_t P, f2elm_t A24plus, f2elm_t C24, f2elm_t* coeff)
{ // Computes the corresponding 4-isogeny of a projective Montgomery point (X4:Z4) of order 4.
  // Input:  projective point of order four P = (X4:Z4).
  // Output: the 4-isogenous Montgomery curve with projective coefficients A+2C/4C and the 3 coefficients 
  //         that are used to evaluate the isogeny at a point in eval_4_isog().
    
    fp2sub(P->X, P->Z, coeff[1]);                   // coeff[1] = X4-Z4
    fp2add(P->X, P->Z, coeff[2]);                   // coeff[2] = X4+Z4
    fp2sqr_mont(P->Z, coeff[0]);                    // coeff[0] = Z4^2
    fp2add(coeff[0], coeff[0], coeff[0]);           // coeff[0] = 2*Z4^2
    fp2sqr_mont(coeff[0], C24);                     // C24 = 4*Z4^4
    fp2add(coeff[0], coeff[0], coeff[0]);           // coeff[0] = 4*Z4^2
    fp2sqr_mont(P->X, A24plus);                     // A24plus = X4^2
    fp2add(A24plus, A24plus, A24plus);              // A24plus = 2*X4^2
    fp2sqr_mont(A24plus, A24plus);                  // A24plus = 4*X4^4
}

__device__ void eval_4_isog(point_proj_t P, f2elm_t* coeff)
{ // Evaluates the isogeny at the point (X:Z) in the domain of the isogeny, given a 4-isogeny phi defined 
  // by the 3 coefficients in coeff (computed in the function get_4_isog()).
  // Inputs: the coefficients defining the isogeny, and the projective point P = (X:Z).
  // Output: the projective point P = phi(P) = (X:Z) in the codomain. 
    f2elm_t t0, t1;
    
    fp2add(P->X, P->Z, t0);                         // t0 = X+Z
    fp2sub(P->X, P->Z, t1);                         // t1 = X-Z
    fp2mul_mont(t0, coeff[1], P->X);                // X = (X+Z)*coeff[1]
    fp2mul_mont(t1, coeff[2], P->Z);                // Z = (X-Z)*coeff[2]
    fp2mul_mont(t0, t1, t0);                        // t0 = (X+Z)*(X-Z)
    fp2mul_mont(t0, coeff[0], t0);                  // t0 = coeff[0]*(X+Z)*(X-Z)
    fp2add(P->X, P->Z, t1);                         // t1 = (X-Z)*coeff[2] + (X+Z)*coeff[1]
    fp2sub(P->X, P->Z, P->Z);                       // Z = (X-Z)*coeff[2] - (X+Z)*coeff[1]
    fp2sqr_mont(t1, t1);                            // t1 = [(X-Z)*coeff[2] + (X+Z)*coeff[1]]^2
    fp2sqr_mont(P->Z, P->Z);                        // Z = [(X-Z)*coeff[2] - (X+Z)*coeff[1]]^2
    fp2add(t1, t0, P->X);                           // X = coeff[0]*(X+Z)*(X-Z) + [(X-Z)*coeff[2] + (X+Z)*coeff[1]]^2
    fp2sub(P->Z, t0, t0);                           // t0 = [(X-Z)*coeff[2] - (X+Z)*coeff[1]]^2 - coeff[0]*(X+Z)*(X-Z)
    fp2mul_mont(P->X, t1, P->X);                    // Xfinal
    fp2mul_mont(P->Z, t0, P->Z);                    // Zfinal
}

__device__ void xTPL(const point_proj_t P, point_proj_t Q, const f2elm_t A24minus, const f2elm_t A24plus)              
{ // Tripling of a Montgomery point in projective coordinates (X:Z).
  // Input: projective Montgomery x-coordinates P = (X:Z), where x=X/Z and Montgomery curve constants A24plus = A+2C and A24minus = A-2C.
  // Output: projective Montgomery x-coordinates Q = 3*P = (X3:Z3).
    f2elm_t t0, t1, t2, t3, t4, t5, t6;
                                    
    fp2sub(P->X, P->Z, t0);                         // t0 = X-Z 
    fp2sqr_mont(t0, t2);                            // t2 = (X-Z)^2           
    fp2add(P->X, P->Z, t1);                         // t1 = X+Z 
    fp2sqr_mont(t1, t3);                            // t3 = (X+Z)^2
    fp2add(t0, t1, t4);                             // t4 = 2*X
    fp2sub(t1, t0, t0);                             // t0 = 2*Z 
    fp2sqr_mont(t4, t1);                            // t1 = 4*X^2
    fp2sub(t1, t3, t1);                             // t1 = 4*X^2 - (X+Z)^2 
    fp2sub(t1, t2, t1);                             // t1 = 4*X^2 - (X+Z)^2 - (X-Z)^2
    fp2mul_mont(t3, A24plus, t5);                   // t5 = A24plus*(X+Z)^2 
    fp2mul_mont(t3, t5, t3);                        // t3 = A24plus*(X+Z)^3
    fp2mul_mont(A24minus, t2, t6);                  // t6 = A24minus*(X-Z)^2
    fp2mul_mont(t2, t6, t2);                        // t2 = A24minus*(X-Z)^3
    fp2sub(t2, t3, t3);                             // t3 = A24minus*(X-Z)^3 - coeff*(X+Z)^3
    fp2sub(t5, t6, t2);                             // t2 = A24plus*(X+Z)^2 - A24minus*(X-Z)^2
    fp2mul_mont(t1, t2, t1);                        // t1 = [4*X^2 - (X+Z)^2 - (X-Z)^2]*[A24plus*(X+Z)^2 - A24minus*(X-Z)^2]
    fp2add(t3, t1, t2);                             // t2 = [4*X^2 - (X+Z)^2 - (X-Z)^2]*[A24plus*(X+Z)^2 - A24minus*(X-Z)^2] + A24minus*(X-Z)^3 - coeff*(X+Z)^3
    fp2sqr_mont(t2, t2);                            // t2 = t2^2
    fp2mul_mont(t4, t2, Q->X);                      // X3 = 2*X*t2
    fp2sub(t3, t1, t1);                             // t1 = A24minus*(X-Z)^3 - A24plus*(X+Z)^3 - [4*X^2 - (X+Z)^2 - (X-Z)^2]*[A24plus*(X+Z)^2 - A24minus*(X-Z)^2]
    fp2sqr_mont(t1, t1);                            // t1 = t1^2
    fp2mul_mont(t0, t1, Q->Z);                      // Z3 = 2*Z*t1
}

__device__ void xTPLe(const point_proj_t P, point_proj_t Q, const f2elm_t A24minus, const f2elm_t A24plus, const int e)
{ // Computes [3^e](X:Z) on Montgomery curve with projective constant via e repeated triplings.
  // Input: projective Montgomery x-coordinates P = (XP:ZP), such that xP=XP/ZP and Montgomery curve constants A24plus = A+2C and A24minus = A-2C.
  // Output: projective Montgomery x-coordinates Q <- (3^e)*P.
    int i;
        
    copy_words((digit_t*)P, (digit_t*)Q, 2*2*NWORDS_FIELD);

    for (i = 0; i < e; i++) {
        xTPL(Q, Q, A24minus, A24plus);
    }
}

__device__ void get_3_isog(const point_proj_t P, f2elm_t A24minus, f2elm_t A24plus, f2elm_t* coeff)
{ // Computes the corresponding 3-isogeny of a projective Montgomery point (X3:Z3) of order 3.
  // Input:  projective point of order three P = (X3:Z3).
  // Output: the 3-isogenous Montgomery curve with projective coefficient A/C. 
    f2elm_t t0, t1, t2, t3, t4;
    
    fp2sub(P->X, P->Z, coeff[0]);                   // coeff0 = X-Z
    fp2sqr_mont(coeff[0], t0);                      // t0 = (X-Z)^2
    fp2add(P->X, P->Z, coeff[1]);                   // coeff1 = X+Z
    fp2sqr_mont(coeff[1], t1);                      // t1 = (X+Z)^2
    fp2add(t0, t1, t2);                             // t2 = (X+Z)^2 + (X-Z)^2
    fp2add(coeff[0], coeff[1], t3);                 // t3 = 2*X
    fp2sqr_mont(t3, t3);                            // t3 = 4*X^2
    fp2sub(t3, t2, t3);                             // t3 = 4*X^2 - (X+Z)^2 - (X-Z)^2 
    fp2add(t1, t3, t2);                             // t2 = 4*X^2 - (X-Z)^2 
    fp2add(t3, t0, t3);                             // t3 = 4*X^2 - (X+Z)^2
    fp2add(t0, t3, t4);                             // t4 = 4*X^2 - (X+Z)^2 + (X-Z)^2 
    fp2add(t4, t4, t4);                             // t4 = 2(4*X^2 - (X+Z)^2 + (X-Z)^2) 
    fp2add(t1, t4, t4);                             // t4 = 8*X^2 - (X+Z)^2 + 2*(X-Z)^2
    fp2mul_mont(t2, t4, A24minus);                  // A24minus = [4*X^2 - (X-Z)^2]*[8*X^2 - (X+Z)^2 + 2*(X-Z)^2]
    fp2add(t1, t2, t4);                             // t4 = 4*X^2 + (X+Z)^2 - (X-Z)^2
    fp2add(t4, t4, t4);                             // t4 = 2(4*X^2 + (X+Z)^2 - (X-Z)^2) 
    fp2add(t0, t4, t4);                             // t4 = 8*X^2 + 2*(X+Z)^2 - (X-Z)^2
    fp2mul_mont(t3, t4, A24plus);                   // A24plus = [4*X^2 - (X+Z)^2]*[8*X^2 + 2*(X+Z)^2 - (X-Z)^2]
}

__device__ void eval_3_isog(point_proj_t Q, const f2elm_t* coeff)
{ // Computes the 3-isogeny R=phi(X:Z), given projective point (X3:Z3) of order 3 on a Montgomery curve and 
  // a point P with 2 coefficients in coeff (computed in the function get_3_isog()).
  // Inputs: projective points P = (X3:Z3) and Q = (X:Z).
  // Output: the projective point Q <- phi(Q) = (X3:Z3). 
    f2elm_t t0, t1, t2;

    fp2add(Q->X, Q->Z, t0);                       // t0 = X+Z
    fp2sub(Q->X, Q->Z, t1);                       // t1 = X-Z
    fp2mul_mont(t0, coeff[0], t0);                // t0 = coeff0*(X+Z)
    fp2mul_mont(t1, coeff[1], t1);                // t1 = coeff1*(X-Z)
    fp2add(t0, t1, t2);                           // t2 = coeff0*(X+Z) + coeff1*(X-Z)
    fp2sub(t1, t0, t0);                           // t0 = coeff1*(X-Z) - coeff0*(X+Z)
    fp2sqr_mont(t2, t2);                          // t2 = [coeff0*(X+Z) + coeff1*(X-Z)]^2
    fp2sqr_mont(t0, t0);                          // t0 = [coeff1*(X-Z) - coeff0*(X+Z)]^2
    fp2mul_mont(Q->X, t2, Q->X);                  // X3final = X*[coeff0*(X+Z) + coeff1*(X-Z)]^2        
    fp2mul_mont(Q->Z, t0, Q->Z);                  // Z3final = Z*[coeff1*(X-Z) - coeff0*(X+Z)]^2
}

__device__ void inv_3_way(f2elm_t z1, f2elm_t z2, f2elm_t z3)
{ // 3-way simultaneous inversion
  // Input:  z1,z2,z3
  // Output: 1/z1,1/z2,1/z3 (override inputs).
    f2elm_t t0, t1, t2, t3;

    fp2mul_mont(z1, z2, t0);                      // t0 = z1*z2
    fp2mul_mont(z3, t0, t1);                      // t1 = z1*z2*z3
    fp2inv_mont(t1);                              // t1 = 1/(z1*z2*z3)
    fp2mul_mont(z3, t1, t2);                      // t2 = 1/(z1*z2) 
    fp2mul_mont(t2, z2, t3);                      // t3 = 1/z1
    fp2mul_mont(t2, z1, z2);                      // z2 = 1/z2
    fp2mul_mont(t0, t1, z3);                      // z3 = 1/z3
    fp2copy(t3, z1);                              // z1 = 1/z1
}

__device__ void get_A(const f2elm_t xP, const f2elm_t xQ, const f2elm_t xR, f2elm_t A)
{ // Given the x-coordinates of P, Q, and R, returns the value A corresponding to the Montgomery curve E_A: y^2=x^3+A*x^2+x such that R=Q-P on E_A.
  // Input:  the x-coordinates xP, xQ, and xR of the points P, Q and R.
  // Output: the coefficient A corresponding to the curve E_A: y^2=x^3+A*x^2+x.
    f2elm_t t0, t1, one = {0};
    
    fpcopy((digit_t*)&Montgomery_one, one[0]);
    fp2add(xP, xQ, t1);                           // t1 = xP+xQ
    fp2mul_mont(xP, xQ, t0);                      // t0 = xP*xQ
    fp2mul_mont(xR, t1, A);                       // A = xR*t1
    fp2add(t0, A, A);                             // A = A+t0
    fp2mul_mont(t0, xR, t0);                      // t0 = t0*xR
    fp2sub(A, one, A);                            // A = A-1
    fp2add(t0, t0, t0);                           // t0 = t0+t0
    fp2add(t1, xR, t1);                           // t1 = t1+xR
    fp2add(t0, t0, t0);                           // t0 = t0+t0
    fp2sqr_mont(A, A);                            // A = A^2
    fp2inv_mont(t0);                              // t0 = 1/t0
    fp2mul_mont(A, t0, A);                        // A = A*t0
    fp2sub(A, t1, A);                             // Afinal = A-t1
}

__device__ void j_inv(const f2elm_t A, const f2elm_t C, f2elm_t jinv)
{ // Computes the j-invariant of a Montgomery curve with projective constant.
  // Input: A,C in GF(p^2).
  // Output: j=256*(A^2-3*C^2)^3/(C^4*(A^2-4*C^2)), which is the j-invariant of the Montgomery curve B*y^2=x^3+(A/C)*x^2+x or (equivalently) j-invariant of B'*y^2=C*x^3+A*x^2+C*x.
    f2elm_t t0, t1;
    
    fp2sqr_mont(A, jinv);                           // jinv = A^2        
    fp2sqr_mont(C, t1);                             // t1 = C^2
    fp2add(t1, t1, t0);                             // t0 = t1+t1
    fp2sub(jinv, t0, t0);                           // t0 = jinv-t0
    fp2sub(t0, t1, t0);                             // t0 = t0-t1
    fp2sub(t0, t1, jinv);                           // jinv = t0-t1
    fp2sqr_mont(t1, t1);                            // t1 = t1^2
    fp2mul_mont(jinv, t1, jinv);                    // jinv = jinv*t1
    fp2add(t0, t0, t0);                             // t0 = t0+t0
    fp2add(t0, t0, t0);                             // t0 = t0+t0
    fp2sqr_mont(t0, t1);                            // t1 = t0^2
    fp2mul_mont(t0, t1, t0);                        // t0 = t0*t1
    fp2add(t0, t0, t0);                             // t0 = t0+t0
    fp2add(t0, t0, t0);                             // t0 = t0+t0
    fp2inv_mont(jinv);                              // jinv = 1/jinv 
    fp2mul_mont(jinv, t0, jinv);                    // jinv = t0*jinv
}

__device__ void xDBLADD(point_proj_t P, point_proj_t Q, const f2elm_t xPQ, const f2elm_t A24)
{ // Simultaneous doubling and differential addition.
  // Input: projective Montgomery points P=(XP:ZP) and Q=(XQ:ZQ) such that xP=XP/ZP and xQ=XQ/ZQ, affine difference xPQ=x(P-Q) and Montgomery curve constant A24=(A+2)/4.
  // Output: projective Montgomery points P <- 2*P = (X2P:Z2P) such that x(2P)=X2P/Z2P, and Q <- P+Q = (XQP:ZQP) such that = x(Q+P)=XQP/ZQP. 
    f2elm_t t0, t1, t2;

    fp2add(P->X, P->Z, t0);                         // t0 = XP+ZP
    fp2sub(P->X, P->Z, t1);                         // t1 = XP-ZP
    fp2sqr_mont(t0, P->X);                          // XP = (XP+ZP)^2
    fp2sub(Q->X, Q->Z, t2);                         // t2 = XQ-ZQ
    fp2correction(t2);
    fp2add(Q->X, Q->Z, Q->X);                       // XQ = XQ+ZQ
    fp2mul_mont(t0, t2, t0);                        // t0 = (XP+ZP)*(XQ-ZQ)
    fp2sqr_mont(t1, P->Z);                          // ZP = (XP-ZP)^2
    fp2mul_mont(t1, Q->X, t1);                      // t1 = (XP-ZP)*(XQ+ZQ)
    fp2sub(P->X, P->Z, t2);                         // t2 = (XP+ZP)^2-(XP-ZP)^2
    fp2mul_mont(P->X, P->Z, P->X);                  // XP = (XP+ZP)^2*(XP-ZP)^2
    fp2mul_mont(t2, A24, Q->X);                     // XQ = A24*[(XP+ZP)^2-(XP-ZP)^2]
    fp2sub(t0, t1, Q->Z);                           // ZQ = (XP+ZP)*(XQ-ZQ)-(XP-ZP)*(XQ+ZQ)
    fp2add(Q->X, P->Z, P->Z);                       // ZP = A24*[(XP+ZP)^2-(XP-ZP)^2]+(XP-ZP)^2
    fp2add(t0, t1, Q->X);                           // XQ = (XP+ZP)*(XQ-ZQ)+(XP-ZP)*(XQ+ZQ)
    fp2mul_mont(P->Z, t2, P->Z);                    // ZP = [A24*[(XP+ZP)^2-(XP-ZP)^2]+(XP-ZP)^2]*[(XP+ZP)^2-(XP-ZP)^2]
    fp2sqr_mont(Q->Z, Q->Z);                        // ZQ = [(XP+ZP)*(XQ-ZQ)-(XP-ZP)*(XQ+ZQ)]^2
    fp2sqr_mont(Q->X, Q->X);                        // XQ = [(XP+ZP)*(XQ-ZQ)+(XP-ZP)*(XQ+ZQ)]^2
    fp2mul_mont(Q->Z, xPQ, Q->Z);                   // ZQ = xPQ*[(XP+ZP)*(XQ-ZQ)-(XP-ZP)*(XQ+ZQ)]^2
}

__device__ static void swap_points(point_proj_t P, point_proj_t Q, const digit_t option)
{ // Swap points.
  // If option = 0 then P <- P and Q <- Q, else if option = 0xFF...FF then P <- Q and Q <- P
    digit_t temp;
    unsigned int i;

    for (i = 0; i < NWORDS_FIELD; i++) {
        temp = option & (P->X[0][i] ^ Q->X[0][i]);
        P->X[0][i] = temp ^ P->X[0][i]; 
        Q->X[0][i] = temp ^ Q->X[0][i]; 
        temp = option & (P->Z[0][i] ^ Q->Z[0][i]);
        P->Z[0][i] = temp ^ P->Z[0][i]; 
        Q->Z[0][i] = temp ^ Q->Z[0][i]; 
        temp = option & (P->X[1][i] ^ Q->X[1][i]);
        P->X[1][i] = temp ^ P->X[1][i]; 
        Q->X[1][i] = temp ^ Q->X[1][i]; 
        temp = option & (P->Z[1][i] ^ Q->Z[1][i]);
        P->Z[1][i] = temp ^ P->Z[1][i]; 
        Q->Z[1][i] = temp ^ Q->Z[1][i]; 
    }
}

__device__ static void LADDER3PT(const f2elm_t xP, const f2elm_t xQ, const f2elm_t xPQ, const digit_t* m, const unsigned int AliceOrBob, point_proj_t R, const f2elm_t A)
{
    point_proj_t R0 = {0}, R2 = {0};
    f2elm_t A24 = {0};
    digit_t mask;
    int i, nbits, bit, swap, prevbit = 0;

    if (AliceOrBob == ALICE) {
        nbits = OALICE_BITS;
    } else {
        nbits = OBOB_BITS - 1;
    }

    // Initializing constant
    fpcopy((digit_t*)&Montgomery_one, A24[0]);
    fp2add(A24, A24, A24);
    fp2add(A, A24, A24);
    fp2div2(A24, A24);  
    fp2div2(A24, A24);  // A24 = (A+2)/4

    // Initializing points
    fp2copy(xQ, R0->X);
    fpcopy((digit_t*)&Montgomery_one, (digit_t*)R0->Z);
    fp2copy(xPQ, R2->X);
    fpcopy((digit_t*)&Montgomery_one, (digit_t*)R2->Z);
    fp2copy(xP, R->X);
    fpcopy((digit_t*)&Montgomery_one, (digit_t*)R->Z);
    fpzero((digit_t*)(R->Z)[1]);

    // Main loop
    for (i = 0; i < nbits; i++) {
        bit = (m[i >> LOG2RADIX] >> (i & (RADIX-1))) & 1;
        swap = bit ^ prevbit;
        prevbit = bit;
        mask = 0 - (digit_t)swap;

        swap_points(R, R2, mask);
        xDBLADD(R0, R2, R->X, A24);
        fp2mul_mont(R2->X, R->Z, R2->X);
    }
    swap = 0 ^ prevbit;
    mask = 0 - (digit_t)swap;
    swap_points(R, R2, mask);
}

//sidh_vow_base.c

/* Initializations */
//allocates memory for a new st_t instance
/*
static st_t init_st(uint64_t nwords_state)
{
    st_t s;
    s.words = (digit_t*) calloc(nwords_state, sizeof(digit_t));
    return s;
}
*/

//frees allocated memory corresponding to st_t s
static void free_st(st_t *s)
{
    free(s->words);
}

//concatenates 2  arrays of length lenA and lenB into one array
__device__ __host__ static void ConcatArray(unsigned long *cat, unsigned long *A, unsigned long lenA, unsigned long *B, unsigned long lenB)
{
    unsigned int i;

    for (i = 1; i < lenA + 1; i++)
        cat[i] = A[i - 1];
    for (i = 0; i < lenB; i++)
        cat[lenA + i + 1] = B[i];
}

__device__ __host__ static unsigned long OptStrat(unsigned long *strat, const unsigned long n, const double p, const double q)
{
    unsigned int i, j, b, ctr = 2;
    /* Maximum size of strategy = 250 */
    double C[250], newCpqs[250], newCpq, tmpCpq;
    unsigned long S[250][250];
    unsigned long lens[250];

    lens[1] = 0;
    S[1][0] = 0;
    C[1] = 0;
    for (i = 2; i < n + 1; i++) {
        for (b = 1; b < i; b++)
            newCpqs[b] = C[i - b] + C[b] + b * p + (i - b) * q;
        newCpq = newCpqs[1];
        b = 1;
        for (j = 2; j < i; j++) {
            tmpCpq = newCpqs[j];
            if (newCpq > tmpCpq) {
                newCpq = tmpCpq;
                b = j;
            }
        }
        S[ctr][0] = b;
        ConcatArray(S[ctr], S[i - b], lens[i - b], S[b], lens[b]);
        C[ctr] = newCpqs[b];
        lens[ctr] = 1 + lens[i - b] + lens[b];
        ctr++;
    }

    for (i = 0; i < lens[ctr - 1]; i++)
        strat[i] = S[ctr - 1][i];

    return lens[ctr - 1];
}

__device__ static void xDBL_affine(const point_proj_t P, point_proj_t Q, const f2elm_t a24)
{
    f2elm_t t0, t1;

    fp2sub(P->X, P->Z, t0);      // t0 = X1-Z1
    fp2add(P->X, P->Z, t1);      // t1 = X1+Z1
    fp2sqr_mont(t0, t0);         // t0 = (X1-Z1)^2
    fp2sqr_mont(t1, t1);         // t1 = (X1+Z1)^2
    fp2mul_mont(t0, t1, Q->X);   // X2 = C24*(X1-Z1)^2*(X1+Z1)^2
    fp2sub(t1, t0, t1);          // t1 = (X1+Z1)^2-(X1-Z1)^2
    fp2mul_mont(a24, t1, Q->Z);  // t0 = A24plus*[(X1+Z1)^2-(X1-Z1)^2]
    fp2add(Q->Z, t0, Q->Z);      // Z2 = A24plus*[(X1+Z1)^2-(X1-Z1)^2] + C24*(X1-Z1)^2
    fp2mul_mont(Q->Z, t1, Q->Z); // Z2 = [A24plus*[(X1+Z1)^2-(X1-Z1)^2] + C24*(X1-Z1)^2]*[(X1+Z1)^2-(X1-Z1)^2]
}

__device__ static void xDBLe_affine(const point_proj_t P, point_proj_t Q, const f2elm_t a24, const int e)
{ // Computes [2^e](X:Z) on Montgomery curve with projective constant via e repeated doublings.
  // Input: projective Montgomery x-coordinates P = (XP:ZP), such that xP=XP/ZP and Montgomery curve constants A+2C and 4C.
  // Output: projective Montgomery x-coordinates Q <- (2^e)*P.

    copy_words((digit_t *)P, (digit_t *)Q, 2 * 2 * NWORDS_FIELD);

    for (int i = 0; i < e; i++)
        xDBL_affine(Q, Q, a24);
}

__device__ static void GetFourIsogenyWithKernelXeqZ(point_proj_t A24, const f2elm_t a24)
{
    fp2copy(a24, A24->X);
    fp2copy(a24, A24->Z);
    fpsub((digit_t *)A24->Z, (digit_t *)&Montgomery_one, (digit_t *)A24->Z);
}

__device__ static void EvalFourIsogenyWithKernelXeqZ(point_proj_t S, const f2elm_t a24)
{
    f2elm_t T0, T1, T2;

    fpzero(T1[1]); // Set RZ = 1
    fpcopy((digit_t *)&Montgomery_one, T1[0]);
    fp2add(S->X, S->Z, T0);
    fp2sub(S->X, S->Z, T2);
    fp2sqr_mont(T0, T0);
    fp2sqr_mont(T2, T2);
    fp2sub(T1, a24, T1);
    fp2add(T1, T1, T1);
    fp2add(T1, T1, T1);
    fp2mul_mont(T1, S->X, T1);
    fp2mul_mont(T1, S->Z, T1);
    fp2mul_mont(T1, T2, S->Z);
    fp2sub(T0, T1, T1);
    fp2mul_mont(T0, T1, S->X);
}

__device__ static void GetFourIsogenyWithKernelXeqMinZ(point_proj_t A24, const f2elm_t a24)
{
    fp2copy(a24, A24->X);
    fp2copy(a24, A24->Z);
    fpsub((digit_t *)A24->X, (digit_t *)&Montgomery_one, (digit_t *)A24->X);
}

__device__ static void EvalFourIsogenyWithKernelXeqMinZ(point_proj_t S, const f2elm_t a24)
{
    f2elm_t T0, T1, T2;

    fp2add(S->X, S->Z, T2);
    fp2sub(S->X, S->Z, T0);
    fp2sqr_mont(T2, T2);
    fp2sqr_mont(T0, T0);
    fp2add(a24, a24, T1);
    fp2add(T1, T1, T1);
    fp2mul_mont(T1, S->X, T1);
    fp2mul_mont(T1, S->Z, T1);
    fp2mul_mont(T1, T2, S->Z);
    fp2neg(S->Z);
    fp2add(T0, T1, T1);
    fp2mul_mont(T0, T1, S->X);
}

__device__ static void FourwayInv(f2elm_t X, f2elm_t Y, f2elm_t Z, f2elm_t T)
{
    f2elm_t Xt, Zt, XY, ZT, XYZT;

    fp2copy(X, Xt);
    fp2copy(Z, Zt);
    fp2mul_mont(X, Y, XY);
    fp2mul_mont(Z, T, ZT);
    fp2mul_mont(XY, ZT, XYZT);
    fp2inv_mont(XYZT);         /* 1 / XYZT */
    fp2mul_mont(ZT, XYZT, ZT); /* 1 / XY */
    fp2mul_mont(XY, XYZT, XY); /* 1 / ZT */
    fp2mul_mont(ZT, Y, X);
    fp2mul_mont(ZT, Xt, Y);
    fp2mul_mont(XY, T, Z);
    fp2mul_mont(XY, Zt, T);
}

//C is represented by first bit in s.bytes
__device__ __host__ static unsigned char GetC_SIDH(st_t *s)
{
    return (s->bytes[0] & 0x01);
}
//B is represented by second and third bit in s.bytes
__device__ __host__ static unsigned char GetB_SIDH(st_t *s)
{
    return ((s->bytes[0] & 0x06) >> 1);
}

//sets B to value t assuming B was 11 before 
__device__ __host__ static void SetB_SIDH(st_t *s, const unsigned char t)
{
    /* Assumes that b is set to 0x03 */
    s->bytes[0] &= ((t << 1) | 0xF9);
}
//copys one state to the other
__device__ __host__ static void copy_st(st_t *r, const st_t *s, const uint64_t nwords_state)
{
    for (unsigned int i = 0; i < nwords_state; i++)
        r->words[i] = s->words[i];
}

//copy state into an array of uint64's
__device__ __host__ static void copy_st2uint64(uint64_t *r, const st_t *s, const uint64_t nwords_state)
{

    for (unsigned int i = 0; i < nwords_state; i++)
        r[i] = s->words[i];
}

//copy array of uitt64'2 into state
__device__ __host__ static void copy_uint642st(st_t *r, const uint64_t *s, const uint64_t nwords_state)
{
    for (unsigned int i = 0; i < nwords_state; i++)
        r->words[i] = s[i];
}

//swap values of two states by classical swap
__device__ __host__ static void SwapStSIDH(st_t *r, st_t *s, uint64_t nwords_state)
{   /*
    st_t t = init_st(nwords_state);

    copy_st(&t, r, nwords_state);
    copy_st(r, s, nwords_state);
    copy_st(s, &t, nwords_state);
    free_st(&t);
    */
}

//compare if two states are equal
__device__ __host__ bool is_equal_st(const st_t *s, const st_t *t, const uint64_t nwords_state)
{
    for (unsigned int i = 0; i < nwords_state; i++)
    {
        if (s->words[i] != t->words[i])
            return false;
    }
    return true;
}

//compare of state equal to uint_64 array r
__device__ __host__ static bool is_equal_st_words(const st_t *s, const uint64_t *r, const uint64_t nwords_state)
{
    for (unsigned int i = 0; i < nwords_state; i++)
    {
        if (s->words[i] != r[i])
            return false;
    }
    return true;
}

//check if two jinvariants are equal
__device__ __host__ bool IsEqualJinvSIDH(unsigned char j0[FP2_ENCODED_BYTES], unsigned char j1[FP2_ENCODED_BYTES])
{
    for (unsigned int i = 0; i < FP2_ENCODED_BYTES; i++)
    {
        if (j0[i] != j1[i])
            return false;
    }
    return true;
}

//copies one triple to the other
__device__ __host__ void copy_trip(trip_t *s, const trip_t *t, const uint64_t nwords_state)
{
    copy_st(&s->current_state, &t->current_state, nwords_state);
    s->current_steps = t->current_steps;
    copy_st(&s->initial_state, &t->initial_state, nwords_state);
}









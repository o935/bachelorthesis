int main2() {

    unsigned long NBITS_K = 15;
    
    double ALPHA = 2.25;
    double BETA = 10;
    //double gamma = 20;
    
    f2elm_t jinv = {0x87D581FB16D011BD, 0x3, 0x5D14A016E29F3C, 0x1};
    CurveAndPointsSIDH E[2] = 
    {{
        .a24 = {0x898F502EC74EFE54, 0x3, 0x649BFBDDF471F7D0, 0x4},
        .xp = {0x81B68BDCA5EEE933, 0x1, 0x9BC03274CCE5407C, 0xE},
        .xq = {0x82D38AA521A411ED, 0x7, 0x8ECFD9A4B8E874AE, 0x6},
        .xpq = {0xA7DEE573517AA282, 0x5, 0x6AAA88DC2F3041ED, 0xA}
    },
    {
        .a24 = {0x5219884293D7ACA9, 0x3, 0xCCB60983643DBA16, 0xF},
        .xp = {0x34C2A2B46B60718A, 0xF, 0x7066C62B26701E69, 0xE},
        .xq = {0x13E5C49616025EB1, 0x11, 0xD4D573F7A00911FA, 0x4},
        .xpq = {0x3019C8E745061408, 0xE, 0x349BC9BDE78E1278, 0x7}
    }};

    unsigned long strat[250];
    unsigned long RAD = 64; //assuming 64bit architecture
    data_t* data = (data_t*) calloc(1, sizeof(data_t));
    data->function_version = 0;
    data->NBITS_STATE = NBITS_K + 3; /*TODO: is this correct?*/
    data->NBYTES_STATE = ((data->NBITS_STATE + 7) / 8);
    data->NWORDS_STATE = ((data->NBITS_STATE + RAD - 1) / RAD);
    data->NBITS_OVERFLOW = data->NBITS_STATE % 8;
    data->MEMORY_LOG_SIZE = 9;
    data->MEMORY_SIZE = (uint64_t)(1 << data->MEMORY_LOG_SIZE);
    data->MEMORY_SIZE_MASK = data->MEMORY_SIZE - 1;

    double THETA = ALPHA * sqrt((double) data->MEMORY_SIZE / (double)(3 * (1 << (NBITS_K + 1))));

    data->MAX_STEPS = ceil(1/THETA);
    data->MAX_DIST = (unsigned long)(BETA * data->MEMORY_SIZE);
    data->MAX_FUNCTION_VERSIONS = 10000;
    data->DIST_BOUND = THETA * (1 << (data->NBITS_STATE - 3 - data->MEMORY_LOG_SIZE));
    memcpy(data->E, E, 2*sizeof(CurveAndPointsSIDH));
    data->lenstrat = OptStrat(strat, (unsigned long)(NBITS_K + 1) / 2, 1, 1);
    //printf("%lu", data->lenstrat);
    data->strat = (unsigned long*) calloc(data->lenstrat, sizeof(unsigned long));
    memcpy(data->jinv, jinv, sizeof(f2elm_t));
    data->random_functions = 0;

    int B = 1;
    int T = 10;
    int THREADS = B*T;

    srand(time(NULL));

    unsigned long function_version = 1;
    unsigned long seed = rand();

    curandState* d_states;
    if (cudaSuccess != cudaMalloc(&d_states, THREADS*sizeof(curandState))) {
        printf("Error d_states malloc: %d\n", cudaGetLastError());
    }

    DIST_POINT* h_distinguished_points;
    DIST_POINT* d_distinguished_points;

    unsigned long long* h_countlist;
    h_countlist = (unsigned long long*) calloc(THREADS, sizeof(unsigned long long));
    unsigned long long* d_countlist;
    if (cudaSuccess != cudaMalloc(&d_countlist, THREADS*sizeof(unsigned long long))) {
        printf("Error d_countlist malloc: %d\n", cudaGetLastError());
    }

    h_distinguished_points       = (DIST_POINT*)     calloc(THREADS, sizeof(DIST_POINT));
    if (cudaSuccess != cudaMalloc(&d_distinguished_points, THREADS*sizeof(DIST_POINT))) {
        printf("Error d_dist_points malloc: %d\n", cudaGetLastError());
    }

    data_t* d_data;

    if (cudaSuccess != cudaMalloc(&d_data, sizeof(data_t))) {
        printf("Error d_dist_points malloc: %d\n", cudaGetLastError());
    }

    cudaMemcpy(d_data, data, sizeof(data_t), cudaMemcpyHostToDevice);

    setupStateKernel<<<B,T>>>(d_states, seed, function_version);



    size_t shared_size = T*sizeof(DIST_POINT);

    findDistPoints<<<B,T, shared_size>>>(d_states, d_distinguished_points, d_countlist, d_data);

    cudaMemcpy(h_distinguished_points, d_distinguished_points, THREADS*sizeof(DIST_POINT), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_countlist, d_countlist, THREADS*sizeof(unsigned long long), cudaMemcpyDeviceToHost);

    unsigned long long count = 0;

    for (int i = 0; i < THREADS; i++) {
        count += h_countlist[i];
        printf("Steps: %d : \n", h_distinguished_points[i].steps);
        //printf("Steps: %d\n", h_distinguished_points[i].start.bytes[1]);
    }

    printf("Count: %llu\n", count);

    printf("sizeof:%lu,\n", sizeof(st_t));

    free(h_distinguished_points);
    cudaFree(d_states);
    cudaFree(d_distinguished_points);
    cudaFree(d_data);
    free(data);

    return 0;
}


#include "util.cu"

/* Functions for vOW */


//does this function determine if a point is distinguished??????
__device__ bool DistinguishedSIDH(DIST_POINT point, data_t *data)
{
    /* Divide distinguishedness over interval to avoid bad cases */
    uint64_t val;
    val = point.end.words[0] >> (data->MEMORY_LOG_SIZE + EXTRA_MEM_LOG_SIZE);
    val += (uint64_t)data->function_version * (uint64_t)data->DIST_BOUND;
    val &= (((uint64_t)1 << (data->NBITS_STATE - EXTRA_MEM_LOG_SIZE - data->MEMORY_LOG_SIZE)) - 1);

    return (val <= (uint64_t)data->DIST_BOUND);
}

//function which calculates the location where to store a triple?
uint64_t MemIndexSIDH(DIST_POINT point, data_t* data)
{
    return (uint64_t)(((point.end.words[0] >> EXTRA_MEM_LOG_SIZE) + data->random_functions) & data->MEMORY_SIZE_MASK);
}

__device__ static unsigned int GetMSBSIDH(const unsigned char *m, unsigned long nbits_state)
{
    int msb = nbits_state;
    int bit = (m[(msb - 1) >> 3] >> ((msb - 1) & 0x07)) & 1;

    while ((bit == 0) && (msb > 0))
    {
        msb--;
        bit = (m[(msb - 1) >> 3] >> ((msb - 1) & 0x07)) & 1;
    }

    return msb;
}

__device__ void UpdateRandomFunctionSIDH(data_t* data) {

    data->function_version++;
}

__device__ static void LadderThreePtSIDH(point_proj_t R, const f2elm_t a24, const f2elm_t xp, const f2elm_t xq, const f2elm_t xpq, const unsigned char *m, unsigned char b, unsigned long nbits_state)
{ // Non-constant time version that depends on size of k
    point_proj_t R0 = {0}, R2 = {0};
    unsigned int i, bit, msb = nbits_state;

    fp2copy(xpq, R2->X);
    fpcopy((digit_t *)&Montgomery_one, (digit_t *)R0->Z);
    fpzero((digit_t *)(R0->Z)[1]);
    fpcopy((digit_t *)&Montgomery_one, (digit_t *)R->Z);
    fpzero((digit_t *)(R->Z)[1]);
    fpcopy((digit_t *)&Montgomery_one, (digit_t *)R2->Z);
    fpzero((digit_t *)(R2->Z)[1]);

    if (b == 0x02) { // LSB = 0 first
        fp2copy(xp, R0->X);
        fp2copy(xq, R->X);

        xDBLADD_SIDH(R0, R2, R->X, a24);
        fp2mul_mont(R2->X, R->Z, R2->X);
    } else { // Do scalarmult other way around
        fp2copy(xq, R0->X);
        fp2copy(xp, R->X);
    }

    if (b != 0x01) /* Can skip top zeroes of k */
        msb = GetMSBSIDH(m, nbits_state);

    for (i = 3; i < msb; i++) { // Ignore 3 lsb's c,b
        bit = (m[i >> 3] >> (i & 0x07)) & 1;

        if (bit) {
            xDBLADD_SIDH(R0, R, R2->X, a24);
            fp2mul_mont(R->X, R2->Z, R->X);
        } else {
            xDBLADD_SIDH(R0, R2, R->X, a24);
            fp2mul_mont(R2->X, R->Z, R2->X);
        }
    }

    if (b == 0x01) { // MSB b = 1
        xDBLADD_SIDH(R0, R, R2->X, a24);
        fp2mul_mont(R->X, R2->Z, R->X);
    }
}

__device__ static void GetIsogeny(f2elm_t jinv, const f2elm_t a24, const f2elm_t xp, const f2elm_t xq, const f2elm_t xpq, const unsigned char *k, const unsigned char b, const unsigned long *strat, const unsigned long lenstrat, unsigned long nbits_state)
{
    point_proj_t R, A24, pts[MAX_INT_POINTS_ALICE];
    f2elm_t coeff[3];
    unsigned long i, row, index = 0, ii = 0, m, npts = 0, pts_index[MAX_INT_POINTS_ALICE];

    printf("%lu\n", R->X);

    printf("a");

    /* Retrieve kernel point */
    LadderThreePtSIDH(R, a24, xp, xq, xpq, k, b, nbits_state);

    printf("b");

    /* Treat the first row separately */
    while (index < lenstrat)
    {
        printf("b2"); 
        fp2copy(R->X, pts[npts]->X);
        fp2copy(R->Z, pts[npts]->Z);
        pts_index[npts++] = index;
        m = strat[ii++];
        xDBLe_affine(R, R, a24, (int)(2 * m));
        index += m;
    }

    printf("c");

    if (fp2_is_equal(R->X, R->Z)) {
        for (i = 0; i < npts; i++) {
            EvalFourIsogenyWithKernelXeqZ(pts[i], a24);
        }
        GetFourIsogenyWithKernelXeqZ(A24, a24);
    } else {
        fp2copy(R->Z, A24->Z);
        fp2neg(A24->Z);
        if (fp2_is_equal(R->X, A24->Z)) {
            for (i = 0; i < npts; i++) {
                EvalFourIsogenyWithKernelXeqMinZ(pts[i], a24);
            }
            GetFourIsogenyWithKernelXeqMinZ(A24, a24);
        } else {
            GetFourIsogenyWithKernelXneZ(R, A24->X, A24->Z, coeff);
            for (i = 0; i < npts; i++) {
                EvalFourIsogenyWithKernelXneZ(pts[i], coeff);
            }
        }
    }

    printf("d");

    fp2copy(pts[npts - 1]->X, R->X);
    fp2copy(pts[npts - 1]->Z, R->Z);
    index = pts_index[npts - 1];
    npts -= 1;

    /* All steps except the first */
    for (row = 2; row < lenstrat + 1; row++) {
        while (index < lenstrat + 1 - row)
        {
            fp2copy(R->X, pts[npts]->X);
            fp2copy(R->Z, pts[npts]->Z);
            pts_index[npts++] = index;
            m = strat[ii++];
            xDBLe(R, R, A24->X, A24->Z, (int)(2 * m));
            index += m;
        }
        GetFourIsogenyWithKernelXneZ(R, A24->X, A24->Z, coeff);
        for (i = 0; i < npts; i++) {
            EvalFourIsogenyWithKernelXneZ(pts[i], coeff);
        }

        fp2copy(pts[npts - 1]->X, R->X);
        fp2copy(pts[npts - 1]->Z, R->Z);
        index = pts_index[npts - 1];
        npts -= 1;
    }

    GetFourIsogenyWithKernelXneZ(R, A24->X, A24->Z, coeff);
    fp2add(A24->X, A24->X, A24->X);
    fp2sub(A24->X, A24->Z, A24->X);
    fp2add(A24->X, A24->X, A24->X);

    j_inv(A24->X, A24->Z, jinv);
}

//========================================================================================================



//========================================================================================================
//TODO: maybe have to change point to be a pointer...
__device__ void f(unsigned char j[FP2_ENCODED_BYTES], DIST_POINT point, data_t *data) {

    f2elm_t jinv;
    unsigned char c = GetC_SIDH(&point.end);
    unsigned char b = GetB_SIDH(&point.end);

    printf("1");

    GetIsogeny(
        jinv,
        data->E[c].a24,
        data->E[c].xp,
        data->E[c].xq,
        data->E[c].xpq,
        point.end.bytes,
        b,
        data->strat,
        data->lenstrat,
        (unsigned long) data->NBITS_STATE
    );

    printf("2");

    fp2_encode(jinv, j);

    XOF(point.end.bytes, j, (unsigned long) NBYTES, FP2_ENCODED_BYTES, (unsigned long) data->function_version);

    printf("3");

    //wierd randomization shit going on here, TODO: make this better
    if (b == 0x03) {
        while (b == 0x03)
        { /* Loop over bits from the right until we find 11 */
            XOF(&b, &c, 1, 1, (unsigned long)data->function_version);
            b &= 0x03;
            c++;
        }
        SetB_SIDH(&point.end, b);
    }
    //zero top bits
    point.end.bytes[NBYTES - 1] &= (0xFF >> ((8 - data->NBITS_OVERFLOW) % 8));


}

//========================================================================================================

//shared memory to save current distinguished points.
extern __shared__ DIST_POINT curr[];

//Kernel that sets up the states for random seeding later on
__global__ void setupStateKernel(
    curandState* state, 
    unsigned long seed, 
    unsigned int function_version) 
{
    int idx = blockIdx.x*blockDim.x + threadIdx.x;
    curand_init(seed, idx, function_version, &state[idx]);
    //printf("setup works\n");
}

//pass address to point and address to state, seed, and restore state
__device__ void generateSeed(DIST_POINT* point, curandState* state, data_t* data) {

    for (int i = 0; i < NBYTES; i++) {
        float rand = curand_uniform(state);
        rand *= 255.999999;
        point->start.bytes[i] = point->end.bytes[i] = (BYTE) truncf(rand);
        //check that b is never 0x03, only 0,1 or 2
        if (i == 0) {
            while ((point->end.bytes[0] & 0x06) >> 1 == 0x03) {
                rand = curand_uniform(state);
                rand *= 255.999999;
                point->start.bytes[i] = point->end.bytes[i] =( BYTE) truncf(rand);
            }
        }
    }
    //TODO: mask the points if size not bytealigned???
    point->start.bytes[NBYTES - 1] &= (0xFF >> ((8 - data->NBITS_OVERFLOW) % 8)); /* Zero top bits */
    //TODO: since we work with pointers, does it already adapt the state ^^?
}

//Kernel that finds distinguished points
__global__ void findDistPoints(
    curandState* states, 
    DIST_POINT* distinguished_points, 
    unsigned long long* counter,
    data_t* data) 
{
    unsigned long long numSteps = 0;
    unsigned long long count = 0;
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    curandState* localState = &states[idx];

    extern __shared__ DIST_POINT curr[];
    curr[threadIdx.x] = distinguished_points[idx];

    if (curr[threadIdx.x].valid >= 20 || curr[threadIdx.x].valid == 0) {
        //distinguished point is valid or expired, seed a new one
        generateSeed(&curr[threadIdx.x], localState, data);

        curr[threadIdx.x].steps = 0;
        curr[threadIdx.x].valid = 1;
    } else {
        //point wasn't valid / we need to resume from where we left off
        curr[threadIdx.x].valid += 1;
    }

    //states[idx] = localState;

    //"walk" along the random function until we find a distinguished point OR reach totalSteps
    while (numSteps < data->MAX_STEPS) {
        //if its distinguished point OR we just seeded new point, commence
        //if steps is 0, we are on a new "seed", if numSteps is also 0, we also are in the first iteration
        if (!DistinguishedSIDH(curr[threadIdx.x], data) || (curr[threadIdx.x].steps == 0 && numSteps == 0)) {
            unsigned char dummy[FP2_ENCODED_BYTES];
            f(dummy, curr[threadIdx.x], data);
            printf("case0");
            count++;
            numSteps++;
        } else {
            //if we find a distinguished point, set the valid-bit and the steps field
            curr[threadIdx.x].steps += numSteps;
            curr[threadIdx.x].valid = 0x80;
            printf("case1");
            break;
        }
        numSteps++;
    }

    if (numSteps >= data->MAX_STEPS) {
        curr[threadIdx.x].steps += data->MAX_STEPS;
    }

    printf("max: %f, %d\n, ", data->MAX_STEPS, curr[threadIdx.x].steps);

    curr[threadIdx.x].steps++;

    distinguished_points[idx] = curr[threadIdx.x];

    counter[idx] = count;
}

__global__ void locateCollisions(
    data_t* data,
    COLLISION* collisions,
    int num_collisions,
    HASHCOLLISION* results,
    unsigned long long* counter
)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    unsigned long long count = 0;
    f2elm_t jinv;

    //only the case if we found less collisions than we have threads
    if (idx >= num_collisions) {
        counter[idx] = 0;
        return;
    }

    COLLISION col;
    col = collisions[idx];

    //make sure that point0 is the one with fewer steps as before already aswell...
    unsigned long steps_diff = col.b.steps - col.a.steps;


    BYTE collision_hash_1[NBYTES];
    BYTE collision_hash_2[NBYTES];
    unsigned char j0[FP2_ENCODED_BYTES], j1[FP2_ENCODED_BYTES];
    /*
    BYTE cur_hash_a[NBYTES];
    BYTE cur_hash_b[NBYTES];

    //copy into variables
    
    for (int cp = 0; cp < NBYTES; cp++) {
        cur_hash_a[cp] = col.a.start.bytes[cp];
        cur_hash_b[cp] = col.b.start.bytes[cp];
    }
    */
    //walk along "longer" path until the pathlengths are equal
    for (int i = 0; i < steps_diff; i++) {
        f(j1,col.b, data);
        count++;
        col.b.steps -= 1;
    }

    //Robin hoods: if we have equally many steps left and the starting points match, its not a real collision
    if (is_equal_st(&col.a.start, &col.b.start, data->NWORDS_STATE)) {
        for (int cp = 0; cp < data->NWORDS_STATE; cp++) {
            results[idx].a.bytes[cp] = 0;
            results[idx].b.bytes[cp] = 0;
        }
        return;
    }

    for (int i = 0; i < col.a.steps; i++) {
        //save current values
        for (int cp = 0; cp < data->NWORDS_STATE; cp++) {
            collision_hash_1[cp] = col.a.start.bytes[cp];
            collision_hash_2[cp] = col.b.start.bytes[cp];
        }
        //walk one step simultaneously
        f(j0, col.a, data);
        f(j1, col.b, data);
        count += 2;

        if (IsEqualJinvSIDH(j0,j1)) {


            //REPLACEMENT FOR GETC_SIDH SINCE THEY ARE NOT ST_T TYPE HERE..
            if (collision_hash_1[0] & 0x01 == collision_hash_2[0] & 0x01) {

                //not really a collision since from same strang
                break;
            } else {
                fp2_decode(j0, jinv);
                if (fp2_is_equal(jinv, data->jinv)) {
                    printf("found golden?");
                }
            }
            break;
        }
    }
    //TODO: maybe copy words instead of bytes everywhere!? depends on architecture...
    //copy the found collision into global memory
    for (int cp = 0; cp < NBYTES; cp++) {
        results[idx].a.bytes[cp] = collision_hash_1[cp];
        results[idx].b.bytes[cp] = collision_hash_2[cp];
    }

    counter[idx] = count;


}

int main2() {

    unsigned long NBITS_K = 15;
    
    double ALPHA = 2.25;
    double BETA = 10;
    //double gamma = 20;
    
    f2elm_t jinv = {0x87D581FB16D011BD, 0x3, 0x5D14A016E29F3C, 0x1};
    CurveAndPointsSIDH E[2] = 
    {{
        .a24 = {0x898F502EC74EFE54, 0x3, 0x649BFBDDF471F7D0, 0x4},
        .xp = {0x81B68BDCA5EEE933, 0x1, 0x9BC03274CCE5407C, 0xE},
        .xq = {0x82D38AA521A411ED, 0x7, 0x8ECFD9A4B8E874AE, 0x6},
        .xpq = {0xA7DEE573517AA282, 0x5, 0x6AAA88DC2F3041ED, 0xA}
    },
    {
        .a24 = {0x5219884293D7ACA9, 0x3, 0xCCB60983643DBA16, 0xF},
        .xp = {0x34C2A2B46B60718A, 0xF, 0x7066C62B26701E69, 0xE},
        .xq = {0x13E5C49616025EB1, 0x11, 0xD4D573F7A00911FA, 0x4},
        .xpq = {0x3019C8E745061408, 0xE, 0x349BC9BDE78E1278, 0x7}
    }};

    unsigned long strat[250];
    unsigned long RAD = 64; //assuming 64bit architecture
    data_t* data = (data_t*) calloc(1, sizeof(data_t));
    data->function_version = 0;
    data->NBITS_STATE = NBITS_K + 3; /*TODO: is this correct?*/
    data->NBYTES_STATE = ((data->NBITS_STATE + 7) / 8);
    data->NWORDS_STATE = ((data->NBITS_STATE + RAD - 1) / RAD);
    data->NBITS_OVERFLOW = data->NBITS_STATE % 8;
    data->MEMORY_LOG_SIZE = 9;
    data->MEMORY_SIZE = (uint64_t)(1 << data->MEMORY_LOG_SIZE);
    data->MEMORY_SIZE_MASK = data->MEMORY_SIZE - 1;

    double THETA = ALPHA * sqrt((double) data->MEMORY_SIZE / (double)(3 * (1 << (NBITS_K + 1))));

    data->MAX_STEPS = ceil(1/THETA);
    data->MAX_DIST = (unsigned long)(BETA * data->MEMORY_SIZE);
    data->MAX_FUNCTION_VERSIONS = 10000;
    data->DIST_BOUND = THETA * (1 << (data->NBITS_STATE - 3 - data->MEMORY_LOG_SIZE));
    memcpy(data->E, E, 2*sizeof(CurveAndPointsSIDH));
    data->lenstrat = OptStrat(strat, (unsigned long)(NBITS_K + 1) / 2, 1, 1);
    //printf("%lu", data->lenstrat);
    data->strat = (unsigned long*) calloc(data->lenstrat, sizeof(unsigned long));
    memcpy(data->jinv, jinv, sizeof(f2elm_t));
    data->random_functions = 0;

    int B = 1;
    int T = 10;
    int THREADS = B*T;

    srand(time(NULL));

    unsigned long function_version = 1;
    unsigned long seed = rand();

    curandState* d_states;
    if (cudaSuccess != cudaMalloc(&d_states, THREADS*sizeof(curandState))) {
        printf("Error d_states malloc: %d\n", cudaGetLastError());
    }

    DIST_POINT* h_distinguished_points;
    DIST_POINT* d_distinguished_points;

    unsigned long long* h_countlist;
    h_countlist = (unsigned long long*) calloc(THREADS, sizeof(unsigned long long));
    unsigned long long* d_countlist;
    if (cudaSuccess != cudaMalloc(&d_countlist, THREADS*sizeof(unsigned long long))) {
        printf("Error d_countlist malloc: %d\n", cudaGetLastError());
    }

    h_distinguished_points       = (DIST_POINT*)     calloc(THREADS, sizeof(DIST_POINT));
    if (cudaSuccess != cudaMalloc(&d_distinguished_points, THREADS*sizeof(DIST_POINT))) {
        printf("Error d_dist_points malloc: %d\n", cudaGetLastError());
    }

    data_t* d_data;

    if (cudaSuccess != cudaMalloc(&d_data, sizeof(data_t))) {
        printf("Error d_dist_points malloc: %d\n", cudaGetLastError());
    }

    cudaMemcpy(d_data, data, sizeof(data_t), cudaMemcpyHostToDevice);

    setupStateKernel<<<B,T>>>(d_states, seed, function_version);



    size_t shared_size = T*sizeof(DIST_POINT);

    findDistPoints<<<B,T, shared_size>>>(d_states, d_distinguished_points, d_countlist, d_data);

    cudaMemcpy(h_distinguished_points, d_distinguished_points, THREADS*sizeof(DIST_POINT), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_countlist, d_countlist, THREADS*sizeof(unsigned long long), cudaMemcpyDeviceToHost);

    unsigned long long count = 0;

    for (int i = 0; i < THREADS; i++) {
        count += h_countlist[i];
        printf("Steps: %d : \n", h_distinguished_points[i].steps);
        //printf("Steps: %d\n", h_distinguished_points[i].start.bytes[1]);
    }

    printf("Count: %llu\n", count);

    printf("sizeof:%lu,\n", sizeof(st_t));

    free(h_distinguished_points);
    cudaFree(d_states);
    cudaFree(d_distinguished_points);
    cudaFree(d_data);
    free(data);

    return 0;
}

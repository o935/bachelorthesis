#include <stdint.h>

//own config:

typedef unsigned char BYTE;
typedef unsigned int WORD;
typedef unsigned long long LONG;

//TODO: temporarily defined as constant
#define NBYTES 3

#define RADIX      64
#define LOG2RADIX   6
typedef uint64_t digit_t;

#define EXTRA_MEM_LOG_SIZE 3

#define RADIX64 64

#define NBITS_TO_NBYTES(nbits) (((nbits) + 7) / 8)                                             // Conversion macro from number of bits to number of bytes
#define NBITS_TO_NWORDS(nbits) (((nbits) + (sizeof(digit_t) * 8) - 1) / (sizeof(digit_t) * 8)) // Conversion macro from number of bits to number of computer words
#define NBYTES_TO_NWORDS(nbytes) (((nbytes) + sizeof(digit_t) - 1) / sizeof(digit_t))          // Conversion macro from number of bytes to number of computer words


//from triples.h

typedef union {
    unsigned char bytes[8]; /* Byte representation of size NBYTES_STATE */ //TODO: set to fixed size 8 here.
    digit_t words[1];       /* Word representation of size NWORDS_STATE */
} st_t;

typedef struct {
    st_t current_state;
    uint64_t current_steps;
    st_t initial_state;
} trip_t;

typedef struct {
    unsigned int steps;
    st_t start;
    st_t end;
    BYTE valid;
} DIST_POINT;

typedef struct  {
    DIST_POINT a;
    DIST_POINT b;
} COLLISION;

typedef struct  {
    st_t a;
    st_t b;
} HASHCOLLISION;


/********************** Constant-time unsigned comparisons ***********************/

// The following functions return 1 (TRUE) if condition is true, 0 (FALSE) otherwise

__device__ static unsigned int is_digit_nonzero_ct(digit_t x)
{ // Is x != 0?
    return (unsigned int)((x | (0 - x)) >> (RADIX - 1));
}

__device__ static unsigned int is_digit_zero_ct(digit_t x)
{ // Is x = 0?
    return (unsigned int)(1 ^ is_digit_nonzero_ct(x));
}

__device__ static unsigned int is_digit_lessthan_ct(digit_t x, digit_t y)
{ // Is x < y?
    return (unsigned int)((x ^ ((x ^ y) | ((x - y) ^ y))) >> (RADIX - 1));
}

//these are for generic implementation: TODO: check if correct one? from config.h

// Digit multiplication
#define MUL(multiplier, multiplicand, hi, lo) \
    digit_x_digit((multiplier), (multiplicand), &(lo));

// Digit addition with carry
#define ADDC(carryIn, addend1, addend2, carryOut, sumOut)                                                           \
    {                                                                                                               \
        digit_t tempReg = (addend1) + (digit_t)(carryIn);                                                           \
        (sumOut) = (addend2) + tempReg;                                                                             \
        (carryOut) = (is_digit_lessthan_ct(tempReg, (digit_t)(carryIn)) | is_digit_lessthan_ct((sumOut), tempReg)); \
    }

// Digit subtraction with borrow
#define SUBC(borrowIn, minuend, subtrahend, borrowOut, differenceOut)                                                      \
    {                                                                                                                      \
        digit_t tempReg = (minuend) - (subtrahend);                                                                        \
        unsigned int borrowReg = (is_digit_lessthan_ct((minuend), (subtrahend)) | ((borrowIn)&is_digit_zero_ct(tempReg))); \
        (differenceOut) = tempReg - (digit_t)(borrowIn);                                                                   \
        (borrowOut) = borrowReg;                                                                                           \
    }

// Shift right with flexible datatype
#define SHIFTR(highIn, lowIn, shift, shiftOut, DigitSize) \
    (shiftOut) = ((lowIn) >> (shift)) ^ ((highIn) << (DigitSize - (shift)));

// Shift left with flexible datatype
#define SHIFTL(highIn, lowIn, shift, shiftOut, DigitSize) \
    (shiftOut) = ((highIn) << (shift)) ^ ((lowIn) >> (DigitSize - (shift)));

// 64x64-bit multiplication
#define MUL128(multiplier, multiplicand, product) \
    mp_mul((digit_t *)&(multiplier), (digit_t *)&(multiplicand), (digit_t *)&(product), NWORDS_FIELD / 2);

// 128-bit addition, inputs < 2^127
#define ADD128(addend1, addend2, addition) \
    mp_add((digit_t *)(addend1), (digit_t *)(addend2), (digit_t *)(addition), NWORDS_FIELD);

// 128-bit addition with output carry
#define ADC128(addend1, addend2, carry, addition) \
    (carry) = mp_add((digit_t *)(addend1), (digit_t *)(addend2), (digit_t *)(addition), NWORDS_FIELD);

//====================from P128_internal.h=====================
#define NWORDS_FIELD    2               // Number of words of a 128-bit field element
#define p128_ZERO_WORDS 0               // Number of "0" digits in the least significant part of p128 + 1
#define NBITS_FIELD 69 

#define MAXBITS_FIELD           128                
#define MAXWORDS_FIELD          ((MAXBITS_FIELD+RADIX-1)/RADIX)     // Max. number of words to represent field elements
#define NWORDS64_FIELD          ((NBITS_FIELD+63)/64)               // Number of 64-bit words of a 128-bit field element 
#define NBITS_ORDER             128
#define NWORDS_ORDER            ((NBITS_ORDER+RADIX-1)/RADIX)       // Number of words of oA and oB, where oA and oB are the subgroup orders of Alice and Bob, resp.
#define NWORDS64_ORDER          ((NBITS_ORDER+63)/64)               // Number of 64-bit words of a 256-bit element 
#define MAXBITS_ORDER           NBITS_ORDER                         
#define MAXWORDS_ORDER          ((MAXBITS_ORDER+RADIX-1)/RADIX)     // Max. number of words to represent elements in [1, oA-1] or [1, oB].
#define ALICE                   0
#define BOB                     1 
#define OALICE_BITS             32
#define OBOB_BITS               32 
#define PRIME                   p128 
#define PARAM_A                 6  
#define PARAM_C                 1
// Fixed parameters for isogeny tree computation
#define MAX_INT_POINTS_ALICE    7
#define FP2_ENCODED_BYTES       2*((NBITS_FIELD + 7) / 8)

//constants of p_32_20
__constant__ uint64_t p128[NWORDS64_FIELD] = { 0xAC0E7A06FFFFFFFF, 0x12 };
__constant__ uint64_t p128p1[NWORDS64_FIELD] = { 0xAC0E7A0700000000, 0x12 };
__constant__ uint64_t p128x2[NWORDS64_FIELD] = { 0x581CF40DFFFFFFFE, 0x25 };
__constant__ uint64_t Montgomery_rprime1[NWORDS64_ORDER] = { 0xAC0E7A0700000001, 0x96F0AD1DFAEEAC43 };
__constant__ uint64_t Montgomery_R2[NWORDS64_ORDER] = { 0x835010E3A34C2C1C, 0x3 };
__constant__ uint64_t Montgomery_one[NWORDS64_ORDER] = { 0xD9FEFBEAD8BA0D2B, 0x4 };

// SIDH's basic element definitions and point representations

typedef digit_t felm_t[NWORDS_FIELD];                                 // Datatype for representing 128-bit field elements (512-bit max.)
typedef digit_t dfelm_t[2*NWORDS_FIELD];                              // Datatype for representing double-precision 2x128-bit field elements (512-bit max.) 
typedef felm_t  f2elm_t[2];                                           // Datatype for representing quadratic extension field elements GF(p128^2)
        
typedef struct { f2elm_t X; f2elm_t Z; } point_proj;                  // Point representation in projective XZ Montgomery coordinates.
typedef point_proj point_proj_t[1]; 

// Setting up macro defines and including GF(p), GF(p^2), curve, isogeny and kex functions
#define fpcopy                        fpcopy128
#define fpzero                        fpzero128
#define fpadd                         fpadd128
#define fpsub                         fpsub128
#define fpneg                         fpneg128
#define fpdiv2                        fpdiv2_128
#define fpcorrection                  fpcorrection128
#define fpmul_mont                    fpmul128_mont
#define fpsqr_mont                    fpsqr128_mont
#define fpinv_mont                    fpinv128_mont
#define fpinv_chain_mont              fpinv128_chain_mont
#define fpinv_mont_bingcd             fpinv128_mont_bingcd
#define fp2copy                       fp2copy128
#define fp2zero                       fp2zero128
#define fp2add                        fp2add128
#define fp2sub                        fp2sub128
#define fp2neg                        fp2neg128
#define fp2div2                       fp2div2_128
#define fp2correction                 fp2correction128
#define fp2mul_mont                   fp2mul128_mont
#define fp2sqr_mont                   fp2sqr128_mont
#define fp2inv_mont                   fp2inv128_mont
#define fp2inv_mont_bingcd            fp2inv128_mont_bingcd
#define mp_add_asm                    mp_add128_asm
#define mp_subaddx2_asm               mp_subadd128x2_asm
#define mp_dblsubx2_asm               mp_dblsub128x2_asm
#define crypto_kem_keypair            crypto_kem_keypair_SIKEp128
#define crypto_kem_enc                crypto_kem_enc_SIKEp128
#define crypto_kem_dec                crypto_kem_dec_SIKEp128
#define random_mod_order_A            random_mod_order_A_SIDHp128
#define random_mod_order_B            random_mod_order_B_SIDHp128

#define fpcopy                        fpcopy128
#define fpzero                        fpzero128
#define fpsub                         fpsub128
#define fpneg                         fpneg128
#define fp2copy                       fp2copy128
#define fp2add                        fp2add128
#define fp2sub                        fp2sub128
#define fp2neg                        fp2neg128
#define fp2correction                 fp2correction128
#define fp2mul_mont                   fp2mul128_mont
#define fp2sqr_mont                   fp2sqr128_mont
#define fpinv_mont                    fpinv128_mont
#define fp2inv_mont                   fp2inv128_mont

/* Functions from an isogeny library */
#define xDBL_SIDH                               xDBL
#define xDBLe_SIDH                              xDBLe
#define xDBLADD_SIDH                            xDBLADD

#define GetFourIsogenyWithKernelXneZ            get_4_isog
#define EvalFourIsogenyWithKernelXneZ           eval_4_isog

typedef struct {
	f2elm_t a24;
	f2elm_t xp;
	f2elm_t xq;
	f2elm_t xpq;
} CurveAndPointsSIDH;

//from sidh_vow_base.h

#define distinguished           DistinguishedSIDH
#define mem_index               MemIndexSIDH
#define sample                  SampleSIDH
#define backtrack               BacktrackSIDH
#define update                  UpdateSIDH
#define update_random_function  UpdateRandomFunctionSIDH

//state.h

typedef struct {
    uint64_t function_version;
    //number of functionversions tried?
    uint64_t random_functions;
    uint64_t NBITS_STATE;
    uint64_t NBYTES_STATE;
    uint64_t NWORDS_STATE;
    uint64_t NBITS_OVERFLOW;
    uint64_t MEMORY_LOG_SIZE;
    uint64_t MEMORY_SIZE;
    uint64_t MEMORY_SIZE_MASK;
    double MAX_STEPS;
    uint64_t MAX_DIST;
    uint64_t MAX_FUNCTION_VERSIONS;
    double DIST_BOUND;

    CurveAndPointsSIDH E[2];

    unsigned long *strat;
    unsigned long lenstrat;
    f2elm_t jinv;           /* For verifying */
    unsigned char *jinvLUT; // Look-up table for precomputing j-invariants

} data_t;



